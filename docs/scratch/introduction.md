# Project 1 - Missile Command

![Missile Command](assets/game.jpg){: style="width:600px"}

## Context

Before you were born, there were very few people who had computers in their homes. If you wanted to play computer game, you played arcade games on special machines that could only play one type of game.

The games on arcade machines were very simple, but still exciting to play. This is where Pacman, Mario and the Street Fighter games all began.

The game Missile Command, began on these arcade machines, and in this project, you are going to create your own version of it. The idea will be to have Rocks flying towards the Earth, and the player destroying them by firing missiles.

## How to start

* Go to <a href="https://scratch.mit.edu/">the Scratch website</a>. 
* Connect to the class Scratch account (you can use your personal account if you have one).
* Click on Create
* Change the name of the project to your family name.

![create](assets/create.png){: style="width:600px"}

## Change backdrop

The first thing you will need for your game is a new backdrop. You can find the backdrop options on the bottom right corner. By default, you have a white one. 

* Choose the backdrop you want for this project (I picked the Stars).

![backdrop](assets/backdrop.png){: style="width:300px"}

## Choose your base

* Delete the cat sprite by clicking on the bin in the right corner.
* Add a new sprite as your base (I picked the earth).

![cat](assets/cat.png){: style="width:300px; display:inline"}

!!!info "You should have a similar result:"

	![base](assets/base.png){: style="width:300px; display:inline"}


## Asteroids (1/2)

We can start sending asteroids with a few simple scripts. Because we wants lots of asteroids, we need to create copies of the Rock sprite. In Scratch, these copies are called clones. When the game starts, we want to create a new copy of the Rock sprite every two seconds.

* Choose a sprite to represent the asteroids that will be flying towards earth (I picked the rocks).
* Add the below code in your Asteroid sprite:

!!!info "Asteroid Sprite"

	![Asteroid1](assets/asteroid1.png){: style="width:400px"}

!!!warning "If you click on the green flag, you should see your asteroids being created and flying of the stage."

Let’s improve our script and add some code between the block ```when I start as a clone``` and the block ```forever```. We want the rocks to go towards our base (the earth). We shall use a bit of randomness to change their starting position, their size and their direction.

* Improve the code to your Asteroid sprite so it looks like this: 

!!!info "Asteroid Sprite"
	
	![Asteroid2](assets/asteroid2.png){: style="width:400px"}


!!!danger "Improve your code, by adding some blocks to your existing content. Don't recreate everything completely."

* Click the green flag and see what happens.

As you probably noticed, the original rock is still visible. We need to add a block ```hide``` between ```when flag clicked``` and ```forever``` in order to hide it.

Then we need to add a block ```show``` between ```when I start as a clone``` and ```go to x:``` in order to show the rocks when they are cloned.

* Improve the code to your Asteroid sprite so it looks like this: 

!!!info "Asteroid Sprite"
	
	![Asteroid3](assets/asteroid3.png){: style="width:400px"}

You might notice that the rocks hang around at the bottom of the screen. To avoid this, we can improve once again our code and ask to destroy the clones if their ```y position``` is less than ```-180```. (If it doesn’t work, try to put a bit more than -180, -175 for example.)

* Improve the code to your Asteroid sprite so it looks like this: 

!!!info "Asteroid Sprite"
	
	![Asteroid4](assets/asteroid4.png){: style="width:400px"}


## Create Variables

Now we need to enable the player to destroy the rocks and protect the earth.
We are going to need a few more scripts and some variables to achieve this.
Firstly create three new variables: 

* xTarget;
* yTarget;
* fired.

To create a variable, you need to:

* Click on Make a Variable.
* Write the name of the variable (xTarget for example).
* Select ```For all sprites```.
* Click on OK.

![Variable](assets/variable.png){: style="width:300px"}


!!!danger "Repeat this process 3 times so you can create ```xTarget```, ```yTarget``` and ```fired```"

## Pimp my Stage

We need to save the exact coordinates where the player clicked on the stage and to then broadcast a message. We are going to use the variables xTarget and yTarget to save the coordinates and the variable fired because the player can fire only one missile at a time. 

* Add the following code to your Stage. 

!!!info "Stage Sprite"
	
	![Stage](assets/stage.png){: style="width:200px"}

!!!danger "Don’t forget to select your stage first before adding the code!"

## Missile (1/3)

Choose or create a sprite that you will be used as missiles. Those will destroy the asteroids and protect the earth. I picked the rocketship.

![Rocketship](assets/rocketship.png){: style="width:300px"}

## Target

Let’s now create our target sprite.
You can either pick one from the library or create it yourself.
If you draw it, make sure it’s in the middle of the canvas.

![Target](assets/target.png){: style="width:400px"}

Let’s now add some scripts to our target. When we start the game, the target sprite needs to be hidden, but when the player clicks on the stage, the fire message will be broadcasted and the target should put a clone of itself at the mouse pointer. It will also broadcast explode if it gets touched by the missile.

* Add the following code to your target sprite:

!!!info "Target Sprite"
	
	![Target](assets/target1.png){: style="width:300px"}

## Missile (2/3)

Now let’s fire the missile.
We only allow one missile fired at a time.

* Add these scripts to your missile sprite:

!!!info "Missile Sprite"
	
	![Missile](assets/missile1.png){: style="width:550px"}

## Explosion (1/2)

Let’s create an explosion. Make or import something that looks like a ball of fire. Make sure it starts very small. 

![Explosion](assets/explosion.png){: style="width:100px"}

## Missile (3/3)

When the missile touches the target, we want to create a clone of explosion

* Improve the code to your missile sprite so it looks like this: 

!!!info "Missile Sprite"
	
	![Missile](assets/missile2.png){: style="width:550px"}

## Explosion (2/2)

When a clone of explosion is made, we want it to grow and shrink rapidly.

* Add these scripts to your explosion sprite:

!!!info "Explosion Sprite"
	
	![Explosion](assets/explosion2.png){: style="width:400px"}


## Asteroid (2/2)

To finish, we need to have the rocks being destroyed if they touch the explosion. If they touch the base however, the game sould end and a message should be displayed.

* Add a new backdrop to your stage. It should say something like Game over.
* Then add the following script to your asteroid sprite:

!!!info "Asteroid Sprite"
	
	![Asteroid4](assets/asteroid5.png){: style="width:300px"}

## Grading

* Project working without improvment: C
* Project working with some minor improvments: B
* Project working with a major improvment: A

Ideas of Improvments:

* Scoring system
* Second base
* Lives
* Different levels
* ...


















