# Project 2 - Snake

![Snake](assets/snake.jpg){: style="width:600px"}

## How to start

* Go to <a href="https://scratch.mit.edu/">the Scratch website</a>. 
* Connect to the class Scratch account (you can use your personal account if you have one).
* Click on Create
* Change the name of the project to your family name.

![create](assets/create.png){: style="width:600px"}

## Create your snake

Let’s delete the cat sprite and create a snake sprite. You can either get it from the internet or you can create your own. Your sprite needs two costumes, the first one is the head and the second one is the body.

* Click on Choose a sprite -> Paint

![Paint](assets/snakeCostume.png){: style="width:300px"}

* Draw the head of your snake (or upload one)
* Click on Choose a costume -> Paint
* Draw the body of your snake

![Snake Costume](assets/snakeCostume2.png){: style="width:500px"}

## Make it move

Let’s add some scripts so your snake continuously moves. Continuous movement means using a loop. The code is very easy:

![move](assets/move1.png){: style="width:150px"}

Our snake just disappears to the right, so let’s now make sure that the player can control where it goes using the arrows on the keyboard. Each time an arrow is pressed, an event will be triggered and the snake will go in the direction chosen by the user.

I will show you how to make it go down, you have to do the three other directions and adapt the angle of the direction accordingly:

![keyboard move](assets/move2.png){: style="width:200px"}

!!!danger "Important"

	Before going to the next step, make sure that your snake move and you can control it with the arrows of the keyboard.

## Add target

Our snake is hungry! Let’s add a mouse at a random location in the screen so he can try to eat it.

Click on Choose a sprite -> Choose a sprite and select the mouse. Don’t hesitate to pick a different sprite and to adjust its size.

Let’s now make it appear at a random position on the screen each time we click on the green flag. And as soon as the mouse touches the snake, it should go to another random position.

![Mouse](assets/mouse.png){: style="width:250px"}


## Score

When the snake eats a mouse, we want a variable score to be increased by one.

* Make a new variable and call it score.
* Set the score to 0 when we click on the green flag.
* Change the score by 1 each time the mouse is touched by the snake.

!!!danger "Imporant"

	Don't add a new ```touching Snake``` block. There is already one in the mouse sprite, add the change score there.

## Tail of the snake

Each time the snake eats a mouse, we want its tail to grow. To do that, we are going to use clones. It is a bit tricky but the idea is that we create clones continuously (every 0.1 sec) and we are going to delete the ones that we don’t need.

* Insert a block in your main code in the snake to create clones

![Create Clone](assets/createClone.png){: style="width:200px"}

!!!danger "Important"

	Don't rewrite everything, add blocks and upgrade your existing move block.


* When you start as a clone, change costumes so you are a body, then wait score/10 seconds before deleting the clone

![Create Clone](assets/createClone2.png){: style="width:200px"}



## Game Over

The game is over if the snake touches the edges or if it crosses its body. 
The second condition (color ```red``` is touching ```green```) bugs a lot in scratch so if it doesn’t work for you, do not worry, it’s normal.

* First you have to specify when the game is over:

![Game Over](assets/gameOver.png){: style="width:250px"}

!!!danger "Important"

	Again, don't rewrite everything, add blocks and upgrade your existing move block.


* Then you have to take the appropriate actions when your receive the game over message (for example changing the backdrop):

![Game Over](assets/gameOver2.png){: style="width:200px"}



## Grading

* Project working without improvment: C
* Project working with some minor improvments: B
* Project working with a major improvment: A

Ideas of Improvments:

* Introduction and Game Over screens
* Different Levels
* Snake can teleport from one side of the screen to the opposite one
* ...





