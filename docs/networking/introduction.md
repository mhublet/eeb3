The goal of networks is to link multiple devices so they can share resources and communicate with each other.

In this introduction, we will learn about:

* the different types of network;
* the main components of a network;
* the different methods to transfer data on a network.
* the different options to connect to an ISP;
* what makes a host a client or a server;


## Network Types

Local networks come in all sizes. They can range from simple networks consisiting of two computers, to networks connecting hundreds of thousands of devices.

* Small Home Networks

Small home networks connect a few computers to each other and to the internet.

* SOHO

Networks installed in small offices or homes and home offices, are referred to as small office/home office (SOHO) networks. SOHO networks allow computers in a home office or a remote office to connect to a corporate network, or access centralized, shared resources.

* Medium to Large Networks

Medium to large networks, such as those used by corporations and schools, can have many locations with hundreds or thousands of interconnected hosts.

* World Wide Networks

The internet is a network of networks that connects hundreds of millions of computers world-wide.
The Internet backbone is the core of the Internet where the largest and fastest networks are linked together with fiber-optic connections and high-performance routers.

??? "More info"

	 You might be surprised to know that 99% of international data is transmitted by cables at the bottom of the ocean with submarine communication cables.

	 <iframe width="560" height="315" src="https://www.youtube.com/embed/d0gs497KApU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>



## Network Components

The network infrastructure contains three categories of hardware components:

* End devices
* Intermediate devices
* Network media

![components](assets/components.png){: style="width:500px"}

## Data Transmission

Data is first transformed into a series of bits and then, it is converted into signals that can be sent across the network media to its destination. 

There are three common methods of signal transmission used in networks:

1. Electrical signals - Transmission is achieved by representing data as electrical pulses on copper wire.
2. Optical signals - Transmission is achieved by converting the electrical signals into light pulses.
3. Wireless signals - Transmission is achieved by using infrared, microwave, or radio waves through the air.

!!!info "Definitions"

	**Media** refers to the physical medium on which the signals are transmitted (copper wire, fiber-optic cable, and electromagnetic waves through the air).

	**Signal** consists of electrical or optical patterns that are transmitted from one connected device to another. These patterns represent the digital bits (i.e. the data) and travel across the media from source to destination. Signals may be converted many times before ultimately reaching the destination, as corresponding media changes between source and destination.

	**Bandwidth** is the capacity of a medium to carry data. Digital bandwidth measures the amount of data that can flow from one place to another in a given amount of time. Bandwidth is typically measured in the number of bits that (theoretically) can be sent across the media in a second (Kbps, Mbps or Gbps).

	**Throughput** is the measure of the transfer of bits across the media over a given period of time. However, due to a number of factors, throughput does not usually match the specified bandwidth. Many factors influence throughput including the amount of data being sent and received over the connection, the types of data being transmitted and the latency created by the number of network devices encountered between source and destination. Bandwidth is theoretical, throughput is empirical.


## ISP Connectivity Options

An Internet Service Provider (ISP) provides the link between the home network and the internet. An ISP can be the local cable provider, a landline telephone service provider, the cellular network that provides your smartphone service, or an independent provider who leases bandwidth on the physical network infrastructure of another company.

The interconnection of ISPs forms the backbone of the internet and is a complex web of fiber-optic cables with expensive networking switches and routers that direct the flow of information between source and destination hosts all around the world. The primary medium that connects the internet backbone is fiber-optic cable. Fiber-optic cables also run under the sea to connect continents, countries, and cities.

Most home network users do not connect to their service providers with fiber-optic cables. The other methods are:

* Cable - Typically offered by cable television service providers, the internet data signal is carried on the same coaxial cable that delivers cable television. A special cable modem separates the internet data signal from the other signals carried on the cable and provides an Ethernet connection to a host computer or LAN.
* DSL - Digital Subscriber Line requires a special high-speed modem that separates the DSL signal from the telephone signal and provides an Ethernet connection to a host computer or LAN.
* Cellular - Cellular internet access uses a cell phone network to connect. Wherever you can get a cellular signal, you can get cellular internet access.
* Satellite - Satellite service is a good option for homes or offices that do not have access to DSL or cable.
* Dial-up Telephone - An inexpensive option that uses any phone line and a modem. To connect to the ISP, a user calls the ISP access phone number.

![connectivity](assets/ispConnectivity.png){: style="width:600px"}

## Clients and Servers

All computers connected to a network that participate directly in network communication are classified as hosts. Hosts can send and receive messages on the network. In modern networks, computer hosts can act as a client, a server, or both. The software installed on the computer determines which role the computer plays.

* Servers are hosts that have software installed which enable them to provide information, like email or web pages, to other hosts on the network.

* Clients are computer hosts that have software installed that enables the hosts to request and display the information obtained from the server.


!!!info "Acronyms"

	**WAN** - Wide Area Network or the Internet

	**LAN** - Local Area Network

	**WLAN** - Wireless Local Area Network