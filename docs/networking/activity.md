# Activity

## Activity 1

Configure DHCP on a Wireless Router using Cisco Packet Tracer.

* Go to Classes ICT.
* Open the file ```1. configure-dhcp-on-a-wireless-router.pka```.
* Follow the instructions.

!!!info
	
	In this activity, you will complete the following objectives:

	* Connect 3 PCs to a wireless router.
	* Change the DHCP setting to a specific network range.
	* Configure the clients to obtain their address via DHCP.

## Activity 2

Examine NAT on a Wireless Router using Cisco Packet Tracer.

* Go to Classes ICT.
* Open the file ```2. examine-nat-on-a-wireless-router.pka```.
* Follow the instructions.

!!!info
	
	In this activity, you will complete the following objectives:

	* Examine NAT configuration on a wireless router.
	* Set up 4 PCs to connect to a wireless router using DHCP.
	* Examine traffic that crosses the network using NAT.


## Activity 3

Identify MAC and IP Addresses using Cisco Packet Tracer.

* Go to Classes ICT.
* Open the file ```3. identify-mac-and-ip-addresses.pka```.
* Follow the instructions.

!!!info
	
	In this activity, you will complete the following objectives:

	* Gather PDU Information for Local Network Communication
	* Gather PDU Information for Remote Network Communication
	
	This activity is optimized for viewing PDUs. The devices are already configured. You will gather PDU information in simulation mode and answer a series of questions about the data you collect.



## Activity 4

Observe Traffic Flow in a Routed Network

* Go to Classes ICT.
* Open the file ```4. observe-traffic-flow-in-a-routed-network.pka```.
* Follow the instructions.

!!!info
	
	In this activity, you will complete the following objectives:

	* Part 1: Observe Traffic Flow in an Unrouted LAN
	* Part 2: Reconfigure the Network to Route Between LANs
	* Part 3: Observe Traffic Flow in the Routed Network


## Activity 5

Observe Traffic Flow in a Routed Network

* Go to Classes ICT.
* Open the file ```5. create-lan.pka```.
* Follow the instructions.

!!!info
	
	In this activity, you will complete the following objectives:

	* Connect Network Devices and Hosts
	* Configure Devices with IPv4 Addressing
	* Verify the End Device Configuration and Connectivity
	* Use Networking Commands to View Host Information


