
## The IPv4 Address

A host needs an IPv4 address to participate on the internet and almost all LANs today. The IPv4 address is a logical network address that identifies a particular host. It must be properly configured and unique within the LAN, for local communication. It must also be properly configured and unique in the world, for remote communication.

An IPv4 address is assigned to the network interface connection for a host. This connection is usually a NIC (Network Interface Card) installed in the device.

Every packet sent across the internet has a source and destination IPv4 address. This information is required by networking devices to ensure the information gets to the destination and any replies are returned to the source.

???example "Test your Connectivity"

	Open a command prompt and use the ping command to verify your connectivity:

	```ping 8.8.8.8```

	You should get a similar result:

	```cmd
	C:\Users\magal>ping 8.8.8.8
	Pinging 8.8.8.8 with 32 bytes of data:
	Reply from 8.8.8.8: bytes=32 time=17ms TTL=116
	Reply from 8.8.8.8: bytes=32 time=31ms TTL=116
	Reply from 8.8.8.8: bytes=32 time=19ms TTL=116
	Reply from 8.8.8.8: bytes=32 time=18ms TTL=116

	Ping statistics for 8.8.8.8:
    	Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
	Approximate round trip times in milli-seconds:
    	Minimum = 17ms, Maximum = 31ms, Average = 21ms
    ```

## Octets and Dotted-Decimal Notation

IPv4 addresses are 32 bits in length. Here is an IPv4 address in binary:

```11010001101001011100100000000001```

Notice how difficult this address is to read. Imagine having to configure devices with a series of 32 bits! For this reason, the 32 bits are grouped into four 8-bit bytes called octets like this:

```11010001.10100101.11001000.00000001```

That's better, but still difficult to read. That's why we convert each octet into its decimal value, separated by a decimal point or period. 

The above binary IPv4 becomes this dotted-decimal representation:

```209.165.200.1```

## Structure

The logical 32-bit IPv4 address is hierarchical and is made up of two parts, the network, and the host. 

!!!example

	There is a host with an IPv4 address ```192.168.5.11``` with a subnet mask of ```255.255.255.0```. 

	The first three octets, ```192.168.5```, identify the network portion of the address, and the last octet, ```11```, identifies the host. 

This is known as hierarchical addressing because the network portion indicates the network on which each unique host address is located.

Routers only need to know how to reach each network, rather than needing to know the location of each individual host. With IPv4 addressing, multiple logical networks can exist on one physical network if the network portion of the logical network host addresses is different.

![IPv4 Example](assets/ipv4Example.png){: style="width:600px"}

## Transmission

**Unicast transmission** refers to one device sending a message to one other device in one-to-one communications. A unicast packet has a destination IP address that is a unicast address which goes to a single recipient. 

**Broadcast transmission** refers to a device sending a message to all the devices on a network in one-to-all communications. A broadcast packet has a destination IP address with all ones (1s) in the host portion, or 32 one (1) bits. A broadcast packet must be processed by all devices in the same broadcast domain. A broadcast may be directed or limited. In **limited broadcast**, the data reaches from a source to all the host in the same network. In **directed broadcast**, a host in one network send the message to all the hosts in another network.

**Multicast transmission** reduces traffic by allowing a host to send a single packet to a selected set of hosts that subscribe to a multicast group. A multicast packet is a packet with a destination IP address that is a multicast address. IPv4 has reserved the ```224.0.0.0``` to ```239.255.255.255``` addresses as a multicast range. Each multicast group is represented by a single IPv4 multicast destination address. When an IPv4 host subscribes to a multicast group, the host processes packets addressed to this multicast address, and packets addressed to its uniquely allocated unicast address.

!!!info

	* A source IP address can only be a unicast address because the packet can only originate from a single source. This is regardless of whether the destination IP address is a unicast, broadcast or multicast. IPv4 unicast host addresses are in the address range of ```1.1.1.1``` to ```223.255.255.255```.
	* By default, routers do not forward broadcasts.

## Types of IPv4 Addresses

**Public IPv4 addresses** are addresses which are globally routed between ISP routers. However, not all available IPv4 addresses can be used on the internet. There are blocks of addresses called **private addresses** that are used by most organizations to assign IPv4 addresses to internal hosts. Most internal networks, from large enterprises to home networks, use private IPv4 addresses for addressing all internal devices (intranet) including hosts and routers. However, private addresses are not globally routable. Before the ISP can forward this packet, it must translate the source IPv4 address, which is a private address, to a public IPv4 address using NAT.

**Loopback addresses** (127.0.0.0 /8 or 127.0.0.1 to 127.255.255.254) are more commonly identified as only 127.0.0.1, these are special addresses used by a host to direct traffic to itself. 

**Link-local addresses** (169.254.0.0 /16 or 169.254.0.1 to 169.254.255.254) are more commonly known as the Automatic Private IP Addressing (APIPA) addresses or self-assigned addresses. They are used by a Windows DHCP client to self-configure in the event that there are no DHCP servers available.

!!!danger "Special IPv4 Addresses"
	
	![Special IPv4 Address](assets/specialAddress.png){: style="width:500px"}

## Classful Addressing

In 1981, IPv4 addresses were assigned using classful addressing as defined in RFC 790 (https://tools.ietf.org/html/rfc790), Assigned Numbers. Customers were allocated a network address based on one of three classes, A, B, or C. The RFC divided the unicast ranges into specific classes as follows:

* Class A (0.0.0.0/8 to 127.0.0.0/8) - Designed to support extremely large networks with more than 16 million host addresses.
* Class B (128.0.0.0 /16 - 191.255.0.0 /16) - Designed to support the needs of moderate to large size networks with up to approximately 65,000 host addresses.
* Class C (192.0.0.0 /24 - 223.255.255.0 /24) - Designed to support small networks with a maximum of 254 hosts.
* Class D, a multicast block consisting of 224.0.0.0 to 239.0.0.0.
* Class E experimental address block consisting of 240.0.0.0 - 255.0.0.0.

Public IPv4 addresses are addresses which are globally routed over the internet. Public IPv4 addresses must be unique. Both IPv4 and IPv6 addresses are managed by the IANA. The IANA manages and allocates blocks of IP addresses to the RIRs. RIRs are responsible for allocating IP addresses to ISPs who provide IPv4 address blocks to organizations and smaller ISPs. Organizations can also get their addresses directly from an RIR.

![RIR](assets/rir.png){: style="width:700px"}

## Network Segmentation

In an Ethernet LAN, devices use broadcasts and ARP to locate other devices. ARP sends Layer 2 broadcasts to a known IPv4 address on the local network to discover the associated MAC address. Devices on Ethernet LANs also locate other devices using services. A host typically acquires its IPv4 address configuration using DHCP which sends broadcasts on the local network to locate a DHCP server. Switches propagate broadcasts out all interfaces except the interface on which it was received.

A large broadcast domain is a network that connects many hosts. A problem with a large broadcast domain is that these hosts can generate excessive broadcasts and negatively affect the network. The solution is to reduce the size of the network to create smaller broadcast domains in a process called subnetting. These smaller network spaces are called subnets. The basis of subnetting is to use host bits to create additional subnets. Subnetting reduces overall network traffic and improves network performance. It helps administrators to implement security policies such as which subnets are allowed or not allowed to communicate together. It reduces the number of devices affected by abnormal broadcast traffic due to misconfigurations, hardware/software problems, or malicious intent.

!!!example

	=== "Without segmentation"

		![before seg](assets/beforeSeg.png){: style="width:400px"}

	=== "With segmentation"

		![after seg](assets/afterSeg.png){: style="width:500px"}




## IPv4 Issues

The depletion of IPv4 address space has been the motivating factor for moving to IPv6. IPv6 has a larger 128-bit address space, providing 340 undecillion possible addresses. 

![ipv4Exhaustion](assets/ipv4Exhaustion.png){: style="width:600px"}

When the IETF began its development of a successor to IPv4, it used this opportunity to fix the limitations of IPv4 and include enhancements. One example is ICMPv6, which includes address resolution and address autoconfiguration not found in ICMPv4.

Both IPv4 and IPv6 coexist and the transition to only IPv6 will take several years. The IETF has created various protocols and tools to help network administrators migrate their networks to IPv6. The migration techniques can be divided into three categories: 

1. Dual Stack
2. Tunneling
3. Translation 

!!!info "Migration Techniques"

	=== "Dual Stack" 

		Dual stack devices run both IPv4 and IPv6 protocol stacks simultaneously. 

		![Dual Stack](assets/dualStack.png){: style="width:600px"}

	=== "Tunneling"

		Tunneling is a method of transporting an IPv6 packet over an IPv4 network. The IPv6 packet is encapsulated inside an IPv4 packet, similar to other types of data. 

		![Tunneling](assets/tunneling.png){: style="width:600px"}

	=== "Translation"

		NAT64 allows IPv6-enabled devices to communicate with IPv4-enabled devices using a translation technique similar to NAT for IPv4. An IPv6 packet is translated to an IPv4 packet and an IPv4 packet is translated to an IPv6 packet.

		![Translation](assets/translating.png){: style="width:600px"}

## IPv6 Addressing

IPv6 addresses are 128 bits in length and written as a string of hexadecimal values. Every four bits is represented by a single hexadecimal digit; for a total of 32 hexadecimal values. 

![Hexadecimal value](assets/hexa.png){: style="width:600px"}

IPv6 addresses are not case-sensitive and can be written in either lowercase or uppercase. In IPv6, a hextet that refers to a segment of 16 bits, or four hexadecimal values. Each ```X``` is a single hextet, which is 16 bits or four hexadecimal digits. 

Preferred format means that you write IPv6 address using all 32 hexadecimal digits.

```fe80:0000:0000:0000:0123:4567:89ab:cdef```

There are two rules that help to reduce the number of digits needed to represent an IPv6 address.

### Rule 1 – Omit Leading Zeros.

You can only omit leading zeros, not trailing zeros.

!!!example

	* 01ab can be represented as 1ab
	* 09f0 can be represented as 9f0
	* 0a00 can be represented as a00
	* 00ab can be represented as ab

### Rule 2 – Double Colon. 

A double colon ```::``` can replace any single, contiguous string of one or more 16-bit hextets consisting of all zeros. 

For example, ```2001:db8:cafe:1:0:0:0:1``` (leading 0s omitted) could be represented as ```2001:db8:cafe:1::1```. 

The double colon ```::``` is used in place of the three all-0 hextets (0:0:0).

The double colon ```::``` can only be used once within an address, otherwise there would be more than one possible resulting address. If an address has more than one contiguous string of all-0 hextets, best practice is to use the double colon ```::``` on the longest string. If the strings are equal, the first string should use the double colon ```::```.

!!!example "Examples"

	![Compressed IPv6](assets/compressed.png){: style="width:600px"}

!!!question "Exercises"

	1. What is the compressed notation of the IP address: ```fd73:8800:0022:0980:0038:8000:c900:8200```?
	2. What is the compressed notation of the IP address: ```fd15:0000:0005:0000:0000:0000:0000:000a```?
	3. What is the expanded notation of the IP address: ```fda0:83:0:10:0:a0d0:41c0:2900```?	
	4. What is the expanded notation of the IP address: ```fd90:0:90:a01::6200:0```?

???question "Answers"

	1. fd73:8800:22:980:38:8000:c900:8200
	2. fd15:0:5::a
	3. fda0:0083:0000:0010:0000:a0d0:41c0:2900
	4. fd90:0000:0090:0a01:0000:0000:6200:0000

## Dynamic Addressing with DHCP

### Static and Dynamic Addressing

With a static assignment, the network administrator must manually configure the network information for a host. At a minimum, this includes the host IPv4 address, subnet mask, and default gateway. Static assignment of addressing information can provide increased control of network resources, but it can be time consuming to enter the information on each host. When using static IPv4 addressing, it is important to maintain an accurate list of which IPv4 addresses are assigned to which devices.

IPv4 addresses can be assigned automatically using a protocol known as DHCP. DHCP is generally the preferred method of assigning IPv4 addresses to hosts on large networks because it reduces the burden on network support staff and virtually eliminates entry errors. Another benefit of DHCP is that an address is not permanently assigned to a host but is only leased for a period of time. If the host is powered down or taken off the network, the address is returned to the pool for reuse.

As you enter area with a wireless hotspot, your laptop DHCP client contacts the local DHCP server via a wireless connection. The DHCP server assigns an IPv4 address to your laptop. With home networks, the DHCP server may be located at the ISP and a host on the home network receives its IPv4 configuration directly from the ISP. Many home networks and small businesses use a wireless router and modem. In this case, the wireless router is both a DHCP client and a server.

![DHCP Servers](assets/dhcp.png){: style="width:600px"}


### DHCPv4 Configuration

The DHCP server is configured with a range, or pool, of IPv4 addresses that can be assigned to DHCP clients. 

1. A client that needs an IPv4 address will send a **DHCP Discover** message which is a broadcast with a destination IPv4 address of ```255.255.255.255``` (32 ones) and a destination MAC address of ```FF-FF-FF-FF-FF-FF``` (48 ones). All hosts on the network will receive this broadcast DHCP frame, but only a DHCP server will reply. 
2. The server will respond with a **DHCP Offer**, suggesting an IPv4 address for the client. 
3. The host then sends a **DHCP Request** asking to use the suggested IPv4 address. 
4. The server responds with a **DHCP Acknowledgment**.

For most home and small business networks, a wireless router provides DHCP services to the local network clients. To configure a home wireless router, access its graphical web interface by opening the browser and entering the router default IPv4 address. The IPv4 address of ```192.168.0.1``` and subnet mask of ```255.255.255.0``` are the defaults for the internal router interface. This is the default gateway for all hosts on the local network and also the internal DHCP server IPv4 address. Most home wireless routers have DHCP Server enabled by default.