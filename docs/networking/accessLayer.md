Ethernet is technology commonly used in local area networks. Devices access the Ethernet LAN using an Ethernet Network Interface Card (NIC). 

Each Ethernet NIC has a unique address permanently embedded on the card known as a Media Access Control (MAC) address. The MAC address for both the source and destination are fields in an Ethernet frame.

## Encapsulation and the Ethernet Frame

The process of placing one message format inside another message format is called encapsulation. De-encapsulation occurs when the process is reversed by the recipient. 

A message that is sent over a computer network follows specific format rules for it to be delivered and processed.

The Ethernet protocol standards define many aspects of network communication including frame format, frame size, timing, and encoding.

!!!info "Ethernet Frame in details"

	![ethernet frame](assets/ethernetFrame.png){: style="width:500px"}

	* Preamble: used for sequencing and timing.
	* Start of Frame Delimiter (SFD): indicates to the receiving NIC that following this byte will be the actual information associated with the ethernet frame.
	* Destination MAC Address: the MAC address of the NIC where this ethernet frame is going to on this network.
	* Source MAC Address: the MAC address of the NIC that originated this ethernet frame.
	* Length or Type of Frame: length of the data (how many bytes are in the data) or information about its type (IPv4? IPv6?).
	* Encapsulated Data.
	* Frame Check Sequence (FCS): detects transmission errors.


## The Access Layer

The access layer is the part of the network in which people gain access to other hosts and to shared files and printers. 

### Ethernet Switch

When a host sends a message to another host connected to the same switched network, the switch accepts and decodes the frames to read the MAC address portion of the message. 

A table on the switch, called a MAC address table, contains a list of all the active ports and the host MAC addresses that are attached to them. 

When a message is sent between hosts, the switch checks to see if the destination MAC address is in the table. If it is, the switch builds a temporary connection, called a circuit, between the source and destination ports. 

### MAC Address Table

A switch builds the MAC address table by examining the source MAC address of each frame that is sent between hosts. When a new host sends a message or responds to a flooded message, the switch immediately learns its MAC address and the port to which it is connected. 

The table is dynamically updated each time a new source MAC address is read by the switch.

!!!example

	<iframe width="560" height="315" src="https://www.youtube.com/embed/TIHvWb-gruc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>