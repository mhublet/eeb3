# Setting Up a Home Network

In this section, we will do an activity using ```Cisco Packet Tracer``` where you will have to do the initial set up of a wireless home router. You will have to:

* connect the devices;
* configure the wireless router
* configure the IP Addressing and Test Connectivity.

## Theory

But first, a bit on theory on two important home network components, router & modem, and on a client/server protocol that we will discover in the activity, DHCP.

### Home router

A home router enables Internet and local network access. Typically, it is directly connected to a wired or wireless WAN. Users connected to the router are able to access the LAN as well as the external WAN, such as the Internet. Depending on the capabilities of the router, it can support from a few to hundreds of simultaneous users. Moreover, most wireless routers can also function as a firewall with the ability to block, monitor, and control and filter incoming and outgoing network traffic.

Small business and home routers typically have two types of ports:

* Ethernet Ports -  These ports connect to the internal switch portion of the router. These ports are usually labeled “Ethernet” or “LAN”. All devices connected to the switch ports are on the same local network.

* Internet Port - This port is used to connect the device to another network. The internet port connects the router to a different network than the Ethernet ports. This port is often used to connect to the cable or DSL modem in order to access the internet.

In addition to the wired ports, many home routers include a radio antenna and a built-in wireless access point. By default, the wireless devices are on the same local network as the devices that are physically plugged into the LAN switch ports.

![routeur](assets/routeur.png){: style="width:400px"}


### Modem

A modem is a network device that both modulates and demodulates analog carrier signals (called sine waves) for encoding and decoding digital information for processing. Modems accomplish both of these tasks simultaneously and, for this reason, the term modem is a combination of "modulate" and "demodulate."

Delivered by ISP, a modem is basically the access point to the internet.


### DHCP

Dynamic Host Configuration Protocol (DHCP) is a network protocol that allows a computer that connects to a local network to obtain its IP configuration dynamically and automatically. The main purpose is to simplify the administration of a network.

## Activity 0

* Go to Classes ICT.
* Open the file ```0. configure-a-wireless-router-and-clients.pka```.
* Follow the instructions.

