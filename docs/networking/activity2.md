# Activity

## Activity 6

In this activity, you will observe the client interaction between the server and PC.

* Go to Classes ICT.
* Open the file ```6. the-client-interaction.pka```.
* Follow the instructions.

## Activity 7

In this activity, you will observe web requests when a client browser requests web pages from a server.

* Go to Classes ICT.
* Open the file ```7. observe-web-requests.pka```.
* Follow the instructions.

## Activity 8

In this activity, you will put a file on an FTP server and get a file from an FTP server.

* Go to Classes ICT.
* Open the file ```8. use-ftp-services.pka```.
* Follow the instructions.

## Activity 9

In this activity, you will establish remote session to a router using Telnet and SSH.

* Go to Classes ICT.
* Open the file ```9. use-telnet-and-ssh.pka```.
* Follow the instructions.

## Activity 10

In this activity, you will use the ipconfig command to identify incorrect configuration on a PC.

* Go to Classes ICT.
* Open the file ```10. use-the-ipconfig-command.pka```.
* Follow the instructions.

## Activity 11

In this activity, you will use the ping command to identify an incorrect configuration on a PC.

* Go to Classes ICT.
* Open the file ```11. use-the-ping-command.pka```.
* Follow the instructions.