Communication transmits across a network on media. The media provides the channel over which the message travels from source to destination.

## Types of Media

Modern networks primarily use three types of media to interconnect devices, as shown in the figure:

* Metal wires within cables - Data is encoded into electrical impulses.
* Glass or plastic fibers within cables (fiber-optic cable) - Data is encoded into pulses of light.
* Wireless transmission - Data is encoded via modulation of specific frequencies of electromagnetic waves.

![media](assets/media.png){: style="width:500px"}

## Choosing Network Media

The four main criteria for choosing network media are these:

* What is the maximum distance that the media can successfully carry a signal?
* What is the environment in which the media will be installed?
* What is the amount of data and at what speed must it be transmitted?
* What is the cost of the media and installation?

## Most Common Network Cables


!!!info "The three most common network cables:"

	=== "Twisted-Pair Cable"

		Ethernet technology generally uses twisted-pair cables to interconnect devices. Because Ethernet is the foundation for most local networks, twisted-pair is the most commonly encountered type of network cabling.

		![twistedPair](assets/twistedPair.png){: style="width:400px"}

	=== "Coaxial Cable"

		Coaxial was one of the earliest types of network cabling developed. Coaxial cable is the kind of copper cable used by cable TV companies. 

		![coaxial](assets/coaxial.png){: style="width:400px"}

	=== "Fiber-Optic Cable"

		Fiber-optic cable can be either glass or plastic with a diameter about the same as a human hair and it can carry digital information at very high speeds over long distances. Because light is used instead of electricity, electrical interference does not affect the signal.
		
		They have a very high bandwidth, which enables them to carry very large amounts of data. Fiber is used in backbone networks, large enterprise environments, and large data centers. It is also used extensively by telephone companies.

		![fiberOptic](assets/fiberOptic.png){: style="width:400px"}