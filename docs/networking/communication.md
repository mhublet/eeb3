# Communication Principles

## Protocols

Just like humans, computers use rules, or protocols, in order to communicate. Protocols are required for computers to properly communicate across the network. In both a wired and wireless environment, a local network is defined as an area where all hosts must ```speak the same language```, which, in computer terms means they must ```share a common protocol```.

Networking protocols define many aspects of communication over the local network: message format, message size, timing, encoding (light wave, electrical pulse,...), encapsulation (header with source and destination hosts), and message patterns (ACK).

## Standards

A standard is a set of rules that determines how something must be done. Networking and internet standards ensure that all devices connecting to the network implement the same set of rules or protocols in the same manner.

An internet standard is the end result of a comprehensive cycle of discussion, problem solving, and testing. These different standards are developed, published, and maintained by a variety of organizations, as shown in the figure. 

![Standard Organisations](assets/standard.png){: style="width:400px"}

## Network communication model

Succesfull communication requires the use of several protocols, it requires a ```protocol stack``` - a layered collection of protocols that work together to provide communication services. Each protocol in the stack is responsible for a specific task.

Layered models help visualize how the various protocols work together to enable network communications. Let's have a look at two models: the TCP/IP Model and the OSI Model.

![TCP/IP vs OSI](assets/osiVStcpip.png){: style="width:500px"}

### TCP/IP Model

The TCP/IP model is a concise version of the OSI model and contains four layers:

1. Network Access: Controls the hardware devices and media that make up the network.
2. Internet: Determines the best path through the network.
3. Transport: Supports communication between various devices across diverse networks.
4. Application: Represents data to the user, plus encoding and dialog control.


!!!example

	When we are requesting or downloading a webpage, the message contains all those protocols:

	![example TCP/IP](assets/exampleTCPIP.png){: style="width:500px"}

	* Ethernet protocol - communication within the same network.
	* IP (Internet Protocol) - governs communication from original source to final destination. It makes sure the message gets there.
	* TCP (Transmission Control Protocol) - makes sure the information gets there reliabely.
	* HTTP (HyperText Transfer Protocol) - governs the exchange or transfer of HTML.


### OSI Model Layer

The OSI model contains seven layers:

1. Physical: The physical layer protocols describe the mechanical, electrical, functional, and procedural means to activate, maintain, and de activate physical connections for a bit transmission to and from a network device.
2. Data link: The data link layer protocols describe methods for exchanging data frames between devices over a common media.
3. Network: The network layer provides services to exchange the individual pieces of data over the network between identified end devices.
4. Transport: The transport layer defines services to sefment, transfer, and reassemble the data for inidividual communications between the end devices.
5. Session: The session layer provides services to the presentation layer to organize its dialogue and manage data exchange.
6. Presentation: The presentation layer provides for common representation of the data transferred between application layer services.
7. Application: The application layer contains protocols used for process to process communications.


!!!danger "TCP/IP vs OSI"

	TCP/IP is a functional model designed to solve specific communication problems, and which is based on specific, standard protocols. 

	OSI is a generic, protocol-independent model intended to describe all forms of network communication.
