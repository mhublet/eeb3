---
hide:
  - navigation
  - footer
---

### Icone: 

* <a href="https://www.flaticon.com/free-icons/gamepad" title="gamepad icons">Gamepad icons created by smalllikeart - Flaticon</a>

### Python: 

* Hoarau, S., Massart, T., & Poirier, I., "Apprendre à coder avec Python", Août 2019.
* Massart, T. "Projet Lapin Vorace", Mars 2011.
* Author, G. (2019, 19 mai). PYTHON DIARIES CHAPTER 4 → FUNCTION PART – 1. HackerEarth Blog. Consulté le 16 septembre 2022, à l’adresse https://www.hackerearth.com/blog/developers/python-diaries-chapter-4-function-part-1/
* Python Matrix and Introduction to NumPy. (s. d.). Consulté le 16 septembre 2022, à l’adresse https://www.programiz.com/python-programming/matrix
* Real Python. (2022, 18 juillet). Python GUI Programming With Tkinter. Consulté le 23 septembre 2022, à l’adresse https://realpython.com/python-gui-tkinter/
* Müller Didier, Python : objectif jeux. (s. d.). Consulté le 21 octobre 2022, à l’adresse https://www.apprendre-en-ligne.net/pj/index.html


### Mindstorm:

* Valk, L. (2014, 14 juin). The LEGO MINDSTORMS EV3 Discovery Book : A Beginner’s Guide to Building and Programming Robots (1re éd.). No Starch Press.
* Rouse, A. P. B. M. (2020, August 31). What is a Pixel? - Definition from Techopedia. Techopedia. https://www.techopedia.com/definition/24012/pixel


### Networking:

* Cisco, Networking Basics, Consulté le 16 janvier 2023, à l'adresse www.skillsforall.com


### HTML & CSS:

* Codecademy. (s. d.). HTML & CSS https://www.codecademy.com

### SQL:

* Codecademy. (s. d.). SQL https://www.codecademy.com

### Command Line:

* Shaw, Z. A. (2012). The Command Line Crash Course Controlling Your Computer From The Terminal (Version 1.0).

### Turtle:

* TechnoKids. (s.d.). TechnoTurtle Workbook.

### Scratch:

* Arcade games in Scratch. (n.d.). https://bournetocode.com/projects/7-CS-ScratchArcade/pages/1_Lesson.html

### Cryptography:

* Didier Muller, Les neuf couronnes.
* Porta Cipher https://sites.google.com/site/cryptocrackprogram/user-guide/cipher-types/substitution/porta
* Porta Table https://www.chegg.com/homework-help/questions-and-answers/ve-working-2-days-m-completely-lost-java-programming-q42899925
* Porta Example https://furkankamaci.medium.com/unlocking-the-secrets-of-cryptography-a-beginners-guide-to-breaking-ciphers-part-1-67d79c470127
