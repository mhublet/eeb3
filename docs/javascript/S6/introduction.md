## What is Javascript?

Last year, millions of learners from our community started with JavaScript. Why? JavaScript is primarily known as the language of most modern web browsers, and its early quirks gave it a bit of a bad reputation. However, the language has continued to evolve and improve. 

JavaScript is a powerful, flexible, and fast programming language now being used for increasingly complex web development and beyond!

Since JavaScript remains at the core of web development, it’s often the first language learned by self-taught coders eager to learn and build. JavaScript powers the dynamic behavior on most website.

## Console

Data is printed, or logged, to the console, a panel that displays messages, with console.log().
It’s going to be very useful for us to print values to the console, so we can see the work that we’re doing.

```javascript
console.log(5); 
```

## Comments

Programming is often highly collaborative. In addition, our own code can quickly become difficult to understand when we return to it— sometimes only an hour later! For these reasons, it’s often useful to leave notes in our code for other developers or ourselves.
We can write single-line comments with // 

```javascript
// Prints 5 to the console
console.log(5);
```

and multi-line comments between ```/*``` and ```*/```.

```javascript
/*
This is all commented 
console.log(10);
None of this is going to run!
console.log(99);
*/
```

## Data Types

Data types are the classifications we give to the different kinds of data that we use in programming. In JavaScript, there are seven fundamental data types:

* Number: Any number, including numbers with decimals: 4, 8, 1516, 23.42.
* String: Any grouping of characters on your keyboard (letters, numbers, spaces, symbols, etc.) surrounded by single quotes: ' ... ' or double quotes " ... ". Though we prefer single quotes. Some people like to think of string as a fancy word for text.
* Boolean: This data type only has two possible values— either true or false (without quotes). It’s helpful to think of booleans as on and off switches or as the answers to a “yes” or “no” question.
* Null: This data type represents the intentional absence of a value, and is represented by the keyword null (without quotes).
* Undefined: This data type is denoted by the keyword undefined (without quotes). It also represents the absence of a value though it has a different use than null.
* Symbol: A newer feature to the language, symbols are unique identifiers, useful in more complex coding. No need to worry about these for now.
* Object: Collections of related data.


## Arithmetic Operators

Basic arithmetic often comes in handy when programming.

1. Add: +
2. Subtract: -
3. Multiply: *
4. Divide: /
5. Remainder: %

## String Concatenation

Operators aren’t just for numbers! When a ```+``` operator is used on two strings, it appends the right string to the left string:

```javascript
console.log('hi' + 'ya'); // Prints 'hiya'
console.log('wo' + 'ah'); // Prints 'woah'
console.log('I love to ' + 'code.')
// Prints 'I love to code.'
```

This process of appending one string to another is called concatenation. 

## Properties

Objects, including instances of data types, can have properties, stored information. The properties are denoted with a ```.``` after the name of the object, for example: ```'Hello'.length```.

```javascript
console.log('Hello'.length); // Prints 5
```

## Methods

Objects, including instances of data types, can have methods which perform actions. Methods are called by appending the object or instance with a period, the method name, and parentheses. For example: ```'hello'.toUpperCase()```.

```javascript
console.log('hello'.toUpperCase()); // Prints 'HELLO'
console.log('Hey'.startsWith('H')); // Prints true
```

## Built-in Objects

Built-in objects, including Math, are collections of methods and properties that JavaScript provides.

```javascript
console.log(Math.random()); // Prints a random number between 0 and 1
```

To generate a random number between 0 and 50, we could multiply this result by 50, like so:

```javascript
Math.random() * 50;
```

The example above will likely evaluate to a decimal. To ensure the answer is a whole number, we can take advantage of another useful Math method called ```Math.floor()```.

```Math.floor()``` takes a decimal number, and rounds down to the nearest whole number. You can use ```Math.floor()``` to round down a random number like this:

```javascript
Math.floor(Math.random() * 50);
```