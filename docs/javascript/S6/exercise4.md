# Sleep Debt Calculator

Did you know that giraffes sleep 4.6 hours a day? We humans need more than that. If we don’t sleep enough, we accumulate sleep debt. In this exercise we’ll calculate if you’re getting enough sleep each week using a sleep debt calculator.

The program will determine the actual and ideal hours of sleep for each night of the last week.
Finally, it will calculate, in hours, how far you are from your weekly sleep goal.

To complete this exercise, you will need 4 functions:

* ```getSleepHours(day)``` should return the number of hours you slept that night of the week. For example, if you got 8 hours of sleep on Monday night, calling ```getSleepHours(‘monday’)``` should return 8. Use an if/else to implement this.
* ```getActualSleepHours()``` calls the ```getSleepHours``` function for each day of the week and returns the sum of each day sleep.
* ```getIdealSleepHours()``` should return your ideal hours per night multiplied by 7.
* ```calculateSleepDebt()``` calls the two functions ```getActualSleepHours``` and ```getIdealSleepHours```. It then compares the two amounts:
	* If actual sleep equals ideal sleep, log to the console that the user got the perfect amount of sleep.
	* If the actual sleep is greater than the ideal sleep, log to the console that the user got x hours more sleep than needed.
	* If the actual sleep is less than the ideal sleep, log to the console that the user should get some rest and sleep x hours longer.
