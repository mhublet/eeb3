## What are Functions?

When first learning how to calculate the area of a rectangle, there’s a sequence of steps to calculate the correct answer:

1. Measure the width of the rectangle.
2. Measure the height of the rectangle.
3. Multiply the width and height of the rectangle.

With practice, you can calculate the area of the rectangle without being instructed with these three steps every time.
We can calculate the area of one rectangle with the following code:

```javascript
const width = 10;
const height = 6;
const area =  width * height;
console.log(area); // Output: 60
```

Imagine being asked to calculate the area of three different rectangles:

```javascript
// Area of the first rectangle
const width1 = 10;
const height1 = 6;
const area1 =  width1 * height1;
 
// Area of the second rectangle
const width2 = 4;
const height2 = 9;
const area2 =  width2 * height2;
 
// Area of the third rectangle
const width3 = 10;
const height3 = 10;
const area3 =  width3 * height3;
```

In programming, we often use code to perform a specific task multiple times. Instead of rewriting the same code, we can group a block of code together and associate it with one task, then we can reuse that block of code whenever we need to perform the task again. 

We achieve this by creating a function. A function is a reusable block of code that groups together a sequence of statements to perform a specific task.

Take a look at the provided GIF. It shows a function, named ```addOneSide```, adding an additional side to different shape inputs. Notice how there is only one function, represented by the box, that is used to transform individual shapes (inputs) into new shapes (outputs).

![function](assets/function.gif){: style="width:500px"}

## Function Declarations

In JavaScript, there are many ways to create a function. One way to create a function is by using a function declaration. Just like how a variable declaration binds a value to a variable name, a function declaration binds a function to a name, or an identifier. Take a look at the anatomy of a function declaration below:

![function declaration](assets/declaration.png){: style="width:500px"}

A function declaration consists of:

* The function keyword.
* The name of the function, or its identifier, followed by parentheses.
* A function body, or the block of statements required to perform a specific task, enclosed in the function’s curly brackets, ```{ }```.

## Calling a Function

A function declaration binds a function to an identifier.
However, a function declaration does not ask the code inside the function body to run, it just declares the existence of the function. The code inside a function body runs, or executes, only when the function is called.
To call a function in your code, you type the function name followed by parentheses.

![calling](assets/calling.png){: style="width:500px"}

This function call executes the function body, or all of the statements between the curly braces in the function declaration.

![calling2](assets/calling2.png){: style="width:500px"}
 
We can call the same function as many times as needed.

## Parameters and Arguments

A parameter is a named variable inside a function’s block which will be assigned the value of the argument passed in when the function is invoked:

![parameter](assets/parameter.png){: style="width:500px"} 

When calling a function that has parameters, we specify the values in the parentheses that follow the function name. The values that are passed to the function when it is called are called arguments. Arguments can be passed to the function as values or variables.

![argument](assets/argument.png){: style="width:500px"}

ES6 introduces new ways of handling arbitrary parameters through default parameters which allow us to assign a default value to a parameter in case no argument is passed into the function.

Example:

```javascript
function greeting (name = 'stranger') {
  console.log(`Hello, ${name}!`)
}
 
greeting('Nick') // Output: Hello, Nick!
greeting() // Output: Hello, stranger!
``` 

When the code calls ```greeting('Nick')``` the value of the argument is passed in and, 'Nick', will override the default parameter of 'stranger' to log 'Hello, Nick!' to the console.

When there isn’t an argument passed into ```greeting()```, the default value of 'stranger' is used, and 'Hello, stranger!' is logged to the console.

## Return

To pass back information from the function call, we use a return statement. To create a return statement, we use the return keyword followed by the value that we wish to return. If the value is omitted, undefined is returned instead.

![return](assets/return.png){: style="width:500px"}

When a return statement is used in a function body, the execution of the function is stopped and the code that follows it will not be executed. Look at the example below:

```javascript
function rectangleArea(width, height) {
  if (width < 0 || height < 0) {
    return 'You need positive integers to calculate area!';
  }
  return width * height;
}
```

If an argument for width or height is less than 0, then ```rectangleArea()``` will return 'You need positive integers to calculate area!'. The second return statement width * height will not run.

The return keyword is powerful because it allows functions to produce an output. We can then save the output to a variable for later use.

## More information

We have seen how to declare functions but there are two other ways to work with functions: function expressions and arrow functions.

It’s good to be aware of the differences between function declarations, function expressions and arrow functions. As you program more in JavaScript, you’ll see a wide variety of how these function types are used.

* To define a function using function expressions:

![expression](assets/expression.png){: style="width:500px"}
 
* To define a function using arrow function notation:
 
![arrow](assets/arrow.png){: style="width:500px"}

* Function definition can be made concise using concise arrow notation:
 
![definition](assets/definition.png){: style="width:500px"}
