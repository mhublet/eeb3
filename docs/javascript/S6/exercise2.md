# Magic Eight Ball

In this project we will build a Magic Eight Ball using control flow in JavaScript.
The user will be able to input a question, then our program will output a random fortune.

1. Define a variable called userName and assign the user name of your choice
2. Define a variable userQuestion and indicate as a value the question you have for the magic ball.
3. Write a console.log() saying hello and stating what was asked (the username should be included).
4. Create another variable randomNumber and set it equal to a random number between 0 and 7.
5. Create one more variable called eightBall and set it equal to an empty string, we will change its value later on.
6. Create a control flow (if/elseif/else) that takes in the randomNumber and then assigns eightBall to a reply that a Magic Eight Ball would return. Here are 8 Magic Eight Ball phrases that we’d like to save to the variable eightBall:

	* 'It is certain'
	* 'It is decidedly so'
	* 'Reply hazy try again'
	* 'Cannot predict now'
	* 'Do not count on it'
	* 'My sources say no'
	* 'Outlook not so good'
	* 'Signs point to yes'

7.	Write a console.log to print the Magic Eight Ball’s answer.