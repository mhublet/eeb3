# Exercises

## Exercise 1

* Copy/paste the folder ```Exercise1``` from ```ClassesICT``` in your personal folder.
* Open the file ```index.html``` in a browser.
* In Notepad++, open the file ```main.js``` located in ClassesICT in the folder Exercise1.
3.	In main.js call the .querySelector() method to select the tag of h1 from the DOM. Access that element’s .innerHTML property to change the h1 title to ‘Most popular TV show searches in 2016’.
4.	Use the .getElementById method to access the element with an id of "fourth". Access that element’s .innerHTML to replace the content in the fourth item in the list to read “Fourth element”.
5.	Style the background-color of the body in the blog-post document to match the color of the Codecademy text editor, '#201F2E' by using the .style property.
6.	Refresh index.html to check your changes.


Exercise 2.	

1.	Open the file index.html located in Exercise2 in ClassesICT.
2.	In Notepad++ open the file “main.js” located in Exercise2 in ClassesICT.
3.	Create an li element using the .createElement() method and save it in a variable called newDestination.
4.	Assign the element an id of 'oaxaca'.
5.	Assign the element the text 'Oaxaca, Mexico'.
6.	Append the new element you created as the last child of the list with the ID more-destinations.
7.	Scroll to the bottom of the page in the browser to see your new element.

Let’s now delete it:
8.	Remove the element using the .removeChild() method and passing in the element with ID oaxaca.


Exercise 3.	

1.	Open the file index.html located in Exercise3 in ClassesICT.

2.	In Notepad++ open the file “main.js” located in Exercise3 in ClassesICT.

3.	Modify the body of the turnButtonRed() function so that it modifies the button as follows:
a.	Assigns the .style.backgroundColor to 'red'
b.	Assigns the style.color to 'white'
c.	Modifies the .innerHTML to 'Red Button'

4.	Add interactivity to the button element by adding the function turnButtonRed when the button is clicked.

Exercise 4.	

1.	Open the file index.html located in Exercise4 in ClassesICT.

2.	In Notepad++ open the file “main.js” located in Exercise4 in ClassesICT.

3.	Access the .firstChild of the body and save it to a variable named first. Then modify first‘s .innerHTML to:
'I am the child!'
Take a moment to note which element has now been modified.

4.	Use the .parentNode property to access the parent element of the variable first and modify its .innerHTML to:
'I am the parent and my inner HTML has been replaced!'
Take a moment to notice the change in the web page.
