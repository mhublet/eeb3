# The Document Object Model

<iframe width="560" height="315" src="https://www.youtube.com/embed/KShnPYN-voI?si=Ixq19wnGLZRxhm9d" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

The DOM is a structural model of a web page that allows for scripting languages to access that page.

The system of organization in the DOM mimics the nesting structure of an HTML document.

Elements nested within another are referred to as the children of that element. The element they are nested within is called the parent element of those elements.

The DOM also allows access to the regular attributes of an HTML element such as its style, id, etc.

The DOM interface is implemented by every modern browser.

![DOM1](assets/dom1.png){: style="width:500px"}

![DOM2](assets/dom2.png){: style="width:500px"}


