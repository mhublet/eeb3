# Introduction

## Javascript and HTML

HTML defines the structure of a web page by using page elements as the building blocks. However, HTML by itself can not produce web page interactivity, that’s where JavaScript comes in.

HTML provides structure, CSS provides style and JavaScript provides interactivity.
 
Web programmers use JavaScript to make web pages dynamic and interactive. This powerful scripting language is encapsulated in its own HTML element: the ```<script>``` element. 

You can think of this ```<script>``` element as the door to JavaScript for HTML.

## The ```<script>``` tag

The ```<script>``` element allows you to add JavaScript code inside an HTML file. Below, the ```<script>``` element embeds valid JavaScript code:

```html
<h1>This is an embedded JS example</h1>
<script>
  function Hello() {
    alert ('Hello World');
  }
</script>
```

Without the ```<script>``` tag, websites would be unclickable and a bit boring.
The ```<script>``` element, like most elements in HTML, has an opening and closing angle bracket. The closing tag marks the end of the content inside of the ```<script>``` element. Just like the ```<style>``` tag used to embed CSS code, you use the ```<script>``` tag to embed valid JavaScript code.

!!!example

    The following JavaScript code is written between the opening and closing ```<script>``` tags of the file index.html located in the example1 folder in ClassesICT:

    ```javascript
    function blooming() {
      var image = document.getElementById('myImage');
      if (image.src.match("normal")) {
        image.src = "flower.png";
      } else {
        image.src = "normal.png";
      }    
    }
    ```

    Open the file index.html and click on the EEBIII logo to see what it does.

## The ```src``` attribute

Since you know how to use a ```<script>``` element with embedded code, let’s talk about linking code. 

Linking code is preferable because of a programming concept called Separation of Concerns (SoC). Instead of having messy code that is all in the same file, web developers separate their code into different files, making each “concern” easier to understand and more convenient when changes must be made.

Instead of writing JavaScript in our HTML file, we are going to write it in its own file, and then reference this code with a file path name. We will do this using the src attribute.

If the file is in the same project folder, the ```src``` value will be a relative path name. Below is an example of a providing a relative path for a JavaScript file.

!!!example

    ```html
    <script src="exampleScript.js"></script>
    ```

    The ```<script>``` above would look for a file called exampleScript.js that is in the same folder/directory as our index.html file.

???info "More info"

    By default, scripts are loaded and executed as soon as the HTML parser encounters them in the HTML file, the HTML parser waits to load the entire script before from proceeding to parse the rest of the page elements.

    The defer attribute ensures that the entire HTML file has been parsed before the script is executed.

    The async attribute will allow the HTML parser to continue parsing as the script is being downloaded, but will execute immediately after it has been downloaded.

    The old convention was to put scripts right before the ```</body>``` tag to prevent the script from blocking the rest of the HTML content. Now, the convention is to put the script tag in the ```<head>``` element and to use the ```defer``` and ```async``` attributes.