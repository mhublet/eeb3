
Widgets are the elements through which users interact with your program. Each widget in Tkinter is defined by a class. Here are some of the widgets available:


| Widget Class      | Description                          									|
| ----------- 		| --------------------------------------------------------------------- |
| `Label`       	| A widget used to display text on the screen 							|
| `Button`       	| A button that can contain text and can perform an action when clicked |
| `Entry`    		| A text entry widget that allows only a single line of text 			|
| `Text`    		| A text entry widget that allows multiline text entry		 			|
| `Frame`    		| A rectangular region used to group related widgets or provide padding between widgets|


But there are many more. For a full list of Tkinter widgets, check out [Basic Widgets](https://tkdocs.com/tutorial/widgets.html) and [More Widgets](https://tkdocs.com/tutorial/morewidgets.html) in the TkDocs tutorial.

## Label

Label widgets are used to display text or images. The text displayed by a Label widget can’t be edited by the user. It’s for display purposes only. 

You can control the text, the colors, the width and the height of your label widget.

Copy/Paste the two below examples in Thonny's text editor, run the script and check the result.

!!!Example "Examples"

	=== "Example 1"

		```python
		import tkinter as tk
		window = tk.Tk()

		label = tk.Label(
    		text="Hello, Tkinter",
    		foreground="white",  # Set the text color to white
    		background="#34A2FE"  # Set the background color to blue
		)

		label.pack()
		window.mainloop()
		```

	=== "Example 2"

		```python
		import tkinter as tk
		window = tk.Tk()

		label = tk.Label(
    		text="Hello, Tkinter",
    		fg="white",
    		bg="black",
    		width=10,
    		height=10
		)

		label.pack()
		window.mainloop()
		```

!!!info

	The width and height are measured in text units. One horizontal text unit is determined by the width of the character 0, or the number zero, in the default system font. Similarly, one vertical text unit is determined by the height of the character 0.

## Button

Button widgets are used to display clickable buttons. You can configure them to call a function whenever they’re clicked. 

There are many similarities between Button and Label widgets. In many ways, a button is just a label that you can click. The same keyword arguments that you use to create and style a Label will work with Button widgets.

Copy/Paste the below example in Thonny's text editor, run the script and check the result.

!!!Example
	```python
	import tkinter as tk
	window = tk.Tk()

	button = tk.Button(
    	text="Click me!",
    	width=25,
    	height=5,
    	bg="blue",
    	fg="yellow",
	)

	button.pack()
	window.mainloop()
	```

## Entry

When you need to get a little bit of text from a user, like a name or an email address, use an Entry widget. It’ll display a small text box that the user can type some text into. Creating and styling an Entry widget works pretty much exactly like with Label and Button widgets.

The interesting bit about Entry widgets isn’t how to style them. It’s how to use them to get input from a user. There are three main operations that you can perform with Entry widgets:

1. Retrieving text with .get()
2. Deleting text with .delete()
3. Inserting text with .insert()

To understand them, open up a *Python Shell*, type the below example and check what it does.

```
>>> import tkinter as tk
>>> window = tk.Tk()
>>> label = tk.Label(text="Name")
>>> entry = tk.Entry()
>>> label.pack()
>>> entry.pack()
```

By now, you should have the below window visible with a ```label``` and an ```entry``` field.

![entryExample](assets/entryExample.png){: style="width:150px"}

Click inside the ```entry``` widget with your mouse and type ```Hello World```:

![entryExample2](assets/entryExample2.png){: style="width:150px"}

Now that you have some text entered, try the below commands and check what it does on your text:

```
>>> name = entry.get()
>>> name
>>> entry.delete(0)
>>> entry.delete(0, 4)
>>> entry.delete(0, tk.END)
>>> entry.insert(0, "Python")
>>> entry.insert(0, "Real ")
```

!!!info 

	Entry widgets are great for capturing small amounts of text from a user, but because they’re only displayed on a single line, they’re not ideal for gathering large amounts of text. If you want to do the same but with large amounts of text, you can use the text widget.

## Frame

Frame widgets are important for organizing the layout of your widgets in an application.

To test this widget, copy/paste the below code that creates two Frame widgets called frame_a and frame_b and try to modify it.

```python
import tkinter as tk

window = tk.Tk()

frame_a = tk.Frame()
frame_b = tk.Frame()

label_a = tk.Label(master=frame_a, text="I'm in Frame A")
label_a.pack()

label_b = tk.Label(master=frame_b, text="I'm in Frame B")
label_b.pack()

frame_a.pack()
frame_b.pack()

window.mainloop()
```

???question "What happens if you swap frame_a.pack() and frame_b.pack()?"
	Now label_b is on top. Since label_b is assigned to frame_b, it moves to wherever frame_b is positioned.

Frame widgets can be configured with a relief attribute that creates a border around the frame. You can set relief to be any of the following values:

* tk.FLAT: Has no border effect (the default value)
* tk.SUNKEN: Creates a sunken effect
* tk.RAISED: Creates a raised effect
* tk.GROOVE: Creates a grooved border effect
* tk.RIDGE: Creates a ridged effect

Feel free to test it with the below example.

!!!example

	```python
	import tkinter as tk

	border_effects = {
    	"flat": tk.FLAT,
    	"sunken": tk.SUNKEN,
    	"raised": tk.RAISED,
    	"groove": tk.GROOVE,
    	"ridge": tk.RIDGE,
	}

	window = tk.Tk()

	for relief_name, relief in border_effects.items():
    	frame = tk.Frame(master=window, relief=relief, borderwidth=5)
    	frame.pack(side=tk.LEFT)
    	label = tk.Label(master=frame, text=relief_name)
    	label.pack()

	window.mainloop()
	```

## Creating Widgets Dynamically

It is possible to create widgets dynamically using loops in order to avoid repetitive code. Try the below example:

!!!example

	```python

	import tkinter
	
	window = tkinter.Tk()
	window.title("numbers")
	window.geometry("300x200")

	for num in range(10):
    	btn = tkinter.Button(window, text=num)
    	btn.pack(side=tkinter.LEFT)

	window.configure(background="#a1dbcd")
	window.mainloop()
	```