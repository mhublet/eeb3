## Exercise 1

!!!warning "Save your code in your ```Python``` folder and call it ```exercise1```."

Write a program using widgets to create a login page to an application.

![challenge1](assets/challenge1.png){: style="width:150px"}


## Exercise 2

!!!warning "Save your code in your ```Python``` folder and call it ```exercise2```."

Write a program that creates two buttons: ```Hello``` and ```Exit```.

* When you click on the ```Hello``` button, it should write "Hello" below the buttons.

* When you click on the ```Exit``` button, it should close the window.

![exercise2](assets/exercise2.png){: style="width:150px"}

???Help

	To close a window using a button in Tkinter, you can assign ```window.destroy``` to the command attribute of the button (```window``` being the variable name of your window).

## Exercise 3

!!!warning "Save your code in your ```Python``` folder and call it ```exercise3```."

Write a program that creates a window with 3 entries and a button. It should then show the appropriate message when you click on the button:

![exercise3](assets/exercise3.png){: style="width:200px"}

## Exercise 4

!!!warning "Save your code in your ```Python``` folder and call it ```exercise4```."

Using widgets, dynamically create 10 buttons, each having a number from 0 to 9: 

![exercise4](assets/exercise4.png){: style="width:150px"}

???help
	To have the numbers horizontally aligned, you need to use the side parameter when you pack each button and specify that you want it to the left: ```button.pack(side=tkinter.LEFT)```

## Exercise 5

!!!warning "Save your code in your ```Python``` folder and call it ```exercise5```."

Create a skype like "Voice over IP" (VoIP) program that reads a list of people's names and adds them to the window with a ```Call``` button:

![exercise5](assets/exercise5.png){: style="width:200px"}

## Exercise 6

!!!warning "Save your code in your ```Python``` folder and call it ```exercise6```."

Let's improve exercise 1 and write a program checking if the username and password provided are correct. The correct username is your firstname and the correct password is ```5555```.

* If the username and password are correct, it should show the message ```correct``` in green.
* If the username and/or password are incorrect, it should show the message ```incorrect``` in red.




## Exercise 7

!!!warning "Save your code in your ```Python``` folder and call it ```exercise7```."

Write a program that simulates rolling a dice. It should have a button with the text ```Roll``` and when the user clicks the button, it should show a random number between 1 and 6.


![exercise2](assets/exercise7.png){: style="width:150px"}

## Exercise 8

!!!warning "Save your code in your ```Python``` folder and call it ```exercise8```."

Write a program converting a Farenheit temperature in a Celsius temperature.

![exercise8](assets/exercise8.png){: style="width:150px"}

## Exercise 9

!!!warning "Save your code in your ```Python``` folder and call it ```exercise9```."

Build a number guessing name

