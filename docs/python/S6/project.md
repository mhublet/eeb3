# Rock, Paper, Scissors


![projetResult](assets/projet.png){: style="width:250px"}

The goal of the project is to code a graphic version of the Rock Paper Scissors game.
We will proceed in two steps: 

1. First, we will work on the design of our window.
2. Then we will add the interactivity.

!!!info "All the images can be found on Classes ICT."

## Design

For the layout of the game, I will be using the grid method, feel free to use a different one.
The idea is to create a grid starting at (0,0) in the top left corner and finishing at (5,2) in the bottom right corner. 

![grid](assets/projet2.png){: style="width:400px"}

You should have labels in:

* (0,0) and (0,2) specifying who is playing (Human or Machine);
* (1,0) and (1,2) for the scores;
* (2,0), (2,1) and (2,2) for the actual game;
* row 3, for a short explaination.

You shoud also have five buttons in:

* (4,0), (4,1) and (4,2) for showing the three different options.
* (5,0) and (5,2), either to start over or to leave the game.


???Help
	* To add a picture in a label:

		1. Save the picture in the same folder as your code.
		2. Import it in a variable using the following code: ```rock = tk.PhotoImage(file = "rock.gif")```.
		3. Assign that variable to the image parameter when you initialise your label: ```lab1 = Label(window, image = rock)```.

	* To add a picture in a button:

		1. Same as above.
		2. Same as above.
		3. You don't add the picture when you initialise your button, you configure it after using the following code: ```button1.configure(image = rock)```. 

	* To merge 3 columns, you can use the columnspan parameter when you grid your label: ```lab1.grid(row = 3, columnspan = 3)```.



## Interactivity

Our window looks good but it doesn't do anything yet. We need to add some interactivity.

1. The buttons of rock, paper and scissors need to have a command associated to each of them so it initialise a round
2. The score should be updated at the end of each round.
3. The ```Recommencer``` button should reinitialise the game.
4. The ```Quitter``` button should close the window.


### 1 - ```playRock()```, ```playPaper()``` and ```playScissors()``` 

Those three functions will be associated with each of the three buttons.

They should each: 

* configure the label in row 2 based on the choice of the user;

```lab1.configure(image=rock)```

* call our next function ```playRound(choice)``` using 1, 2 or 3 as a choice (1 for rock, 2 for paper and 3 for scissors).

???Help

	Don't forget to link your button with your function using the ```command``` parameter when you initialise your button.

### 2 - ```playRound(choice)```

This function is our main function and will be called after the rock, the paper or the scissors button is clicked.

It should:

* specify the use two global variables: ```my_score``` and ```your_score```;
* determine the computer choice for this round by initialising a variable to a random integer between 1 and 3 (1 for rock, 2 for paper and 3 for scissors);
* update the image in the label for the computer choice accordingly;
* check who won and update the variables ```my_score``` and ```your_score``` accordingly;
* update the text of the labels where the score is indicated.


???Help
	
	To use global variables, you have to:

	* initialise them outside of your function:

	```python
	my_score = 0
	your_score = 0
	```

	* specify the use of the global variables at the beginning of the function where you want to use them:

	```global my_score, your_score```

	To initialise a variable with a random integer between 1 and 3, you have to import the module ```random``` at the beginning of your code and then add:

	 ```myVariable = random.randint(1,3)```

	 To update an image or a text in a label, you have to use the configure method:

	 ```label1.configure(image = rock)```

	 ```label1.configure(text = "3")```

### 3 - ```reinit ()```

This function should reinitialise the labels of the current round to nothing and the scores to 0.

To do it, you need to specify that you will be using global variables (```global my_score, your-score, scoreLabel1, scoreLabel2, label1, label2```) and then configure the appropriate labels to replace their image or text.

### 4 - exit

To close a window with a button, you can just put ```window.destroy``` as a command when you initialise the button.