
Let's learn how to bring your applications to life by performing actions whenever certain events occur.

## Events and Event Handlers

When you create a Tkinter application, you must call ```window.mainloop()``` to start the event loop. During the event loop, your application checks if an event has occurred. If so, then it’ll execute some code in response.

In Tkinter, you write functions called event handlers for the events that you use in your application.

!!!example
	```python
	import tkinter as tk

	# Create a window object
	window = tk.Tk()

	# Create an event handler
	def handle_keypress(event):
	    """Print the character associated to the key pressed"""
	    print(event.char)

	# Run the event loop
	window.mainloop()
	```

!!!question "How does Tkinter know when to use handle_keypress()?" 
	```.mainloop()``` takes care of a lot for you, but there’s something missing from the above code. Tkinter widgets have a method called ```.bind()``` for just this purpose.


## Using ```.bind()```

To call an event handler whenever an event occurs on a widget, use ```.bind()```. The event handler is said to be bound to the event because it’s called every time the event occurs. Let's continue our previous example and use ```.bind()``` to bind ```handle_keypress()``` to the keypress event:

```python
import tkinter as tk

window = tk.Tk()

def handle_keypress(event):
    """Print the character associated to the key pressed"""
    print(event.char)

# Bind keypress event to handle_keypress()
window.bind("<Key>", handle_keypress)

window.mainloop()
```

Here, the ```handle_keypress()``` event handler is bound to a ```"<Key>"``` event using ```window.bind()```. Whenever a key is pressed while the application is running, your program will print the character of the key pressed.


```.bind()``` always takes at least two arguments:

1. An event that’s represented by a string of the form ```"<event_name>"```, where event_name can be any of Tkinter’s events.
2. An event handler that’s the name of the function to be called whenever the event occurs.

The event handler is bound to the widget on which ```.bind()``` is called. When the event handler is called, the event object is passed to the event handler function.

In the example above, the event handler is bound to the window itself, but you can bind an event handler to any widget in your application. For example, you can bind an event handler to a Button widget that will perform some action whenever the button is pressed:

```python
def handle_click(event):
    print("The button was clicked!")

button = tk.Button(text="Click me!")

button.bind("<Button-1>", handle_click)
```

!!!info

	You can bind any event handler to any kind of widget with ```.bind()```, but there’s a more straightforward way to bind event handlers to button clicks using the Button widget’s ```command``` attribute.

## Using ```command```

Every Button widget has a command attribute that you can assign to a function. Whenever the button is pressed, the function is executed.

Take a look at the below example to see how it works:

```python
import tkinter as tk

def increase():
    value = int(lbl_value["text"])
    lbl_value["text"] = value + 1

def decrease():
    value = int(lbl_value["text"])
    lbl_value["text"] = value - 1

window = tk.Tk()


btn_decrease = tk.Button(master=window, text="-", command=decrease)
btn_decrease.pack()

lbl_value = tk.Label(master=window, text="0")
lbl_value.pack()

btn_increase = tk.Button(master=window, text="+", command=increase)
btn_increase.pack()

window.mainloop()
```

* This code creates a window with two buttons and a label widget that holds a numeric value. The top button will be used to decrease the value in the label, and the bottom one will increase the value.

* Label widgets don’t have ```.get()``` like Entry and Text widgets do. However, you can retrieve the text from the label by accessing the text attribute with a dictionary-style subscript notation.

* The ```increase()``` function gets the text from ```lbl_value``` and converts it to an integer with ```int()```. Then, it increases this value by one and sets the label’s text attribute to this new value. The ```decrease()``` function decreases the label value by one.

* To connect the buttons to the functions, we assign the function to the button’s command attribute.