# Exercises

## Exercise 14

!!!warning "Save your code in your ```Python``` folder and call it ```exercise14```."

I want to use a dictionary to store the grades of different students. I want you to help me creating, accessing, modfying, adding & removing grades.

Here are the grades of my students:

| Name      |    Grades                       |
| ----------- | ------------------------------------ |
| "Alice"       | 92 |
| "Bob"     | 85 |
| "Charlie"   | 78 |
| "David"   | 95 |
| "Eve"   | 88 |

* Create a dictionary and initialise it with the combination of names and grades above.
* Print the grade of Alice and the grade of Charlie.
* Add a new student ```Franck``` to the dictionary. His grade is ```89```.
* Change the grade of Bob, his grade is ```87```.
* Eve just left the school, remove her from the dictionary.
* Using ```for``` and ```in```, display all the students grade.

???question "Help"

	* To create a dictionary: ```dico = {"a": 1, "b":2}```.
	* To get an element using the key ```a```: ```dico["a"]```.
	* To add a pair key/value: ```dico["c"] = 3```.
	* To modify a value: ```dico["a"] = "hello"```.
	* To remove a key/value: ```del dico["a"]```.
	* To display a dictionary:
	```python
	for key in dico:
		print(key, ":", dico[key])
	```


## Exercise 15

!!!warning "Save your code in your ```Python``` folder and call it ```exercise15```."

Create a Python program that takes a text as input (a string of words separated by spaces) and counts the frequency of each word. Then, store the word frequencies in a dictionary and print out the result. 

!!!Example "Example"

	```python
	>>> %Run exercise15.py
	give me some words seperated by space: this is a simple example this is a test
	'this': 2
	'is': 2
	'a': 2
	'simple': 1
	'example': 1
	'test': 1
	>>>
	```

???question "Help"

	* To split text into words: ```words = text.split(" ")```.
	* You need to initialize an empty dictionary.
	* Then you need to create a for loop where you check each word in the list ```words```. If the word is already in the dictionary, you need to increase the value by one. If the word is not yet in the dictionary, you need to add it, with the word as a key and 1 as a value.
