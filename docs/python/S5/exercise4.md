# Exercises

## Exercise 7

!!!warning "Save your code in your ```Python``` folder and call it ```exercise7```."

Write a program that:

* creates a first list ```[2,8,5,4,6,7,8,5]```
* creates a second list ```[3.5, 'coucou', 8, 'hello', 6.2]```
* inserts the value ```3``` in the first list between the values ```2``` and ```8```
* adds the value ```72``` at the end of the second list
* removes ```'coucou'``` from the second list
* combines both lists using the ```extend``` method
* prints the final list
* prints the value at index ```5``` of the list
* prints the 4 first elements of the list
* prints the 3 last elements of the list
* prints the elements starting at index ```2``` and ending at index ```5``` included

At the end of the exercise, you should get the following output:

```output
>>> %Run Ex7.py
[2, 3, 8, 5, 4, 6, 7, 8, 5, 3.5, 8, 'hello', 6.2, 72]
6
[2, 3, 8, 5]
['hello', 6.2, 72]
[8, 5, 4, 6]
>>> 
```

???question "Help"

	* To create a list, assign its content to a variable: ```variable = [1,2,3]```.
	* ```my_list.insert(2, 'c')``` will insert the letter 'c' at the index 2 of my_list.
	* ```my_list.append(56)``` will add 56 at the end of my_list.
	* ```my_list1.extend(my_list2)``` will insert my_list2 at the end of my_list1.
	* ```del(my_list[1])``` will remove the element at the index 1 from my_list.
	* ```my_list.remove(32)``` will remove the element 32 from my_list.
	* You can directly access an element from a list using the index method: ```my_list[index]``` and you can modify it.
	* It is possible to select part of a list by writing: ```my_list[start_index:end_index]```

		* If you don’t specify the ```start_index```, Python will assume that it is 0.
		* If you don’t specify the ```end_index```, Python will assume that it is the last index of the list.
	

## Exercise 8

!!!warning "Save your code in your ```Python``` folder and call it ```exercise8```."

Write a function ```total(list_number)``` that receives a list of number and returns the sum of all those numbers. For example, if the list given in parameter is ```[1,2,3]```, it should return ```6```.

Then write a program that: 

* Repeatedly asks numbers to the user and add them to a list until the user enters ```stop```. 
* Once ```stop``` is entered, calls the function ```total(list_number)``` with the list of number as parameter.
* Prints the result returned by the function.

???question "Help"

	* To repeatedly ask a number to the user until the user enters ```stop```, you can use the following code:

	```python
	listOfNumbers = []
	askAgain = True
	while askAgain == True:
		answer = input("Give me a number (or write stop if you are done): ")
		if answer == "stop":
			askAgain = False
		else:
			listOfNumbers.append(int(answer))
	```

	???question "Show the complete answer"

		```python
		def total(list_numbers):
			total = 0
			for number in list_numbers:
				total += number
			return total

		listOfNumbers = []
		askAgain = True
		while askAgain == True:
			answer = input("Give me a number (or write stop if you are done): ")
			if answer == "stop":
				askAgain = False
			else:
				listOfNumbers.append(int(answer))

		print(total(listOfNumbers))
		```		



## Exercise 9

!!!warning "Save your code in your ```Python``` folder and call it ```exercise9```."

Write a function ```largestNumber(list_number)``` that receives a list of number and returns the largest number from the list. For example, if the list given in parameter is ```[1,2,10,3]```, it should return ```10```.

Then write a program that: 

* Repeatedly asks numbers to the user and add them to a list until the user enters ```stop```. 
* Once ```stop``` is entered, calls the function ```largestNumber(list_number)``` with the list of number as parameter.
* Prints the result.

???question "Help" 

	To get the largest number from a list, you should:

	* Initialise a variable to 0;
	* Go through the list using a ```for``` loop;
	* At each cycle of the loop check if the current item is bigger than the value saved in your variable. If it is, replace the value in the variable by the current value, if it isn't, do nothing.

	The largest number from the list will be saved in the variable.


## Exercise 10

!!!warning "Save your code in your ```Python``` folder and call it ```exercise10```."

Write a function ```size(list_strings)``` that receives a list of strings and returns how many of those strings are 5 or more letters. For example, if the list given in parameter is ```['abcdef', 'xyz', 'aba', '1221221221']```, it should return ```2```.

Then write a program that: 

* Repeatedly asks words to the user and adds them in a list until the user enters ```stop```. 
* Once ```stop``` is entered, calls the function ```size(list_strings)``` with the list of words as parameter.
* Prints the result.

???question "Help"
	
	To check the length of a string, you use the ```len()``` function.


## Exercise 11

!!!warning "Save your code in your ```Python``` folder and call it ```exercise11```."

Write a function ```checkZero(list_number)``` that receives a list of number and returns ```True``` if there is a zero in the list, ```False``` otherwise. For example, if the list given in parameter is ```[1,2,3]```, it should return ```False```. If the list is ```[3,6,9,0,3]```, it should return ```True```.

Then write a program that: 

* Repeatedly asks numbers to the user and add them to a list until the user enters ```stop```. 
* Once ```stop``` is entered, calls the function ```checkZero(list_number)``` with the list of number as parameter.
* Prints the result.


## Exercise 12

!!!warning "Save your code in your ```Python``` folder and call it ```exercise12```."

Write a function ```occurrences(list_letters, letter)``` that receives a list of letters and a letter as parameters and returns two things: how many times the letter appears in the list of letters and a list with the indexes where it appears. For example, if the list given in parameter is ```[“a”,”e”,”b”,”a”]``` and the letter given in parameter is ```“a”```, it should return: ```2``` and ```[0,3]```

Then write a program that: 

* Repeatedly asks letters to the user and add them to a list until the user enters ```stop```.
* Once ```stop``` is entered, asks the user to pick one letter. 
* Calls the function ```occurrences``` with the list of letters and the letter as parameters.
* Prints the following:

```output
The letter appears 2 times.
It appears at the following index: [0, 3].
``` 

???question "Help"
	To return more than one variable, you can write the return like that: ```return (variable1, variable2)```.
	When you call your function, you will save whatever is returned in a variable, for example ```result```.
	Then you can get the value saved in ```variable1``` by writing ```result[0]``` and the value saved in ```variable2``` by writing ```result[1]```.
	
	!!!Example

		```python
		def example():
			a = 8
			b = "coucou"
			return (a,b)

		x = example()
		print (x[0]) # will print 8
		print (x[1]) # will print "coucou"
		```
