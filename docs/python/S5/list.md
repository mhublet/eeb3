
Lists are sequences. It is an object that can contain other objects, whatever their type is. You can have lists of integers [1, 2, 50, 2000, …], lists of floats, lists of string or lists containing all of those types.

## Create

There are two methods to create an empty list. You can either use the ```list()``` function, or you can just initialise a variable to an empty list using ```[]``` since those are list delimiters in Python.

You can also create a list that is not empty and directly indicate the objects that it contains.


!!!Example "Examples"
	=== "Example 1"
		```python
 		my_list = list()
 		```
 		We create an empty list.
 	=== "Example 2"
 		```python
 		my_list = []
 		```
 		We create an empty list.
 	=== "Example 3"
		```python
		my_list = [1,2,3,4,5]
		``` 
		We create a list with 5 integers going from 1 to 5.
	=== "Example 4"
		```python
		my_list = [3.5,1,"hello",[]]
		``` 
		We create a list containing 4 different objects: a float, an integer, a string and a new empty list.


## Insert

There are three different ways to insert elements in a list.

### append

We use the method ```append``` to add an element at the end of the list. We just need to give the element we want to add as a parameter.

### insert

We use the method ```insert``` to add an element where we want in the list. This time, we need to give the element we want to add and where we want to add it as parameters.

### extend

We use the method ```extend``` to concatenate two lists (or more) into one. You just need to apply the method on the list you want to extend (my_list1) and give the list you want to add as a parameter. You can also achieve the same result using the “+” operator.

!!!Example "Examples"
	=== "append"

		```python
		my_list = [1,2,3]
 		my_list.append(56)
		print(my_list)
		```

		We add 56 at the end of the list. This code will print: [1, 2, 3, 56].

	=== "insert"

		```python
		my_list = ['a','b','d','e']
		my_list.insert(2, 'c')
		print(my_list)
		```

		We insert ```c``` at the index 2. This code will print: ['a', 'b', 'c', 'd', 'e'].
 	
 	=== "extend"

		```python
		my_list1 = [3,4,5]
		my_list2 = [8,9,10]
		my_list1.extend(my_list2)
		```

		We extend my_list1 by inserting my_list2 at the end of it. This code will print: [3, 4, 5, 8, 9, 10].

		The below code would give us the same result:

		```python 
		my_list1 = [3,4,5]
		my_list1 = my_list1 + my_list2
		```
 
## Delete


There are two different ways to remove an element from a list.

### del 

When using the ```del``` key word, you need to indicate as a parameter the index of the element you want to delete.

### remove

When using the ```remove``` method, you need to indicate as a parameter the element you want to delete.

!!!Example "Examples"
	=== "del"
		```python
		my_list = [-5,-2,1,4,7,10]
		del my_list[0]
		print(my_list) # will print [-2, 1, 4, 7, 10]
		del my_list[2] 
		print(my_list) # will print [-2, 1, 7, 10]
		```

		We delete the first element of the list and then the third element of the list.
 	=== "remove"
 		```python
		my_list = [31,32,33,34,35]
		my_list.remove(32)
		print(my_list) # will print [31, 33, 34, 35]
		``` 


## Select and Modify

Let’s now see how we can access the different elements of a list.

You access elements from a list the same way you access characters from a string: you indicate between ```[]``` the index of the element you want. The big difference is that an element in a string can’t be changed while you can change an element in a list.

To select part of a list, you can use the same method we learned to select part of a String.

```output
>>> my_list = ['c','f','m']
>>> my_list[0]
'c'
>>> my_list[2]
'm'
>>> my_list[1] = 'Z'
>>> my_list
['c', 'Z', 'm']
>>> 
>>> my_list = ["a", 3, "coucou", 7, 10]
>>> my_list[1:3]
[3, 'coucou']
>>> my_list[2:4]
['coucou', 7]
>>> my_list[:3]
['a', 3, 'coucou']
>>> my_list[2:]
['coucou', 7, 10]
>>> 
```


!!!Summary

	* You can directly access an element from a list using the index method: ```my_list[index]``` and you can modify it.
	* It is possible to select part of a list by writing: ```my_list[start_index:end_index]```

		* The ```end_index``` is not included in the selection.
		* If you don’t specify the ```start_index```, Python will assume that it is 0.
		* If you don’t specify the ```end_index```, Python will assume that it is the last index of the list.


## ```for``` and ```in``` keywords

When we want to go through each element of the list, the easiest way to do it is to use a ```for``` loop.

For example, I would like to print each element from my list on a separate row. I can use a ```for``` loop with the ```in``` keyword to go through my list and do this:

```python
people = ["Gertrude", "Marc", "Pierre", "Gaetan", "Jacques", "Sancho"]
for elem in people:
	print(elem)

```

The value of the control variable ```elem``` will be first Gertrude, then Marc, etc,… So if I print each time ```elem```, I will print all the elements from the list.

```output
Gertrude
Marc
Pierre
Gaetan
Jacques
Sancho
```

Another interesting use of the ```in``` keyword is when you want to check if a specific element is in your list.

For example, let's check if ```Thomas``` is already in my list of people. If he is not, let's add him:

```python
people = ["Gertrude", "Marc", "Pierre", "Gaetan", "Jacques", "Sancho"]
if "Thomas" in people:
	print("This name is already in the list.")
else:
	print("This name is not yet in the list, let's add it.")
	people.append("Thomas")
```
 

