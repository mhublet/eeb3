# Exercises

* Create a folder called ```Python``` on your H drive, you will save all your exercises there. 

* Use ```Thonny``` for writing your code.

## Exercise 1

!!!warning "Save your code in your ```Python``` folder and call it ```exercise1```."

Define a function called ```multiplication``` that takes two numbers as parameters and returns the multiplication of those two numbers.

Then write a program:

* Asking two numbers to the user.
* Calling the function ```multiplication``` using the input of the user as parameters and saving the returned value in a variable result.
* Printing the value from the variable result.

???question "Help"

	* Remember the syntax of a function:
	```python
	def name(parameters_seperated_by_comma):
		instructions
		return something
	```
	Here the name is ```multiplication```, the parameters are two numbers (for example a and b), the instructions are the multiplication of those two numbers (for example c = a * b) and it should return the result (for example c).

	* Ask two numbers to the user using the input function twice:
	```number = int(input("question"))```

	* Call the ```multiplication``` function using the number received by the user as parameters:
	```multiplication(number1, number2)```

	* Don't forget to save the returned value (that will be replacing ```multiplication(number1,number2)```) in a variable result:
	```result = multiplication(number1, number2)```

	* Print the result:
	```print(result)```

	???Tip "Answer"

		```python
		def multiplication(a,b):
			c = a * b
			return c

		number1 = int(input("Give me a first number: "))
		number2 = int(input("Give me a second number: "))

		result = multiplication(number1, number2)
		print(result)
		```

## Exercise 2

!!!warning "Save your code in your ```Python``` folder and call it ```exercise2```."

Define a function called ```draw``` that will draw a specific structure based on its parameter.

For example, if the parameter of your function is 4, the output should be:

```output
>>> %Run exercise2.py
*
**
***
****
>>> 
```

If the parameter of your function is 2, it should print:

```output
>>> %Run exercise2.py
*
**
>>>
```

Then, write a program:

* Asking the user how big he wants the drawing.
* Calling the function ```draw``` using the input of the user.

???question "Help"

	* Define a function ```draw``` with one parameter:

	```python
	def name(parameter):
	```

	* Create a loop inside the draw function and at each iteration, it should print the right number of * (check <a href="https://mhublet.gitlab.io/eeb3/python/S4/loops/#for-loop" target="_blank">here</a> how to create a ```for``` loop with the ```range``` function).

	* Ask a number to the user using the input function:
	```number = int(input("question"))```.

	* Call the ```draw``` function: ```draw(number)```.


## Exercise 3

!!!warning "Save your code in your ```Python``` folder and call it ```exercise3```."

Define a function ```two_equals(a,b,c)``` that receives three numbers in parameters. It should return the boolean value ```True``` if at least two of those numbers have the same value, ```False``` if not.

Then, write a program reading three integers ```x```, ```y```, ```z``` as input and showing the results of executing the function ```two_equals(x,y,z)```.


## Exercise 4

!!!warning "Save your code in your ```Python``` folder and call it ```exercise4```."

Define a function ```divisible(a,b)``` that receives two numbers in parameters. It should return the boolean value ```True``` if a is divisible by ```b```, ```False``` if it is not.

Then, write a program reading two integers ```x```, ```y``` as input and showing the results of executing the function ```divisible(x,y)```.

???question "Help"

	To check if one number is divisible by another number, we use the modulus operator ```%```.

	The modulus operation finds the remainder after Euclidian division of one number by another.

	If there is no remainder, it is divisible.

	```python
	if a % b == 0:
		print("a is divisible by b")
	else:
		print("a is not divisible by b")
	```