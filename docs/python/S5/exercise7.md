# Exercises

## Exercise 5

!!!warning "Save your code in your ```Python``` folder and call it ```exercise5```."

Write a function ```concatUpper(a,b)``` that receives two strings as parameters and returns the two strings concatenated and in upper case.

Then, write a program:

* asking two different sentences to the user;
* calling the functions ```concatUpper``` with those two sentences as parameters;
* printing the result.

???question "Help"

	* To concatenate, you can use the ```+``` sign.
	* To put a string in uppercase, you can use the method ```.upper()```.

## Exercise 6

!!!warning "Save your code in your ```Python``` folder and call it ```exercise6```."

Write a function ```ingly(word)``` that receives one word as a parameter. 

* If the word is not ending with ```ing``` and is more than 3 letters, the function returns the same word with ```ing``` at the end. 
* If the word is more than 3 letters but is already ending with ```ing```, it should add ```ly```instead. 
* If the text is less than three letters, it should return it unchanged.

Then, write a program:

* asking a word to the user;
* calling the function ```ingly``` with the word as a parameter;
* printing the result.

!!!Example "Examples"

	=== "Example 1"
		```output
		>>> %Run Ex6.py
		Give me a word
		play
		playing
		>>> 
		```
	=== "Example 2"
		```output
		>>> %Run Ex6.py
		Give me a word
		go
		go
		>>> 
		```
	=== "Example 3"
		```output
		>>> %Run Ex6.py
		Give me a word
		string
		stringly
		>>> 
		```

???question "Help"
	
	* To get the last three letters of a variable, you can write: ```variable[-3:]```.
	* To get the length of a word, you can use the function ```len```: ```len(word)```.
