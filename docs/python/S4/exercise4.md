# Exercises

* Use ```Thonny``` for writing your code.

## Exercise 12

!!!warning "Save your code in your ```Python``` folder and call it ```exercise12```."

Write a program that prints values from 0 to 100 using a ```for``` loop.

???question "Help"
	Go back to the for loop section of the course and check the first example for the ```range``` function.

## Exercise 13

!!!warning "Save your code in your ```Python``` folder and call it ```exercise13```."

Write a program that sums the numbers between 100 and 200 using a ```for``` loop and prints the final result.

???question "Help"

	* Initialise a variable ```total``` to 0.
	* Use a ```for``` with a ```range``` function to create a sequence going from 100 to 200 included (see example 2 in range function section).
	* Inside the for loop add the iterator to the ```total``` variable.


## Exercise 14

!!!warning "Save your code in your ```Python``` folder and call it ```exercise14```."

Write a program printing the multiplication table of 3 using a loop (you can choose the one you prefer).

???question "Help"

	* Use a ```for``` with a range function to create a sequence going from 1 to 10 included.
	* Print the calculation and the result:
	```python
	print(iterator, " * ", 3, " = ", iterator * 3)
	```

## Exercise 15

!!!warning "Save your code in your ```Python``` folder and call it ```exercise15```."

Write a program that asks for a number n as an input and prints a square with n “X” per side. 

For example, if the number n is 6, the result should be:

```output
>>> %Run exercise8.py
XXXXXX
XXXXXX
XXXXXX
XXXXXX
XXXXXX
XXXXXX
>>> 
```

???question "Help"

	* Define a variable ```n``` and ask the user the size of the square, using the input function. You want to receive an integer.

    ```python
    variable_name = int(input("question"))
    ```
    
    * Create a while or a for loop going from 0 to (n-1).
    * Each time, it should ```print(n * "X")```