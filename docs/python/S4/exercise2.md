# Exercises

* Use ```Thonny``` for writing your code.

## Exercise 9

!!!warning "Save your code in your ```Python``` folder and call it ```exercise9```."

Write a program that prints values from 0 to 100 using a ```while``` loop.

???question "Help"
	Go back to the loop section of the course and check the first example for the ```while``` loop.

## Exercise 10

!!!warning "Save your code in your ```Python``` folder and call it ```exercise10```."

Write a program that sums the numbers between 100 and 200 using a ```while``` loop and prints the final result.

???question "Help"

	* Initialisation: one variable ```count``` to 100 and another variable ```total``` to 0.
	* Condition: the variable ```count``` is smaller or equal to 200.
	* Processing: add the variable ```count``` to the variable ```total```.
	* Change: increase the variable ```count``` by 1.

## Exercise 11

!!!warning "Save your code in your ```Python``` folder and call it ```exercise11```."

Using ```input``` and ```while```, write a program that repeatedly asks the user how he is doing and prints a message until the user enters stop.

???question "Help"
	
	* Initialisation: set a variable equal to something (anything except "stop")
	* Condition: variable is different than "stop".
	* Processing: print a message.
	* Change: use ```input``` to change your variable and ask how the user is doing.