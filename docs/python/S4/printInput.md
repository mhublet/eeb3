Until now, we’ve been interacting directly in Python’s interpreter. We can keep using it from time to time to test some specific code but as of now, we will program in the text editor and only then execute it in the interpreter. 

## Script

We are going to create a script that calculates the perimeter and the area of a rectangle based on its length and width.

1. Open Thonny.
2. Write the below code in the script window (above the interpreter).
```python
length = 5
width = 3
perimeter = 2 * width + 2 * length
area = width * length
(area, perimeter)
```
3. Save your file and name it ```rectangle.py```.
4. Go to the tab ```Run``` and click on ```Run current script```

    The below result should appear in the interpreter:

    ```output
    >>> %Run rectangle.py
    >>> 
    ```

    It does not show an error, everything worked well.


!!!note "Question"
    
    Why do we not see any result? Should we not see the value of the perimeter and the area of the rectangle?

???note "Answer"

     As of the moment we use scripts, if we want to communicate with the user and show results on the screen, we need to use an output function: ```print()```.


## Output

The ```print()``` function prints whatever is written between the parenthesis to the screen. Let's improve our previous script so we can show to the user the results of our calculations. 

1. Update your code:

    ```python
    length = 5
    width = 3
    perimeter = 2 * width + 2 * length
    area = width * length
    print(area, perimeter)
    ```

    !!!info

        No need to rewrite everything, just add ```print``` at the beginning of row 5.

2. Go to the tab ```Run``` and click on ```Run current script```.

    The below result should appear in the interpreter:

    ```output
    >>> %Run rectangle.py
    15 16
    >>>  
    ```

3. Let's add some text in our ```print``` so the message is clearer.

```python
    length = 5
    width = 3
    perimeter = 2 * width + 2 * length
    area = width * length
    print("Area:", area, "Perimeter:", perimeter)
```

Very nice, you can now easily share information with the user.

!!!note "Question"
 
    What if we want the user to choose the length and width of the rectangle?

???note "Answer"

    If we want the user to communicate with the computer and give some information, we need to use an input function: ```input()```


## Input

The function ```input()``` will allow our script to ask a question to the user. It will interrupt the execution of the program and wait for the input from the user. Then you can save the given answer in a variable and reuse it later on.

```input()``` accepts one optional parameter: the message that will be shown to the user. 

The type of data returned by this function will always be a string. If you want to receive a float or an integer from the user, you have to specify it.


!!!example 

    === "Integer"

        ```python
        numberOfChildren = int(input("How many children do you have? "))
        ```

    === "Float"

        ```python
        temperature = float(input("What is the temperature? "))
        ```

    === "String"

        ```python
        name = input("What is your name? ")
        ```


Let's improve our previous script by asking the width and length to the user.

1. Update your code:

    ```python
    length = float(input("Give me a length: "))
    width = float(input("Give me a width: "))
    perimeter = 2 * width + 2 * length
    area = width * length
    print("Area:", area, "Perimeter:", perimeter)
    ```

    !!!info

        No need to rewrite everything, just add the input on the right side of the assignation ```=``` for length and width.

2. Go to the tab ```Run``` and click on ```Run current script```.

    The below result should appear in the interpreter:

    ```output
    >>> %Run rectangle.py
    Give me a length: 8
    Give me a width: 9
    Area: 72.0 Perimeter: 34.0
    >>> 
    ```