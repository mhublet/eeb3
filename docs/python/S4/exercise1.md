# Exercises

* Use ```Thonny``` for writing your code.

## Exercise 6

!!!warning "Save your code in your ```Python``` folder and call it ```exercise6```."

Write a program checking if the user is more or less than 12 years old. It should:

* Ask the user his age.
* Check if he is older than 12 years old.
* If he is, print the message: ```You are getting old...``` and if he is not, print the message: ```You are a child.```

Run your code and check with different inputs the results in the interpreter.

???question "Help"
    * Define a variable age and ask the user what is his age, using the input function. You want to receive an integer.

    ```python
    variable_name = int(input("question"))
    ```
    
    * Write your conditional statement:
    

    ```python
    if condition:
        instructions
    else:
        instructions
    ```
     with ```age > 12``` as a condition and ```print("message")``` as an instruction.
 

## Exercise 7

!!!warning "Save your code in your ```Python``` folder and call it ```exercise7```."

Write a program acting like a thermostat. It should:

* Ask what is the current temperature.
* Ask the user his ideal temperature.
* Check if the current temperature is higher, lower or equal to the ideal temperature.
* If the current temperature is higher, it should print a message asking to decrease the temperature. If it is lower, it should print a message asking to increase the temperature. If it is the same, it should print nothing.

Run your code and check with different inputs the results in the interpreter.

???question "Help"
    * Define a variable it (for ideal temperature) and ask the user what is his ideal temperature, using the input function. You want to receive a float.
    * Define a variable ct (for current temperature) and ask the user what is the current temperature, using the input function. You want to receive a float.

    ```python
    variable_name = float(input("question"))
    ```

    * Write your conditional statement:

    ```python
    if condition:
        instructions
    elif condition:
        instructions
    else:
        instructions
    ``` 

    where ```condition``` needs to be replaced by a comparison between ct and it (>, < or ==) and ```instructions``` need to be replaced by a print (```print("message")```).

## Exercise 8

!!!warning "Save your code in your ```Python``` folder and call it ```exercise8```."

Write a program checking if you have enough money to buy a specific item. It should:

* Ask the user how much money he has.
* Ask the price of the item.
* Check if the user has enough money to buy the item.
* If he does, it should print how much money he would have left after the purchase, if he doesn't, it should tell the user how much more money he would need. 

Run your code and check with different inputs the results in the interpreter.

???question "Help"
    * Define a variable money and ask the user how much money he has, using the input function. You want to receive a float.
    * Define a variable item and ask the user how much the item he wants to buy cost, using the input function. You want to receive a float.

    ```python
    variable_name = float(input("question"))
    ```

    * Write your conditional statement:

    ```python
    if condition:
        instructions
    elif condition:
        instructions
    else:
        instructions
    ``` 

    where ```condition``` needs to be replaced by a comparison between money and item (>, < or ==) and ```instructions``` need to be replaced by a print (```print("message")```).

    * You can use the print function to show the result of a calculation between to variables. You can also use a single print to show, at the same time, a message and the result of a calculation between two variables.

    ```python
    print(var1 - var2)
    print("The result of var1 - var2 is: ", var1 - var2)
    ```
    Don't hesitate to test this in the interpreter (Shell).