In Python, we use the below arithmetic operators.

![arithmetic operators](https://d1uvxqwmcz8fl1.cloudfront.net/tes/resources/11508930/9e973af0-e96e-41f9-985b-b5898a6ede5d/image?width=500&height=500&version=1486669095253){: style="width:400px"}
 
## Modulus %

The modulus operation finds the remainder after Euclidian division of one number by another.


!!!example "Examples"
	
	=== "Example 1"

		<span style="color:green">10</span> % <span style="color:red">4</span> = ?

		<span style="color:green">10</span> / <span style="color:red">4</span> = <span style="color:blue">2</span>.~~5~~

		<span style="color:blue">2</span> * <span style="color:red">4</span> = <span style="color:orange">8</span>

		<span style="color:green">10</span> – <span style="color:orange">8</span> = 2

		**10 % 4 = 2**

	=== "Example 2"

		<span style="color:green">27</span> % <span style="color:red">16</span> = ?

		<span style="color:green">27</span> / <span style="color:red">16</span> = <span style="color:blue">1</span>.~~6875~~

		<span style="color:blue">1</span> * <span style="color:red">16</span> = <span style="color:orange">16</span>

		<span style="color:green">27</span> – <span style="color:orange">16</span> = 11

		**27 % 16 = 11**


## Quotient

The quotient operation keeps only the truncated result of the division.

!!!example "Examples"

	=== "Example 1"

		<span style="color:green">18</span> // <span style="color:red">5</span> = ?

		<span style="color:green">18</span> / <span style="color:red">5</span> = <span style="color:blue">3</span>.~~6~~

		<span style="color:green">18</span> // <span style="color:red">5</span> = <span style="color:blue">3</span>

	=== "Example 2"

		<span style="color:green">12</span> // <span style="color:red">7</span> = ?

		<span style="color:green">12</span> / <span style="color:red">7</span> = <span style="color:blue">1</span>.~~714~~

		<span style="color:green">12</span> // <span style="color:red">7</span> = <span style="color:blue">1</span>



!!!help "Exercise"

	In the interpreter, type the following and observe the results:

	* 4 + 17

	* 15 * 90 * 2

	* 2 * (3 + 4)
	
	* 1 + 18 / 4
	
	* 7 ** 3
	
	* 20 % 17
	
	* 20 // 17
	
	* 0.09 + 8.3
	
	* 7.004 - 9.004
	
	* 3.4 % 0.5
	
	* 3.4 // 0.5

{{ terminal() }}
