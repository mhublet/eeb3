Python is a popular programming language first created by Guido von Rossum in 1991 that was named after the British comedy Monty Python. 

It is widely used by a lot of people and lot of famous companies (Google, YouTube, Bit Torrent, Intel, Cisco, Hewlett-Packard, NASA, iRobot,…).

## Definitions

We will begin with some important definitions:

!!!abstract "Algorithm"

    An algorithm is a process or a list of steps to be followed in order to solve a particular problem.

    An algorithm can be implemented on a computer.

!!!abstract "Program"

    A program is a set of instructions which, when executed on a computer, performs a defined process. 

    A program is therefore seen as the translation into computer-understandable code of an algorithm that solves a problem.

!!!abstract "Programming language"

    A programming language such as Python defines the necessary rules for the program to be understandable and executable by a computer.

!!!info "Interpreter vs Compiler"

    A program called an interpreter or compiler "translates" the programming language into machine code.

    Python uses an interpreter which means that your code will be read, translated and executed line by line. 
    C++, however, uses a compiler, which means that your code is first fully translated by the compiler and only then executed.


## Tools

Two tools are essential:

* a Python3 interpreter (>>>) that will allow you to run your codes on your computer.
* a text editor to enter and save your codes.

The software we will be using is the Thonny IDE.

![thonny](assets/thonny.png){: style="width:500px"}

Thonny comes with Python 3.7 built in, so just one simple installer is needed and you're ready to learn programming.

It's already installed on the school's computers but if you want to install it at home, the easiest way to install Thonny is to go to the website  [https://thonny.org/](https://thonny.org/) and download the latest version. Installation is a simple click, like most applications.



## First use

Let's test the interpreter.

\>>> means the interpreter is ready to receive your instructions.

Programing languages respect a very specific syntax, if you type random things and don’t respect the syntax, python will return an error.

!!!question "Exercise"

    In Thonny's interpreter or in the online interpreter below, type the following and observe the results:

    * Hello
    * "Hello"
    * 7
    * 9.5 + 2
    * 2 * 3
    * 10 / 5 
    * "Hello" * 10
    * "He" + "llo"


## Online Interpreter

{{ terminal() }}
