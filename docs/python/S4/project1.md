* Use ```Thonny``` for writing your code.

## Project 1

!!!warning "Save your code in your ```Python``` folder and call it ```project1```."

Write a program asking the user to find a number between 1 and 10. The user can have up to three guesses.

Your program should:

* randomly select a number between 1 and 10 and save it in a variable.
* create a loop with two conditions: the user still has guesses and he hasn't find the correct answer yet.

Inside the loop, it should:

* ask the user what is his guess using the input function.
* check with a conditional if he guessed correctly or not. If he did congratulate him and get out of the loop. If he didn't, remove one of his guess.

???Help

	* Create a variable ```number``` and assign a random number between 1 and 10 to it. It's the number that the user will have to guess.
	* Create a variable ```guess``` and assign the value 3 to it. This variable should be decreased by one each time the user gives a wrong number.
	* Create a variable ```found``` and assign the boolean value ```False``` to it. The point of this variable is to track whether the user found the right answer or not. If he finds the right number, you should change that variable to ```True```.
	* Write your while loop with the two conditions (found == False and guess > 0).
	* Ask the user to guess a number between 1 and 10 using the input function. Save the answer in the variable ```user_guess```.
	* Write your conditional where you will compare number and user_number. If the numbers are the same, congratulate the user and change the found variable to True. If the numbers are not the same, decrease the variable guess by 1.

	```python
	number = ...
	guess = ...
	found = ...
	while condition:
		user_number = ...
		if condition:
			print("Congratulations, you found the number!")
			found = ...
		else:
			print("Wrong answer") 
			guess = ...
	```

## Project 2

!!!warning "Save your code in your ```Python``` folder and call it ```project2```."
 
Write a program that will ask the user to choose between three doors. Behind two doors, you will find starving tigers, behind one door, there is a treasure. He has to guess the door hiding the treasure. The user should have the possibility to play again.

You program should:

*  create a loop with a condition: the user wants to play.

Inside the loop, it should:

* randomly select a number between 1 and 3.
* ask the user his guess is using the input function.
* check with a conditional if he guessed correctly or not, print an appropriate message based on the scenario.
* ask the user if he wants to play again.

???Help
	
	* Initialise a variable ```play``` with the boolean value ```True``` to track if the user wants to play again. If he doesn't want to play anymore, you should change that variable to ```False```.
	* Write a loop where the condition is that ```play == True```. 
	* Create a variable ```door``` and randomly assign a number between 1 and 3.
	* Ask the user which door he wants to open and save his answer in a variable.
	* Write a conditional to check if he found the treasure and print a message.
	* Ask the user if he wants to play again.


## Project 3

!!!warning "Save your code in your ```Python``` folder and call it ```project3```." 

Code a Rock, Paper, Scissor game.
