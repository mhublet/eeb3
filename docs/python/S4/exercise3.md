# Exercises

* Create a folder called ```Python``` on your H drive, you will save all your exercises there. 

* Use ```Thonny``` for writing your code.

## Exercise 1

!!!warning "Save your code in your ```Python``` folder and call it ```exercise1```."

Write a program that uses the following variables:

* daysPerWeek  
* hoursPerDay 
* minutesPerHour

to calculate the number of minutes in a week.

???question "Help"

	* To initialise a variable, the syntax is the following: ```name = value```.
	* You can get the number of minutes in a week by multiplying the three variables: ```minutesPerWeek = daysPerWeek * hoursPerDay * minutesPerHour```
	* Don't forget to print your answer.
	* The answer is: 10080.

## Exercise 2

!!!warning "Save your code in your ```Python``` folder and call it ```exercise2```."

Write a program:

* asking for three integers ```a```, ```b```, ```c``` as input;
* printing the result of the calculation: ```(a + b)² - c```. 

Run your code and check with different inputs the results in the interpreter.

???question "Help"

	* ```input()``` allows you to ask a question to the user. This is the syntax when you ask for an integer: ```numberOfChildren = int(input("How many children do you have? "))```
	* a to the power of b in python is written: ```a ** b```.

## Exercise 3

!!!warning "Save your code in your ```Python``` folder and call it ```exercise3```."

Write a program:

* asking for two numbers as input;
* dividing the first number by the second number.

It should print:

* the calculation;
* the result of the division;
* the remainder of the division.

Run your code and check with different inputs the results in the interpreter.

!!!example

	```output
	>>> %Run exercise3.py
	Give me a first number: 9
	Give me a second number: 4
	9 : 4 = 2.25
	The quotient of the division is 2
	The remainder of the division is 1
	>>> 
	```

???note "What is the remainder of the division when the first number is divisible by the second one?"
	
	If a is divisible by b, the remainder of the division is nul, ```a % b``` is equal to 0.

???question "Help"

	* To print a combination of strings and variables, you need to seperate them using a coma.

	For example: ```print("Your name is", name, "and it's a nice name.")```.

	* To calculate the quotient of a division, the operator is ```//```.
	* To calculate the remainder of a division, the operator is ```%```. 



## Exercise 4

!!!warning "Save your code in your ```Python``` folder and call it ```exercise4```."

Write a program asking as input:

* the name of the user;
* and their favorite food.

With the data given, print: ```The favorite food of x is y```

Run your code and check with different inputs the results in the interpreter.


## Exercise 5

!!!warning "Save your code in your ```Python``` folder and call it ```exercise5```."

Write a program asking as input:

* the name of the user;
* the age of the user.

And printing as output:

* a message including the name of the user, saying that it is a nice name.
* another message including the age of the user, saying that you are one year older and printing your age.

Run your code and check with different inputs the results in the interpreter.


 

 


 

 



 

 

 

 

