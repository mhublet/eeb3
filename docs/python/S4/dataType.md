Data is a basic element when it comes to programming, it can be numbers or letters. 

Every data has a specific type:

* Whole number such as 1 or 8 is an integer (int)
* Decimal number such as 1.2 or 7.8 is a float (float)
* Text such as 'Hello', "Hello" or '"Hello"' is a string (str)

There are more data types, but we will see them later on.

!!!question "Exercise"

	In the interpreter, type the following:

	* type("hello")
	* type(3)
	* type(2.5)


{{ terminal() }}
