# Help 4 - Finishing the game

## Main Function

In your main function, after your call to the function ```start_water(garden,n,m)```, you now need to do different actions based on whatever is returned by the function.

If you wrote ```result = start_water(garden,n,m)```:

* ```result[0]``` will give you the last version of your garden
* ```result[1]``` will either give you -10 000, None or the amount of statues hit.

If you get:

* ```-10000```: then you print a message to the user to let him know that the game is over and you stop the game.
* ```None```: then the attempt failed but the user can try again. You should go back to the original version of the garden, without water and mirrors and let the user play again.
* The amount of statues hit: then you need to call the function ```calculate_score(statue,attempt,n,m)``` that calculate the final score of the user and prints it to the screen. After that, you stop the game.


## Play Again

The user failed and the ```start_water``` function returned ```None```. You now need to reset the garden to the original value.

At the beginning of your ```play``` function, when you get your garden for the first time (after calling the ```create_garden``` function), you need to create a deep copy of your garden so you can reset it if needed. In order to do a deep copy of your garden, you will need to import the ```copy``` module.

You also need an attempt variable to keep track of how many times the user tried to solve the game.

```python
# import should be at the very top of your code
import copy

# at the beginning of your play function
attempt = 1
reset_garden = copy.deepcopy(garden)

# after the start_water function, if the result returned was None
garden = copy.deepcopy(reset_garden)
attempt += 1
```

## Calculate Score

The ```calculate_score(statue,attempt,n,m,)``` function will calculate the score based on the different values given as parameters. It should print a message with the score of the user.

The score is calculated as follow :

$$
score = \frac{n+m−totalOfStatues}{numberOfAttempts×0.1}
$$


## Improvments

It is now your turn to improve the game !

You can:

* Add new fonctionalities;
* Make your code user-proof by checking that the user can't put values that would break your code;
* Avoid impossible scenarios when you create your garden;
* ...




