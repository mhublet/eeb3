# Help 1 - Initialisation

!!!warning "Make sure you read the theory about Matrix before."

## List of Lists of 0

Let's create our garden by defining the ```create_garden(n,m)``` function.

We want to create a matrix (a list of list) filled with ```0``` based on the parameters ```n``` and ```m``` received by the function. The idea is that the inside of the garden should be ```n x m``` so we are going to create a matrix ```(n+2) x (m+2)```.

To do that, we first need to initialize an empty list and then add each row one by one using a loop.

!!!warning "Make sure your function returns the garden!"

!!!example

	Let's say you want to create a matrix ```4 x 5``` filled with 0:

	$\begin{bmatrix}0 & 0 & 0 & 0 & 0\\0 & 0 & 0 & 0 & 0\\0 & 0 & 0 & 0 & 0\\0 & 0 & 0 & 0 & 0\end{bmatrix}$

	You could write the below code:

	```python
	matrix = []
	matrix.append([0]*5)
	matrix.append([0]*5)
	matrix.append([0]*5)
	matrix.append([0]*5)
	```

	This is just an example, you will have to use ```n``` and ```m``` as parameters and use a loop to append the right number of rows.
	
	It initialises an empty list ```matrix``` and then adds 4 rows of 5 ```0```. 
	If you print the list matrix, the output is: 

	```[[0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]```.



## Display the Garden

Let's define our function ```show_garden(garden)``` so we can show our garden matrix in a nice way.
Check the theory to see how to do it.

By now, if you add the below code at the bottom of your code, it should:

* initialise a garden 7 x 8 filled with 0
* print the garden in a nice way

```python
n = 5
m = 6
garden = create_garden(n,m)
show_garden(garden)
```

```output
0 0 0 0 0 0 0 0

0 0 0 0 0 0 0 0

0 0 0 0 0 0 0 0

0 0 0 0 0 0 0 0

0 0 0 0 0 0 0 0

0 0 0 0 0 0 0 0

0 0 0 0 0 0 0 0 
```

## Finish the Garden

### Borders

We need to create the borders of our garden. To do so, we are going to add ```x```'s in the first and last row of our matrix, as well as in the first and last cell of each other rows.

Your code to add ```x```s on the first and last row will look like the one below, except that you will be using a loop and ```b``` will go from ```0``` to ```m - 1```.

```python
matrix[0][b] = "x" # adds x's to the first row
matrix[-1][b] = "x" # adds x's to the last row
```

Try to find out on your own how to add ```x```'s at the beginning and at the end of each other rows.

!!!example "Example of output with ```n = 5``` and ```m = 6```"

	```output
	X X X X X X X X 

	X 0 0 0 0 0 0 X 

	X 0 0 0 0 0 0 X 

	X 0 0 0 0 0 0 X 

	X 0 0 0 0 0 0 X 

	X 0 0 0 0 0 0 X 

	X X X X X X X X 
	```

	As you can see, it creates a ```5 x 6``` garden.


### Rabbit


You need to replace one of the 0 inside your garden by the letter "R" to represent a rabbit.
Use the random module to select a random column and a random row.

!!!warning "If you forgot how the random module works, go read the random section in Python S4."

!!!example "Example of output with ```n = 5``` and ```m = 6```"

	```output
	X X X X X X X X 

	X 0 0 0 0 0 0 X 

	X 0 0 0 0 0 0 X 

	X 0 0 0 0 0 0 X 

	X 0 0 0 0 0 0 X 

	X 0 R 0 0 0 0 X 

	X X X X X X X X 
	```

### Statues

We now need to add ```n``` statues, with a value going from ```1``` to ```n```. We will need to use loops and random again but we will also need to make sure that there is not yet a rabbit or a statue at the randomly selected cell before changing the value.

!!!example "Example of output with ```n = 5``` and ```m = 6```"

	```output
	X X X X X X X X 

	X 0 1 0 0 0 0 X 

	X 0 0 0 0 2 5 X 

	X 0 3 0 0 0 0 X 

	X 4 0 0 0 0 0 X 

	X 0 R 0 0 0 0 X 

	X X X X X X X X 
	```


## Replace the 0

Improve your ```show_garden(garden)``` function so it shows a space ```" "``` instead of ```0```.


!!!Example "Example of output with ```n = 5``` and ```m = 6```"
	```output
	X X X X X X X X 

	X   2     3   X 

	X         5   X 

	X     1       X 

	X   4       R X 

	X             X 

	X X X X X X X X 
	```

Congratulations, your garden is properly initialised and printed to the screen!