# Help 2 - Gameplay

## General Gameplay

Let's start defining our function ```play()``` so it initialises the game by:

* printing a welcome message;
* asking to the user how many statues he wants (n parameter);
* asking to the user how many mirrors he wants (m parameter);
* initialising the garden using the ```create_garden(n,m)``` function;
* showing the garden using the ```show_garden(garden)``` function.

!!!example

	By now, if you call your ```play()``` function at the bottom of your code, it should print the following output:

	```output
	Welcome to my amazing game
	How many statues should I put? 6
	How many mirrors do you want? 5
	Good luck trying to kick the rabbit out, you have 5 mirrors available for this game.
	And here is your garden today:
	X X X X X X X 

	X           X 

	X   5 3     X 

	X     1 4 2 X 

	X   R       X 

	X         6 X 

	X           X 

	X X X X X X X 
	```

Then this function should start a loop where you ask the user what he wants to do:

1. start the water;
2. add a mirror;
3. leave the game.

If the user wants to leave the game, it should stop the loop and print a goodbye message.

!!!example

	You should now get this additional output:

	```output
	What do you want to do?
	1: start the water
	2: add a mirror
	3: leave the game
	3
	Bye Bye
	```


## Add Mirrors

This function should be called when the user chooses the option 2 and it should:

* Use 3 inputs to ask the row, the column and the orientation of the mirror that the user wants to add;
* Update the garden and print it.