
# Year Project - Lapin Vorace

## Introduction

You are a meticulous gardener and you love your job. But recently, a very annoying rabbit keeps destroying all your efforts. Not being able to chase that damn rabbit away, you have invested in a high powered water cannon to send it home (with some well-deserved bruises). 

Unfortunately, the power of this water jet is such that it can only move in a straight line. To direct the flow, you can use mirrors, but their angle is limited to two positions.
You also have some precious sculptures in your garden that the water jet risks destroying. If possible, you should avoid touching them.

![inclinaison](assets/inclinaison.png){: style="width:500px" : .center}


## The game

The game consists of the following :

* A board is created and is displayed to the user ;
* The user has 3 options : quit the game, insert a mirror or start the jet ;
* If you insert a mirror, the coordinates will be requested ;
* If you start the jet, you need to display the result and based on the rules explained later on,
declare winner, loser (if the jets cross) or ask the user to place the mirrors again in the same
garden and layout (back to the three starting options) ;

The program that we ask you to write should begin a game at the level of difficulty chosen by the n and m parameters, allow you to play and display the game in progress and display your final score once the game is complete. Please see below an example of execution.


```output

>>> %Run lapinVorace.py
Welcome to my amazing game
How many statues should I put? 6
How many mirrors do you want? 5
Good luck trying to kick the rabbit out, you have 5 mirrors available for this game.
And here is your garden today:
X X X X X X X 

X           X 

X   5 3     X 

X     1 4 2 X 

X   R       X 

X         6 X 

X           X 

X X X X X X X 

What do you want to do?
1: start the water
2: add a mirror
3: leave the game
2
In which row do you want to add a mirror? 1
In which column do you want to add a mirror? 2
Which mirror do you want to add ? / or \? \
X X X X X X X 

X   \       X 

X   5 3     X 

X     1 4 2 X 

X   R       X 

X         6 X 

X           X 

X X X X X X X 

What do you want to do?
1: start the water
2: add a mirror
3: leave the game
2
In which row do you want to add a mirror? 6
In which column do you want to add a mirror? 2
Which mirror do you want to add ? / or \? \
X X X X X X X 

X   \       X 

X   5 3     X 

X     1 4 2 X 

X   R       X 

X         6 X 

X   \       X 

X X X X X X X 

What do you want to do?
1: start the water
2: add a mirror
3: leave the game
1
X X X X X X X 

X - \       X 

X   | 3     X 

X   | 1 4 2 X 

X   |       X 

X   |     6 X 

X   \ - - - X 

X X X X X X X 

Congratulations, your score is 110.0
>>> 

```

## The rules


* The game board is a matrix of dimension n×m representing the garden;
* The jet starts in the coordinate (0,0) and in a horizontal direction. It must finish in (n−1,m−1) passing through where the rabbit is to win;
* There are n statues, whose values range from 1 to n. They are randomly placed in the garden ;
* You are limited to m mirrors for a given game;
* The jet is so powerful that it cannot cross itself at the risk of destroying your garden (great power
implies great stupidity). This will immediately cause you to lose the game with a score of
-10000;
* Mirrors cannot be placed on spaces already occupied by a statue or the rabbit;
* The score is calculated as follow :

$$
score = \frac{n+m−totalOfStatues}{numberOfAttempts×0.1}
$$

* As far as the display is concerned, ```R``` represents the rabbit, statues are represented by their
numerical value, empty spaces (indicated as 0 in the grid) by two empty spaces and the following characters ```\``` and ```/``` represents the different orientations of the mirrors;
* When you display the grid after the jet has been started, the jet will be shown by the characters ```-```  or ```|``` based on the direction of the jet;
* The difficulty of the game is determined by the choice of n and m;
* Each time you start the jet and do not reach your goal, you do not lose the game but you
need to try again, replace all the mirrors and your final score will be impacted by the number of failed
attempts you make;
* The jet can only leave the garden via the (n − 1,m − 1) cell, otherwise it is considered as a
failed attempt;
* The water jet can only move horizontally or vertically and each time the jet hits a mirror, it
will only be modified by + or - 90° from its original trajectory.

## Functions

We ask you to include the following functions:

* create_garden(n,m) creates and returns a board with the dimensions n × m containing the
rabbit and the n statues.
* start_jet(garden,n,m) simulates the water jet and returns a list containing two elements:

	1. a copy of the garden in progress with the trajectory of the tested jet;
	2. either None if the jet is not complete or does not cross, or a list containing the values of the different statues touched by the jet, or the value −10000 if the jets cross;	

* calculate_score(l) calculates and returns the final score of the game based on the list
returned by the function start_jet();
* play() which will be your main function ;
* show_garden(garden) displays the game in progress.

## Grading

You can work on this project alone or in group of 2.
The grade will be out of 10 and the repartition will be as follow:

* 5 points for the project in itself
* 5 points for the presentation

For the project in itself, I will try the game and see if it works. I will also check how you wrote the code and if you respected best practices (use of functions, naming variables with a meaningful name, no copy/paste,...). The grade will be the same for each member of the group.

For the presentation, I will ask individual questions to each member of the group. The goal is to see if you understand your code and if you could modify it in order to implement some improvements. 
