# Help 3 - Start Water

```start_water(garden,n,m)``` is the most complicated function of our project. There are multiple ways of doing it. I will be explaining how I did it but feel free to do it differently.

## Initialisation

You will first have to initialise a bunch of variables, such as the row and column where your start, the initial direction in which you are going (right, left, up or down), boolean values for the rabbit and the explosion of the garden, an empty variable to keep track of the statues that you will hit and an empty list for your end result.

## Body of the function

The main idea of this function is to have a while loop that will go on while we are in the garden and the garden has not exploded. So basically, while:

* row is between 1 and n;
* column is between 1 and m;
* kaboom (boolean variable checking if the garden has exploded) is False.

In this loop, we will:

* check if we are on a mirror. If we are, we change the direction and the current row or column based on the mirror and the previous direction;
* if we are not on a mirror, we should check if we are on the rabbit, on a statue, or crossing water. If we are we need to change the appropriate variables;
* then, still if we are not on a mirror, we modify the current cell with ```-``` or ```|``` and we update the row or the column based on the direction.

## Finishing the Function

When we get out of the loop, we add the updated version of the maze in the result list and then we check in which scenario we are:

1. if the garden exploded, we add -10 000 as a score in result;
2. else, if we found the rabbit and exit the garden in the bottom right corner, we add the variable statues in result;
3. else, if we failed but didn't destroy the garden, we add ```None``` in result.

```python
def startwater(garden,n,m):
	result = []

	# while loop

	result.append(garden)
	if ...: # garden exploded
		result.append(-10000)
	elif ...: #you win
		result.append(statues)
	else:
		result.append(None)
	return result
```

