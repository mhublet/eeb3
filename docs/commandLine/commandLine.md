The command line is a text interface for your computer. It’s a program that takes in commands and passes them on to the computer’s operating system to run.

From the command line, you can navigate through files and folders on your computer, just as you would with Finder on Mac OS or Windows Explorer on Windows. The difference is that the command line is fully text-based.

The advantage of using the command line is its power. You can run programs, write scripts to automate common tasks, and combine simple commands to handle difficult tasks. All of these traits come together to make it an important programming tool.

On Mac and Linux systems, we access the command line through something called Bash. In this course, we will focus on Bash. Although Windows comes with a different built-in command line, Windows users can also download Bash.

We are going to use an online virtual machine with bash installed to learn how to use it:

1. Click on <a href="https://vfsync.org/signup">the following link</a> to create your account.
2. Click on <a href="https://bellard.org/jslinux/vm.html?url=https://bellard.org/jslinux/buildroot-x86.cfg">the following link</a> to open the virtual machine.
3. To connect to the virtual machine, you first need to type ```vflogin yourUsername``` and then you enter your password.

!!!danger "Important"

	Nothing will appear on the screen when you type in your password, don't worry, it's completely normal. Just type your password and press ```Enter```.


You will learn by doing. Type in the examples I give you for each command and try to understand them. It is important that you understand that:

* You do not type in the ```$```.
* You type what is after ```$```, then press ```Enter```.
* The output comes after you press ```Enter```, followed by another ```$``` prompt.

## Print Working Directory - ```pwd```

```bash
[mhublet@localhost ~]$ pwd
/home/mhublet
```

```pwd``` outputs the name of the directory you are currently in, called the working directory.

The ```pwd``` command is useful to show where you are in the filesystem.

## Make Directory - ```mkdir```


```bash
[mhublet@localhost ~]$ mkdir temp
[mhublet@localhost ~]$ mkdir temp/stuff
[mhublet@localhost ~]$ mkdir temp/stuff/thing
[mhublet@localhost ~]$ mkdir -p temp/stuff/things/frank/joe/alex/john
```

These are all the different ways you can run ```mkdir```. What does ```mkdir``` do? It makes directories. 

What does it mean to make a directory? You might call directories ```folders```. They're the same thing. All you did above is create directories inside directories inside of more directories. This is called a ```path``` and it's a way of saying ```first temp, then stuff, then things and that's where I want it```. It's a set of directions to the computer of where you want to put something in the tree of folders (directories) that make up your computer's hard disk.

!!!info

	The ```-p``` option causes any parent directories to be created if necessary.

## Change Directory - ```cd```

```bash
[mhublet@localhost ~]$ cd temp
[mhublet@localhost temp]$ pwd
/home/mhublet/temp
[mhublet@localhost temp]$ cd stuff
[mhublet@localhost stuff]$ pwd
/home/mhublet/temp/stuff
[mhublet@localhost stuff]$ cd things
[mhublet@localhost things]$ pwd
/home/mhublet/temp/stuff/things
[mhublet@localhost things]$ cd frank
[mhublet@localhost frank]$ pwd
/home/mhublet/temp/stuff/things/frank
[mhublet@localhost frank]$ cd joe
[mhublet@localhost joe]$ pwd
/home/mhublet/temp/stuff/things/frank/joe
[mhublet@localhost joe]$ cd alex
[mhublet@localhost alex]$ pwd
/home/mhublet/temp/stuff/things/frank/joe/alex
[mhublet@localhost alex]$ cd john
[mhublet@localhost john]$ pwd
/home/mhublet/temp/stuff/things/frank/joe/alex/john
[mhublet@localhost john]$ cd ..
[mhublet@localhost alex]$ cd ..
[mhublet@localhost joe]$ pwd
/home/mhublet/temp/stuff/things/frank/joe
[mhublet@localhost joe]$ cd ..
[mhublet@localhost frank]$ cd ..
[mhublet@localhost things]$ pwd
/home/mhublet/temp/stuff/things
[mhublet@localhost things]$ cd ../../..
[mhublet@localhost ~]$ pwd
/home/mhublet
[mhublet@localhost ~]$ cd temp/stuff/things
[mhublet@localhost things]$ pwd
/home/mhublet/temp/stuff/things
[mhublet@localhost things]$ cd ../../..
[mhublet@localhost ~]$ pwd
/home/mhublet
```

You made all these directories in the last exercise, and now you're just moving around inside them with the cd command.

## List Directory - ```ls```

```bash
[mhublet@localhost ~]$ cd temp
[mhublet@localhost temp]$ ls
stuff
[mhublet@localhost temp]$ cd stuff
[mhublet@localhost stuff]$ ls
things
[mhublet@localhost stuff]$ cd things
[mhublet@localhost things]$ ls
frank
[mhublet@localhost things]$ cd ../..
[mhublet@localhost temp]$ cd ..
[mhublet@localhost ~]$ ls
temp
```

The ```ls``` command lists out the contents of the directory you are currently in. You can the we use ```cd``` to change into different directories and then list what's in them so we know which directory to go to next.

!!!info

	There are a lot of options for the ```ls``` command, don't hesitate to try them:

	* ```-a``` lists all contents, including hidden files and directories;
	* ```-l``` lists all contents of a directory in long format, as well as the file permissions;
	* ```-t``` orders files and directories by the time they were last modified.

	You can use those options seperately (ls -a for example) or together (ls -alt).

## Remove Directory - ```rmdir```

```bash
[mhublet@localhost ~]$ cd temp
[mhublet@localhost temp]$ ls
stuff
[mhublet@localhost temp]$ cd stuff/things/frank/joe/alex/john
[mhublet@localhost john]$ cd ..
[mhublet@localhost alex]$ rmdir john
[mhublet@localhost alex]$ cd ..
[mhublet@localhost joe]$ ls
alex
[mhublet@localhost joe]$ rmdir alex
[mhublet@localhost joe]$ ls
[mhublet@localhost joe]$ cd ..
[mhublet@localhost frank]$ ls
joe
[mhublet@localhost frank]$ rmdir joe
[mhublet@localhost frank]$ cd ..
[mhublet@localhost things]$ ls
frank
[mhublet@localhost things]$ rmdir frank
[mhublet@localhost things]$ cd ..
[mhublet@localhost stuff]$ ls
things
[mhublet@localhost stuff]$ rmdir things
[mhublet@localhost stuff]$ cd ..
[mhublet@localhost temp]$ ls
stuff
[mhublet@localhost temp]$ rmdir stuff
[mhublet@localhost temp]$ pwd
/home/mhublet/temp
[mhublet@localhost temp]$ cd ..
[mhublet@localhost ~]$ pwd
/home/mhublet
```

We are now mixing up commands so make sure you type them exactly and pay attention.
Here you learned how to remove a directory. It's easy, you just go to the directory right above it, then type ```rmdir DIR``` replacing ```DIR``` with the name of the directory to remove.

## Making Empty Files - ```touch```

```bash
[mhublet@localhost ~]$ cd temp
[mhublet@localhost temp]$ touch iamcool.txt
[mhublet@localhost temp]$ ls
iamcool.txt
[mhublet@localhost temp]$ cd ..
```

## Copy a File - ```cp```

```bash
[mhublet@localhost ~]$ cd temp
[mhublet@localhost temp]$ cp iamcool.txt neat.txt
[mhublet@localhost temp]$ ls
iamcool.txt  neat.txt
[mhublet@localhost temp]$ cp neat.txt awesome.txt
[mhublet@localhost temp]$ ls
awesome.txt  iamcool.txt  neat.txt
[mhublet@localhost temp]$ cp awesome.txt thefourthfile.txt
[mhublet@localhost temp]$ ls
awesome.txt        iamcool.txt        neat.txt           thefourthfile.txt
[mhublet@localhost temp]$ mkdir something
[mhublet@localhost temp]$ cp awesome.txt something/
[mhublet@localhost temp]$ ls
awesome.txt        neat.txt           thefourthfile.txt
iamcool.txt        something
[mhublet@localhost temp]$ cd something
[mhublet@localhost something]$ ls
awesome.txt
[mhublet@localhost something]$ cd ..
[mhublet@localhost temp]$ ls something/
awesome.txt
[mhublet@localhost temp]$ cp -r something newplace
[mhublet@localhost temp]$ ls
awesome.txt        neat.txt           something
iamcool.txt        newplace           thefourthfile.txt
[mhublet@localhost temp]$ ls newplace/
awesome.txt
[mhublet@localhost temp]$ cd ..
```

Now you can copy files. It's simple to just take a file and copy it to a new one. In this exercise I also make a new directory and copy a file into that directory.

!!!info

	* The -r option allows you to copy directories with files in them.
	* Notice how sometimes I put a ```/``` at the end of a directory? That is to make sure that it is a directory, if it isn't, there would be an error.
	* You can copy multiple files in a folder using the following command:
	```cp file1.txt file2.txt my_directory/```


## Moving a File - ```mv```

```bash
[mhublet@localhost ~]$ cd temp
[mhublet@localhost temp]$ ls
awesome.txt        neat.txt           something
iamcool.txt        newplace           thefourthfile.txt
[mhublet@localhost temp]$ mv awesome.txt uncool.txt
[mhublet@localhost temp]$ ls
iamcool.txt        newplace           thefourthfile.txt
neat.txt           something          uncool.txt
[mhublet@localhost temp]$ mv newplace oldplace
[mhublet@localhost temp]$ ls
iamcool.txt        oldplace           thefourthfile.txt
neat.txt           something          uncool.txt
[mhublet@localhost temp]$ mv oldplace newplace
[mhublet@localhost temp]$ ls
iamcool.txt        newplace           thefourthfile.txt
neat.txt           something          uncool.txt
[mhublet@localhost temp]$ cd ..
```

Moving files or, rather, renaming them is easy. You just have to give the old name and the new name.

## Write a File - ```cat```

To type text into a file very easily, you can type ```cat > somefile.txt```. Then ```cat``` will read whatever you type and write it in that file. 

!!!danger "Important"

	You have to ```close``` the file when you are done typing some content with ```CTRL d```.

```bash
[mhublet@localhost ~]$ cd temp
[mhublet@localhost temp]$ cat > ex1.txt
This is a text.
Very interesting.
```


You just created a new file and wrote some text in it. THe name of your file is ```ex14.txt``` and it is located in your ```temp``` folder.

## View a File - ```less```

!!!danger "Important"

	To get out of the file, type ```q``` as in quit.

```bash
[mhublet@localhost temp]$ less ex1.txt

[displays the content of your file here]

```

This is one way to look at the contents of a file. It's useful because, if the file has many lines, it will "page" so that only one screenful at a time is visible.

## Stream a File - ```cat```

```bash
[mhublet@localhost temp]$ cat > ex2.txt
Hello you,
Feel free to improvise whatever you write here.
Enjoy and don't forget to press CTRL D to leave.
[mhublet@localhost temp]$ less ex2.txt

[displays content here, don't forget to press q to leave]

[mhublet@localhost temp]$ cat ex2.txt

[displays content here]

[mhublet@localhost temp]$ cat ex1.txt

[displays content here]
```

This command just spews the whole file to the screen with no paging or stopping. 

???info "More info on the difference between ```less``` and ```cat```"

	Less is a file reading program, and Cat is a string manipulation program.

	Less is a dedicated file reader that reads a file one screen at a time, and loads more of the file as you scroll through it.

	Cat, however, is not a dedicated file reader. The intended use of Cat is to take multiple inputs and stick them end to end. Cat is short for "concatenate", which means exactly that. Cat, by default, outputs the result to standard output, which is normally the command line. However, if Cat has only one argument, it simply spits out the input. For this reason, if you have a small enough file that you don't have to worry about scrolling, Cat can function as a much simpler file reader.


## Removing a File - ```rm```

```bash
[mhublet@localhost ~]$ cd temp
[mhublet@localhost temp]$ ls
ex1.txt            iamcool.txt        newplace           thefourthfile.txt
ex2.txt            neat.txt           something          uncool.txt
[mhublet@localhost temp]$ rm uncool.txt
[mhublet@localhost temp]$ ls
ex1.txt            iamcool.txt        newplace           thefourthfile.txt
ex2.txt            neat.txt           something
[mhublet@localhost temp]$ rm iamcool.txt neat.txt thefourthfile.txt
[mhublet@localhost temp]$ ls
ex1.txt    ex2.txt    newplace   something
[mhublet@localhost temp]$ ls something/
awesome.txt
[mhublet@localhost temp]$ rm something/awesome.txt
[mhublet@localhost temp]$ ls something/
[mhublet@localhost temp]$ rmdir something
[mhublet@localhost temp]$ ls newplace/
awesome.txt
[mhublet@localhost temp]$ rmdir newplace
rmdir: 'newplace': Directory not empty
[mhublet@localhost temp]$ rm -rf newplace
[mhublet@localhost temp]$ ls
ex1.txt  ex2.txt
```

Here we clean up the files from the previous exercises.

!!!info

	You can't use ```rmdir``` on a non empty directory. That is why it is showing an error when you try to remove ```newplace```, the directory is not empty. Or you delete the file and then the folder, like we did for ```something```, or you have to recursively delete all of its content using ```rm -rf```.

## Pipes and Redirection - ```|, <, >```

```bash
[mhublet@localhost ~]$ cd temp
[mhublet@localhost temp]$ echo "Hello" > hello.txt
[mhublet@localhost temp]$ ls
bigFile.txt  ex1.txt      ex2.txt      hello.txt    newFolder    uncool.txt
[mhublet@localhost temp]$ cat hello.txt
Hello
[mhublet@localhost temp]$ cat bigFile.txt > hello.txt
[mhublet@localhost temp]$ cat hello.txt
This is a text.
Very interesting.
Hello you,
Feel free to improvise whatever you write here.
Enjoy
[mhublet@localhost temp]$ cat ex2.txt >> bigFile.txt
[mhublet@localhost temp]$ cat bigFile.txt
This is a text.
Very interesting.
Hello you,
Feel free to improvise whatever you write here.
Enjoy
Hello you,
Feel free to improvise whatever you write here.
Enjoy
[mhublet@localhost temp]$ cat ex2.txt | wc
        3        11        65
[mhublet@localhost temp]$ cat ex2.txt | wc | cat > ex1.txt
[mhublet@localhost temp]$ cat ex1.txt
        3        11        65
```

* ```>``` takes the standard output of the command on the left, and redirects it to the file on the right.
Note that ```>``` overwrites all original content.
* ```>>``` takes the standard output of the command on the left and appends (adds) it to the file on the right.
* ```<``` takes the standard input from the file on the right and inputs it into the command on the left.
* ```|``` is a “pipe.” The ```|``` takes the standard output of the command on the left, and pipes it as standard input to the command on the right. You can think of this as “command to command” redirection. Multiple ```|```s can be chained together.
* ```wc``` is a word count command.

## Wildcard Matching - ```*```

```bash
[mhublet@localhost temp]$ cd ..
[mhublet@localhost ~]$ cd temp
[mhublet@localhost temp]$ touch uncool.txt
[mhublet@localhost temp]$ mkdir newFolder
[mhublet@localhost temp]$ ls
ex1.txt     ex2.txt     newFolder   uncool.txt
[mhublet@localhost temp]$ ls *.txt
ex1.txt     ex2.txt     uncool.txt
[mhublet@localhost temp]$ ls ex*.*
ex1.txt  ex2.txt
[mhublet@localhost temp]$ ls e*
ex1.txt  ex2.txt
[mhublet@localhost temp]$ ls *t
ex1.txt     ex2.txt     uncool.txt
[mhublet@localhost temp]$ ls u*
uncool.txt
[mhublet@localhost temp]$ cat *.txt > bigFile.txt
[mhublet@localhost temp]$ ls
bigFile.txt  ex1.txt      ex2.txt      newFolder    uncool.txt
```


In addition to using exact filenames as arguments, we can use special characters like ```*``` to select groups of files. These special characters are called wildcards.

For example, here we use cp to copy all files in the current working directory into another directory:

```cp * my_directory/```

To move just the ‘.txt’ files you can use a wild card with a suffix:

```cp *.txt my_directory/```

In addition to suffixes, prefix text can be used with wildcards. Using w*.txt selects all files in the working directory starting with “w” (prefix) and ending with “.txt” (suffix), and copies them to my_directory/.

```cp w*.txt my_directory/```


## Looking inside Files - ```grep```

```bash
[mhublet@localhost ~]$ cd temp
[mhublet@localhost temp]$ cat > newfile.txt
This is a new file.
This is a new file.
This is still a new file.
[CTRL + d]
[mhublet@localhost temp]$ cat > oldfile.txt
This is an old file.
This is an old file.
This is still an old file.
[CTRL + d]
[mhublet@localhost temp]$ grep new *.txt
newfile.txt:This is a new file.
newfile.txt:This is a new file.
newfile.txt:This is still a new file.
[mhublet@localhost temp]$ grep old *.txt
oldfile.txt:This is an old file.
oldfile.txt:This is an old file.
oldfile.txt:This is still an old file.
[mhublet@localhost temp]$ grep -r file /home/mhublet/
/home/mhublet/temp/newfile.txt:This is a new file.
/home/mhublet/temp/newfile.txt:This is a new file.
/home/mhublet/temp/newfile.txt:This is still a new file.
home/mhublet/temp/oldfile.txt:This is an old file.
/home/mhublet/temp/oldfile.txt:This is an old file.
/home/mhublet/temp/oldfile.txt:This is still an old file.
[mhublet@localhost temp]$ grep -rl file /home/mhublet/
/home/mhublet/temp/newfile.txt
/home/mhublet/temp/oldfile.txt
[mhublet@localhost temp]$ grep "This is still" *.txt
newfile.txt:This is still a new file.
oldfile.txt:This is still an old file.
```

* ```grep``` stands for ```global regular expression print```. It searches files for lines that match a pattern and then returns the results. It is also case sensitive.
* ```grep -i``` enables the command to be case insensitive. 
* ```grep -r``` searches all files in a directory and outputs filenames and lines containing matched results. -r stands for ```recursive```
* ```grep -rl``` searches all files in a directory and outputs only filenames with matched results (so no lines). l (a lowercase L, not a capital i) stands for “files with matches.”

## Help - ```man```

```bash
[mhublet@localhost ~]$ man find

[displays help]

[mhublet@localhost ~]$ man less

[displays help]

[mhublet@localhost ~]$ man man

[displays help]

[mhublet@localhost ~]$ man grep

[displays help]
```

You can use the man command on Unix, and the help command in Windows to find information about commands. 

## Bandit

Let's practice what we learned with a game.

* Open ```PuTTY``` and fill in the following information:

hostname: ```bandit.labs.overthewire.org```

port: ```2220```

![putty](assets/putty.png){: style="width:300px"}

* Check the <a href="https://overthewire.org/wargames/bandit/bandit0.html"> following website</a> where you will find all the information on what to do next.

!!!info
	
	```PuTTY``` is an SSH and telnet client. It will allow us to connect to the server of the game.


### Level 2

```-``` is a special character in bash so if you try ```cat -``` it is not going to work.
Check on Google what you can put before ```-``` to let bash know that you don't want to use it as a special character this time.

### Level 3

Check the theory on the ```ls``` command to remember how you can list the hidden files.

### Level 4

Use the ```file``` command to check if a file is human-readable (ASCII text). 

* Don’t forget to add ```./```(or a similar method) in front of the filenames so the ```-``` is not considered a special character.
* Don’t forget to add a wildcard ```*``` at the end of the command so it checks all the different ```-file```s.

### Level 5

Use the ```find``` command to search for files in a directory hierarchy.
You can add the ```-readable``` option and a ```-size``` option. You need to add the ```c``` letter after the size to specify that the size you indicate is in bytes. Example: ```-size 3c``` will try to find a file with a size of 3 bytes.

### Level 6

Use the ```find``` command to search for files in a directory. Here the directory is the entire server so you search in ```/```.
The find command has an option ```-user```, ```-group``` and ```-size```.

!!!info

	If you want to get rid of the Permission Denied warnings, you can add ```2>/dev/null``` at the end of your command.

### Level 7

To look inside files, you need the ```grep``` command. The password is on the same line as ```millionth``` so if you find ```millionth```, you will see it.

### Level 8

You will need the ```uniq``` command but pay attention, uniq only “Filter adjacent matching lines”. In other words, uniq is not able to detect duplicate lines unless they are adjacent. Hence, the file have to be sorted using ```sort data.txt``` in order for the repeated lines to be together.
Then you can use the ```uniq``` command with the ```-u``` option since that option only prints unique lines.
Use a ```|``` between the ```sort``` and the ```uniq``` command so the output of ```sort``` is the input of ```uniq```.

### Level 9

The ```strings``` command finds human-readable strings in files. Specifically, it prints sequences of printable characters. Its main use is for non-printable files like hex dumps or executables.
1. First, we need to distinguish human-readable strings in ```data.txt```. We use the ```strings``` command.
2. Next, we want to filter that output by looking at lines that include more than one equal sign. - Assuming the equal signs and the password are on the same line, we can use ```grep``` again.

### Level 10

```Base64``` is a binary-to-text encoding scheme. It can often be recognised by equal signs at the end of the data. However, this is not always the case. Bash has a command called ```base64``` that allows for encoding and decoding in Base64. For decoding, we need to use the flag ```-d```.

### Level 11

Substitution means replacing one character with another. Substitution ciphers have been known and used for a very long time. Some of the most known are the Caesar and Vigenère ciphers. However, these simple substitution ciphers are not secure anymore!

As the ‘Helpful Reading Material’ on the levels site mentions, rotating letters by 13 positions is the ROT13 substitution cipher. It is nice because looking at the Latin alphabet with 26 letters the encryption algorithm equals the decryption algorithm.

The bash ```tr``` command, which stands for ’translate’, allows replacing characters with others. The base command syntax looks like the following ```tr <old_chars> <new_chars>```.

You can combine multiple changes, for example if you want the letters going from A to C to be replaced by the letters going from D to F and the letter D to F going from A to C, you could write:
```tr 'A-CD-F a-cd-f' 'D-FA-C d-fa-c'```

Don't forget to use ```cat``` and ```|```.

### Level 12

Hexdumps are often used when we want to look at data that cannot be represented in strings and therefore is not readable, so it is easier to look at the hex values. A hexdump has three main columns. The first shows the address, the second the hex representation of the data on that address and the last shows the actual data as strings (with ‘.’ being hex values that cannot be represented as a string). 

Many hex editors exist, for command line, ```xxd``` can be used. ```xxd <input_file> <output_file>``` creates hexdumps. When using the ```-r``` flag, it reverts the hexdump.

Hexdumps can be used to figure out the type of a file. Each file type has a magic number/file signature. You can find lists with a collection of these different file signatures online. 

Compression is a method of encoding that aims to reduce the original size of a file without losing information (or only losing as little as possible).

* ```gzip``` is a command to compress or decompress (using ```-d```) files. A ```gzip``` file generally ends with ```.gz```.
* ```bzip2``` is another command which allows for compressing and decompressing (using ```-d```) files. A ```bzip2``` file generally ends with ```.bz2```.
* An ```Archive File``` is a file that contains one or multiple files and their metadata. This can allow easier portability. ```tar``` is a command that creates archive files (using ```-cf```). It also allows extracting these files again (using ```-xf```). A tar archive generally ends with ```.tar```.

To solve this level, you should:

1. Create a folder and copy the data to make further actions easier.
2. Revert the hexdump
3. Decompress multiple times (each time check the header using xxd and decompress using the right command).


!!!info

	* 1f8b 08 -> gzip
	* 425a 68 -> bzip2
	* data.bin -> archive -> tar

### Level 13

Can't be done from the school computers, password for level 14 is: fGrHPx402xGC7U7rXKDaxiWFTOiF0ENq 

### Level 14

Localhost is a hostname and its IP address is ```127.0.0.1```. It is used to access network services on the same device that is running these services.

```nc``` or ```netcat``` is a command that allows to read and write data over a network connection. It can be used for TCP and UDP connections. To connect to a service (as client) on a network the command syntax is the following: ```nc <host> <port>```.

You need to submit the password to port ```30000``` on ```localhost``` and write the password.

### Level 15

OpenSSL is a library for secure communication over networks. It implements the Transport Layer Security (TLS) and Secure Sockets Layer (SSL) cryptographic protocols that are, for example, used in HTTPS to secure the web traffic.

```openssl s_client``` is the implementation of a simple client that connects to a server using SSL/TLS.

### Level 16

Port scanning is a method to find open ports on a host. A port can be seen as an address for a specific service. Every computer has ports with the numbers 0 to 65535. Some services have standard ports, like HTTP/80 or SSH/22. An open port means that the host offers a service to the network on this port.

```Nmap``` is a network scanner. It can do Host Discovery, Port Scanning, Version Detection (Service Detection) and a lot more. For this task, the ```-p``` flag is important. This flag lets us choose which ports to scan. By default, Nmap scans the top 1000 ports (not the first 1000 ports). Use ```-p``` to scan all 65535 ports. The ```-sV``` flag lets us do a service/version detection scan. It is possible to make Nmap perform all possible scans with the ```-A``` flag this will take a while though. A full scan would have the following command: ```nmap -p- -A <host>```, where ```<host>``` could be either an IP address or the name.

This level also uses SSL again.

1. First, we need to find open ports between 31000 to 32000 on localhost and check what services are running on them.
2. Then, use OpenSSL again to connect to your chosen port on localhost and send the password.



