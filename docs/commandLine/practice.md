# Bandit

Let's practice what we learned with a game.

* Open ```PuTTY``` and fill in the following information:

hostname: ```bandit.labs.overthewire.org```

port: ```2220```

![putty](assets/putty.png){: style="width:300px"}

* Check the <a href="https://overthewire.org/wargames/bandit/bandit0.html"> following website</a> where you will find all the information on what to do next.

!!!info
	
	```PuTTY``` is an SSH and telnet client. It will allow us to connect to the server of the game.


## Help

### Level 2

```-``` is a special character in bash so if you try ```cat -``` it is not going to work.
Check on Google what you can put before ```-``` to let bash know that you don't want to use it as a special character this time.

### Level 3

Check the theory on the ```ls``` command to remember how you can list the hidden files.

### Level 4

Use the ```file``` command to check if a file is human-readable (ASCII text). 

* Don’t forget to add ```./```(or a similar method) in front of the filenames so the ```-``` is not considered a special character.
* Don’t forget to add a wildcard ```*``` at the end of the command so it checks all the different ```-file```s.

### Level 5

Use the ```find``` command to search for files in a directory hierarchy.
You can add the ```-readable``` option and a ```-size``` option. You need to add the ```c``` letter after the size to specify that the size you indicate is in bytes. Example: ```-size 3c``` will try to find a file with a size of 3 bytes.

### Level 6

Use the ```find``` command to search for files in a directory. Here the directory is the entire server so you search in ```/```.
The find command has an option ```-user```, ```-group``` and ```-size```.

!!!info

	If you want to get rid of the Permission Denied warnings, you can add ```2>/dev/null``` at the end of your command.

### Level 7

To look inside files, you need the ```grep``` command. The password is on the same line as ```millionth``` so if you find ```millionth```, you will see it.

### Level 8

You will need the ```uniq``` command but pay attention, uniq only “Filter adjacent matching lines”. In other words, uniq is not able to detect duplicate lines unless they are adjacent. Hence, the file have to be sorted using ```sort data.txt``` in order for the repeated lines to be together.
Then you can use the ```uniq``` command with the ```-u``` option since that option only prints unique lines.
Use a ```|``` between the ```sort``` and the ```uniq``` command so the output of ```sort``` is the input of ```uniq```.

### Level 9

The ```strings``` command finds human-readable strings in files. Specifically, it prints sequences of printable characters. Its main use is for non-printable files like hex dumps or executables.
1. First, we need to distinguish human-readable strings in ```data.txt```. We use the ```strings``` command.
2. Next, we want to filter that output by looking at lines that include more than one equal sign. - Assuming the equal signs and the password are on the same line, we can use ```grep``` again.

### Level 10

```Base64``` is a binary-to-text encoding scheme. It can often be recognised by equal signs at the end of the data. However, this is not always the case. Bash has a command called ```base64``` that allows for encoding and decoding in Base64. For decoding, we need to use the flag ```-d```.

### Level 11

Substitution means replacing one character with another. Substitution ciphers have been known and used for a very long time. Some of the most known are the Caesar and Vigenère ciphers. However, these simple substitution ciphers are not secure anymore!

As the ‘Helpful Reading Material’ on the levels site mentions, rotating letters by 13 positions is the ROT13 substitution cipher. It is nice because looking at the Latin alphabet with 26 letters the encryption algorithm equals the decryption algorithm.

The bash ```tr``` command, which stands for ’translate’, allows replacing characters with others. The base command syntax looks like the following ```tr <old_chars> <new_chars>```.

You can combine multiple changes, for example if you want the letters going from A to C to be replaced by the letters going from D to F and the letter D to F going from A to C, you could write:
```tr 'A-CD-F a-cd-f' 'D-FA-C d-fa-c'```

Don't forget to use ```cat``` and ```|```.

### Level 12

Hexdumps are often used when we want to look at data that cannot be represented in strings and therefore is not readable, so it is easier to look at the hex values. A hexdump has three main columns. The first shows the address, the second the hex representation of the data on that address and the last shows the actual data as strings (with ‘.’ being hex values that cannot be represented as a string). 

Many hex editors exist, for command line, ```xxd``` can be used. ```xxd <input_file> <output_file>``` creates hexdumps. When using the ```-r``` flag, it reverts the hexdump.

Hexdumps can be used to figure out the type of a file. Each file type has a magic number/file signature. You can find lists with a collection of these different file signatures online. 

Compression is a method of encoding that aims to reduce the original size of a file without losing information (or only losing as little as possible).

* ```gzip``` is a command to compress or decompress (using ```-d```) files. A ```gzip``` file generally ends with ```.gz```.
* ```bzip2``` is another command which allows for compressing and decompressing (using ```-d```) files. A ```bzip2``` file generally ends with ```.bz2```.
* An ```Archive File``` is a file that contains one or multiple files and their metadata. This can allow easier portability. ```tar``` is a command that creates archive files (using ```-cf```). It also allows extracting these files again (using ```-xf```). A tar archive generally ends with ```.tar```.

To solve this level, you should:

1. Create a folder and copy the data to make further actions easier.
2. Revert the hexdump
3. Decompress multiple times (each time check the header using xxd and decompress using the right command).


!!!info

	* 1f8b 08 -> gzip
	* 425a 68 -> bzip2
	* data.bin -> archive -> tar

### Level 13

Can't be done from the school computers, password for level 14 is: fGrHPx402xGC7U7rXKDaxiWFTOiF0ENq 

### Level 14

Localhost is a hostname and its IP address is ```127.0.0.1```. It is used to access network services on the same device that is running these services.

```nc``` or ```netcat``` is a command that allows to read and write data over a network connection. It can be used for TCP and UDP connections. To connect to a service (as client) on a network the command syntax is the following: ```nc <host> <port>```.

You need to submit the password to port ```30000``` on ```localhost``` and write the password.

### Level 15

OpenSSL is a library for secure communication over networks. It implements the Transport Layer Security (TLS) and Secure Sockets Layer (SSL) cryptographic protocols that are, for example, used in HTTPS to secure the web traffic.

```openssl s_client``` is the implementation of a simple client that connects to a server using SSL/TLS.

### Level 16

Port scanning is a method to find open ports on a host. A port can be seen as an address for a specific service. Every computer has ports with the numbers 0 to 65535. Some services have standard ports, like HTTP/80 or SSH/22. An open port means that the host offers a service to the network on this port.

```Nmap``` is a network scanner. It can do Host Discovery, Port Scanning, Version Detection (Service Detection) and a lot more. For this task, the ```-p``` flag is important. This flag lets us choose which ports to scan. By default, Nmap scans the top 1000 ports (not the first 1000 ports). Use ```-p``` to scan all 65535 ports. The ```-sV``` flag lets us do a service/version detection scan. It is possible to make Nmap perform all possible scans with the ```-A``` flag this will take a while though. A full scan would have the following command: ```nmap -p- -A <host>```, where ```<host>``` could be either an IP address or the name.

This level also uses SSL again.

1. First, we need to find open ports between 31000 to 32000 on localhost and check what services are running on them.
2. Then, use OpenSSL again to connect to your chosen port on localhost and send the password.



