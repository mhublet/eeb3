# 3D Design

## 3D Printer

A 3D printer is a machine that creates real, physical objects from a digital design. It works by printing one layer of material at a time. Think of it like a very advanced glue gun. The printer melts plastic filament and lays it down, layer by layer, to slowly build your object from the bottom up. Once the printer is done, you’ll have a real 3D version of the object you designed on the computer.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Vx0Z6LplaMU?si=izZf0b9RFPgSxeJX" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>


## Tinkercad

Tinkercad is an easy-to-use, web-based 3D design tool that lets you create 3D models by combining different shapes. You can drag and drop objects like cubes, cylinders, and text to build more complex designs. It’s perfect for beginners because it’s simple but still powerful enough to create awesome projects.

## Let's Learn Tinkercad

To get familiar with Tinkercad, I want you to follow the tutorial ```Let's Learn Tinkercad```:

* Go to Resources - Learning Center;
* Click on ```View all```;
* Select ```Let's Learn Tinkercad```;
* Follow the 5 tutorials.

## Project: Personalized Keychain

In this project, you’ll design your own personalized keychain. You can add your name, initials, or a favorite symbol or shape to make it unique. Once your design is complete, we will 3D print it, and you’ll have your own custom keychain to take home.


Follow these steps to design a keychain in Tinkercad that is ready for 3D printing:

1\. **Log in to Tinkercad**

   * Go to [www.tinkercad.com](https://www.tinkercad.com) and log in with your account.

2\. **Create a New Design**
  
   * Once logged in, click on **"Create new design"** to start your keychain project.

3\. **Create the Base of the Keychain**

   * Drag a **rectangle** or **oval** shape from the shapes menu onto the workplane. This will be the base of your keychain.
   * **Resize it**: Make it about **5 cm long**, **2 cm wide**, and about **2-3 mm thick**. This ensures it’s strong enough but still light and practical for a keychain.

4\. **Add a Hole for the Keyring**

   * Drag a **cylinder** shape onto the workplane and resize it to about **5 mm in diameter**.
   * Position the cylinder near one edge of your keychain base. Then, turn the cylinder into a **hole** by selecting the "hole" option. This hole will allow you to attach the keyring later.

5\. **Personalize Your Keychain**

   * Use the **text tool** to add your **name, initials, or a short word** onto the base. You can change the font, size, and position to make it exactly how you want it.
   * You can also add extra **shapes or symbols** (like stars, hearts, or sports balls) by dragging them onto the base. Just be sure all shapes are **attached** to the base.

6\. **Raise Your Text and Shapes**

   * Make sure that any text or symbols are **raised at least 1-2 mm** above the base. This will make them visible once printed.
   * Check that everything is properly connected to the base. If text or shapes are floating, they won’t print correctly.

7\. **Check Your Design**

   * Review your keychain design. Is the base thick enough? Are all parts attached? Is there enough space for the hole without breaking the edge? These checks will ensure a successful print.

8\. **Export Your Design**

   * When your keychain is ready, click **"Export"** and choose the **.STL** file format. This is the format the 3D printer uses.
   
9\. **Get Ready to Print**

   * Upload your file in the assignment on Teams so I can send it to the people that will print it. They will first prepare the the file using a slicer program - this software will break your design down into layers for the 3D printer. And then they will print your design.


## Recommendations

Here are a few important things to keep in mind when designing your keychain to ensure it prints well on the 3D printer:

* **Thickness**: Make sure the base of your keychain is at least **2-3 mm thick**. If it’s too thin, it could break during or after printing.
  
* **Connected Parts**: Ensure that all parts of your design (text, symbols, etc.) are **attached** to the base. Floating parts or parts that don’t touch the base will not print correctly.

* **Hole Size**: The hole for the keyring should be around **5 mm in diameter**. If it’s too small, it might be difficult to attach a keyring after printing.

* **Avoid Overhangs**: Try to avoid designs that have parts hanging in the air without support. For example, shapes that float off the base will be difficult to print. Everything should be **connected to the base**.

* **Detail Size**: Be careful with very small details (like tiny letters or intricate shapes). If details are too thin or small, the printer might not print them well. Letters should be at least **2 mm wide** to be clearly readable.

* **Check Your Dimensions**: Make sure the overall size of your keychain isn’t too large or too small. It should be practical for a keychain (around 5 cm long) and strong enough not to break easily.