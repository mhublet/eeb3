Howdy! It’s your first day as a TechCrunch reporter. Your first task is to write an article on the rising trends in the startup world.

To get you started with your research, your boss emailed you a project.sqlite file that contains a table called startups. It is a portfolio of some of the biggest names in the industry.

Write queries with aggregate functions to retrieve some interesting insights about these companies.

1\. Click on <a href="https://sqliteonline.com/">the following link</a> to open the Online SQL Editor.

2\.	Click on File>Open DB and select the file ```startup.db``` in ```ClassesICT```

3\.	Start by getting a feel for the startups table:

```SQL
SELECT *
FROM startup;
```

???question "What are the column names?"

	* name
	* location
	* category
	* employees
	* raised
	* valuation
	* founded
	* stage
	* ceo
	* info

4\.	Calculate the total number of companies in the table.

5\.	We want to know the total value of all companies in this table. Calculate this by getting the ```SUM()``` of the valuation column.

6\.	What is the highest amount raised by a startup? 
Return the maximum amount of money raised.

7\.	Edit the query so that it returns the maximum amount of money raised, during ```Seed``` stage.

8\.	In what year was the oldest company on the list founded?

<hr>

Let's find out the valuations among different sectors:

9\.	Return the average valuation.

10\. Return the average valuation, in each category.

11\. Return the average valuation, in each category. 
Round the averages to two decimal places.

12\. Return the average valuation, in each category. 
Round the averages to two decimal places. 
Lastly, order the list from highest averages to lowest.

<hr>

What are the most competitive markets?

13\. First, return the name of each category with the total number of companies that belong to it.

14\. Next, filter the result to only include categories that have more than three companies in them. 
What are the most competitive markets?

<hr>

Let's see if there's a difference in startups sizes among different locations:

15\. What is the average size of a startup in each location?

16\. What is the average size of a startup in each location, with average sizes above 500?
