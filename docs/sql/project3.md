Using the same database as we used in the theory, use queries to return the following:

1\.	Return the unique values of the ```year``` column.

2\.	Retrieve all the recent movies, specifically those that were released after 2014. 
Select all the columns, using ```*```.

3\.	Select all the information about the movie titles that begin with the word ```The ```. 
You might need a space in there!

4\.	Write a query to find all the movies without an IMDb rating. 
Select only the name column!

5\.	Using the ```BETWEEN``` operator, write a new query that selects all information about movies that were released in the 1970’s.

6\.	Suppose we have a picky friend who only wants to watch old horror films. 
Using ```AND```, write a new query that selects all movies made prior to ```1985``` that are also in the ```horror``` genre.

7\.	Suppose we are in the mood for a good laugh or a good cry. 
Using ```OR```, write a query that returns all movies that are either a ```romance``` or a ```comedy```.

8\.	Write a new query that retrieves the ```name```, ```year```, and ```imdb_rating``` columns of all the movies, ordered highest to lowest by their ```ratings```.

9\.	Combining your knowledge of ```LIMIT``` and ```ORDER BY```, write a query that returns the top 3 highest rated movies. 
Select all the columns.

10\. Select the ```name``` column and use a ```CASE``` statement to create the second column that is:

* ```'Chill'``` if ```genre = 'romance'```;
* ```'Chill'``` if ```genre = 'comedy'```;
* ```'Intense'``` in all other cases.

Optional: Rename the whole CASE statement to ```Mood``` using ```AS```.
