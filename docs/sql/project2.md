1\. Click on <a href="https://sqliteonline.com/">the following link</a> to open the Online SQL Editor. Click on File>Open DB and select the file “emptyDb.db” in ClassesICT.

2\.	In the online editor, create a table named friends with three columns:

* ```id``` that stores ```INTEGER```;
* ```name``` that stores ```TEXT```;
* ```birthday``` that stores ```DATE```.

3\. Add Ororo Munroe to friends. (don’t forget to clean the editor before). 
Her birthday is May 30th, 1940.

!!! help

	* Let’s make sure that Ororo has been added to the database:

	```SQL
	SELECT * 
	FROM friends;
	```

	Check for two things:
		* Is friends table created?
		* Is Ororo Munroe added to it?

	* The date should be formatted as ```"YYYY-MM-DD"```

4\. Add two of your friends to the table. Insert an ```id```, ```name```, and ```birthday``` for each of them.

5\.	Ororo Munroe just realized that she can control the weather and decided to change her name. Her new name is ```Storm```.
Update her record in friends.

6\.	Add a new column named email.

7\.	Update the email address for everyone in your table. Storm’s email is ```storm@eursc.eu```.

8\.	Wait, Storm is fictional… 
Remove her from friends.

9\. Great job! Let’s take a look at the result one last time:

```SQL
SELECT * 
FROM friends;
```

