We have put together a table of restaurants called nomnom and we need your help to answer some questions. Use the SQL commands you just learned and find the best dinner spots in the city.

1\. Click on <a href="https://sqliteonline.com/">the following link</a> to open the Online SQL Editor.

2\.	Click on File>Open DB and select the file ```nomnom.db``` in ```ClassesICT```

3\.	Start by getting a feel for the nomnom table:

```SQL
SELECT *
FROM nomnom;
```

???question "What are the column names?"

	* name
	* neighborhood
	* cuisine
	* review
	* price
	* health

4\.	What are the distinct neighborhoods?

5\.	What are the distinct cuisine types?

6\.	Suppose we would like some Chinese takeout.
What are our options?

7\.	Return all the restaurants with reviews of 4 and above.

8\.	Suppose Abbi and Ilana want to have a fancy dinner date.
Return all the restaurants that are ```Italian``` and ```$$$```.

9\. Your coworker Trey can’t remember the exact name of a restaurant he went to but he knows it contains the word ```meatball``` in it.
Can you find it for him using a query?

10\. Let’s order delivery to the house!
Find all the close by spots in ```Midtown```, ```Downtown``` or ```Chinatown```.
???help

	```OR``` can be used more than once

11\. Find all the health grade pending restaurants (empty values).

12\. Create a Top 10 Restaurants Ranking based on reviews.

13\. Use a ```CASE``` statement to change the rating system to:

* review > 4.5 is Extraordinary
* review > 4 is Excellent
* review > 3 is Good
* review > 2 is Fair
* Everything else is Poor

Don’t forget to rename the new column!
