# Infox

## Introduction

!!!info "Définition"

	Infox (ou Fake News): Information mensongère, délibérément biaisée ou tronquée, diffusée par un média ou un réseau social afin d’influencer l’opinion publique ; fausse information.

Les infox peuvent être créées pour gagner de l'argent, manipuler l'opinion publique, ou simplement pour faire rire. 

!!!question "Questions"

	Voici une liste de titres d'article. A ton avis, lesquels sont vrais et lesquels sont faux?

	1. Un homme retrouve son portefeuille perdu il y a 40 ans dans une forêt.
	2. Les élèves qui dorment en classe apprennent plus vite, selon une étude.
	3. Le dernier dinosaure vivant filmé dans une jungle amazonienne.
	4. Une pluie de poissons observée dans un village en Inde.
	5. Un ado invente un filtre d’eau à partir de déchets recyclés.

???question "Réponses"

	1. Vrai
	2. Faux
	3. Faux
	4. Vrai
	5. Vrai

Comme vous le voyez, il est impossible de déterminer si une information est vraie ou fausse en se basant uniquement sur son intuition ou son bon sens.

## Les dangers du deepfake

!!!info "Définition"

	Un deepfake est une vidéo ou une image créée ou modifiée par une intelligence artificielle pour imiter une personne. Cela peut être amusant, comme mettre ton visage sur celui d’un acteur dans un film, mais aussi très dangereux quand c’est utilisé pour tromper les gens.  

Les deepfakes peuvent manipuler des images ou des vidéos pour faire dire ou faire quelque chose à quelqu’un qu’il n’a jamais fait, diffuser de fausses preuves ou servir à du harcèlement et des fausses accusations. 

!!!example "Exemples"

	* [Deepfake Bill Gates](https://www.youtube.com/watch?v=WzK1MBEpkJ0)
	* [Deepfake Ours Polaire](https://www.facebook.com/reel/551812554261024?locale=fr_FR).
	* [Deepfake Obama](https://www.youtube.com/watch?v=AmUC4m6w1wo).
	* [Deepfake or not?](https://youtu.be/B4jNttRvbpU?si=-ohM3nC-zQ3fo7ov)


Même si les deepfakes sont de plus en plus réalistes, il existe quelques indices pour les repérer :  

1. Les détails étranges: Regarde attentivement les mouvements de la bouche, les yeux ou les expressions du visage. Ils peuvent sembler "bizarres" ou "robotisés".  
2. Le son : La voix peut parfois sembler artificielle ou mal synchronisée avec les lèvres.  
3. Le contexte : Pose-toi des questions :

	* Cette personne dirait-elle vraiment cela ?  
	* Cette vidéo vient-elle d’une source fiable ?  


## Le rôle des réseaux sociaux et leur impact

Les réseaux sociaux montrent principalement des contenus qui te plaisent ou qui ressemblent à ce que tu as déjà vu. C’est ce qu’on appelle une bulle de filtres : tu restes "coincé" dans un cercle d’idées et d’informations qui renforcent ce que tu crois déjà, sans voir d’autres points de vue.

En plus, une infox peut facilement se propager grâce à des partages rapides, surtout si elle est étonnante ou amusante. Sur les réseaux sociaux, cela peut toucher des millions de personnes en quelques heures. Avant de partager, il faut toujours vérifier.

## Identifier une Infox

### La méthode des 5W

La méthode des 5W est un outil simple et efficace pour vérifier une information en se posant les bonnes questions. Ces 5 questions permettent de s’assurer que tous les éléments importants ont été pris en compte. Les 5W viennent de l’anglais :

* Who ? (Qui ?) - Qui est concerné par cette problématique ? Quelles sont les personnes liées à ce sujet ?
* What ? (Quoi ?) - De quel sujet parle-t-on ? Quelle est la problématique ? Quel est l’angle d’attaque ?
* When ? (Quand ?) - Quand l’événement a-t-il eu lieu ?
* Where ? (Où ?) - Quelle est la zone géographique concernée ? Où l’événement se déroule-t-il ?
* Why ? (Pourquoi ?) - Pourquoi ce sujet présente-t-il un intérêt ? Pourquoi la problématique se pose-t-elle de cette manière ?

En plus de ces cinq questions sur le fond de l'information, il est également important de savoir:

* Qui est la source? Est-elle fiable?
* Quand est-ce que l'information a été publiée? Est-ce toujours d'actualité?
* Est-ce que d'autres sources fiables relaient cette information?

En répondant à toutes ces questions, on explore un sujet sous tous ses angles et on peut déceler d’éventuelles incohérences. C’est une méthode utile pour développer un esprit critique face à la désinformation.

### Outils

Pour lutter contre les infox, plusieurs outils pratiques sont disponibles.

* Google Images propose une recherche inversée d’images : il suffit d'uploader une image ou d’en coller l’URL pour découvrir son origine et ses éventuelles modifications.
* TinEye est un autre outil de recherche inversée permettant de retrouver où et quand une image a été utilisée en ligne. 
* Hoaxbuster fournit des analyses détaillées sur une grande variété de fausses informations circulant sur Internet. 

## Mise en Pratique

<a href="https://jeu.escape-fake.fr/">Escape Fake</a> propose aux joueuses et joueurs de créer leur avatar, afin d’intégrer, en tant que stagiaire, l’équipe du Vrai Journal, un média en ligne. 

Lors de son stage, le/la journaliste stagiaire se voit confier un rôle fondamental : la vérification des informations qui tombent par dizaines par le biais des réseaux sociaux, de la boîte mail ou des messageries instantanées...