## Recherches de base

### Comment faire une recherche simple ?

Étape 1 : Choisir un moteur de recherche – Les moteurs de recherche les plus courants sont Google, Bing, et Ecosia.

Étape 2 : Taper les mots-clés – Pour trouver de l’information, tape simplement des mots ou expressions en lien avec ce que tu cherches.
Exemple : Si tu veux en savoir plus sur les volcans, tu peux taper “volcan en éruption”.

Étape 3 : Analyser les résultats – Les premières pages affichées sont souvent les plus pertinentes, mais il est utile de parcourir les titres et descriptions avant de cliquer pour s’assurer que le lien correspond à ce que tu recherches.

### Astuces pour des recherches simples :

* Soyez précis – Si possible, soyez précis dans vos termes. Par exemple, au lieu de “animaux de compagnie”, utilisez “meilleurs animaux de compagnie pour enfants”.
* Essayez plusieurs formulations – Si vous ne trouvez pas ce que vous cherchez, essayez de changer la manière dont vous posez la question.

## Recherches avancées

Pour affiner tes résultats et trouver exactement ce que tu cherches, tu peux utiliser des critères de recherche avancés :

* Guillemets (" ") – Utiliser des guillemets permet de rechercher une expression exacte.

!!!example "Exemple"

	Chercher “réchauffement climatique” va te montrer des pages où ces deux mots apparaissent ensemble et dans cet ordre.

* Le signe moins (-) – Si tu veux exclure certains mots, utilise le signe moins.

!!!example "Exemple"

	Si tu cherches “jaguar -voiture”, cela affichera des résultats sur l'animal et non la voiture.

* Site : – Si tu veux des résultats venant d’un site spécifique, tu peux ajouter site: suivi du nom du site.

!!!example "Exemple"

	“réchauffement climatique site:lemonde.fr” ne montrera que les articles de lemonde.fr.

* Filetype : – Si tu cherches un type de fichier particulier, utilise filetype: suivi du type de fichier.

!!!example "Exemple"

	“cours de mathématiques filetype:pdf” affichera seulement des fichiers PDF.

* Opérateurs de date – Tu peux limiter les résultats à une période en ajoutant une restriction de date. Dans Google, tu peux sélectionner “Outils” sous la barre de recherche, puis “Date” pour choisir des résultats récents.

## Contrôler l’information

Maintenant que tu sais comment trouver de l’information, il est important d’apprendre à vérifier si cette information est fiable. Voici quelques points importants à vérifier.

* Vérification de la source :

Fiabilité du site – Les sites gouvernementaux (.gouv, .edu) et les sites reconnus (journaux connus, universités, etc.) sont généralement plus fiables.
Auteur et date de publication – Qui a écrit l’article ? Est-ce un expert ? Est-ce que l’article est récent ou ancien ?
Domaines suspects – Fais attention aux sites qui ont des noms étranges ou des domaines suspects (.biz, .info) qui peuvent souvent contenir des informations peu fiables.

* Vérification des images :

Recherche inversée d’image – Pour vérifier l’origine d’une image, utilise Google Images. Clique sur l’icône de l’appareil photo, télécharge l’image ou colle l’URL, et Google te montrera où l’image apparaît sur le web. Cela peut t’aider à voir si une image a été manipulée ou utilisée hors contexte.

* Vérification d'une rumeur ou fausse nouvelle :

Consulte plusieurs sources – Si plusieurs sources fiables confirment une information, il est plus probable qu’elle soit vraie.
Utiliser des sites de fact-checking – Des sites comme HoaxBuster, CheckNews (de Libération), ou encore Les Décodeurs (de Le Monde) permettent de vérifier si une nouvelle est vraie ou fausse.

Vérification de la date – Assure-toi que l’article ou l’information n’est pas trop vieux. Sur certains sites, la date de dernière mise à jour est indiquée, surtout dans les articles scientifiques ou d’actualité.

* Vérification de la date de dernière mise à jour :

Vérifier directement sur le site – La date de publication ou de mise à jour se trouve souvent en haut ou en bas de l’article.

Utiliser les outils de recherche avancée – Google permet de filtrer les résultats selon leur date. Après avoir lancé une recherche, tu peux cliquer sur “Outils” puis sélectionner une plage de dates (par exemple, “Moins d’un an”).