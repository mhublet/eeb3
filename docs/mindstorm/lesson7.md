# Lesson 5 - Going Further

!!!note "DISCOVERY #9: CIRCLE TIME!"

	!!!danger "Difficulty: :fire: Time: :material-clock-time-one-outline: :material-clock-time-one-outline:"

	Can you make the robot drive in a circular pattern? Your circle should be as big as possible to fit within the table. You’ll need only one Move Steering block to accomplish this. 

	How do you configure the Steering setting, and for how long should the motors run? How does the Steering setting affect the diameter of the circle? Does changing the motor speed have any effect on the diameter? When you’re ready, try to accomplish the same effect with the Move Tank block.

!!!note "DISCOVERY #10: NAVIGATOR!"

	!!!danger "Difficulty: :fire: :fire: Time: :material-clock-time-one-outline: :material-clock-time-one-outline: :material-clock-time-one-outline:"

	Create a program with Move Steering blocks that makes the robot drive in the pattern shown in the picture below. 
	While moving, the robot should display arrows on the EV3 screen that show the direction of its movement. 
	When finished, it should display a stop sign. 

	In addition to displaying its heading, the robot should speak the direction it’s moving in. 

	How do you configure the Play Type setting in the Sound blocks? 

	![discovery 10](assets/discovery10.png){: .center style="width:200px"} 


	???info "HINT"

		You can find all the direction signs shown in the picture below in the list of images in the Display block drop down list.
