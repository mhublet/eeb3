# Lesson 2 - The Move Steering Block

## Learn

The Move Steering block controls the movement of a robot’s motors. By using it in your program, you can make the EV3 move forward or backward and steer left or right.

Modes (blue) and settings (black) of the Move Steering block:


![move steering](assets/moveSteering.png){: style="width:800px"}


To select a different mode, click the Mode Selector and choose a mode from the drop-down menu. Most of the settings will be the same, regardless of which mode you choose.

To make the robot turn, adjust the Steering setting; the block will control the speed and direction of both motors to make the robot turn. The red arrows indicate that a wheel rolls forward, and the blue ones mean that it rolls backward. The green arrow shows the direction in which the robot moved as a result.

![direction](assets/direction.png){: style="width:600px"}
 
## Practice

!!!warning

	Never click on ```Download and Run```to test your code, you should always click on ```Download```, disconnect the robot from the computer and go run your program on the table in the middle of the classroom.

	![download](assets/download.png){: style="width:200px"}

Let’s make a small program that will make the robot drive backward until the wheels make two full rotations and then spin quickly to the right for three seconds. Because these are two different actions, you’ll use two Move Steering blocks.

* Create a new project called ```MoveSteering```.
* Put the two ```set movement``` blocks needed.
* Pick two Move Steering blocks from the Programming Palette and place them on the Canvas.
* By default, the blocks you’ve just placed are configured to make the robot go forward for one rotation at 50% power, but we want to change that:

![lesson2](assets/lesson2.png){: style="width:450px"}

* Once you’ve configured both Move Steering blocks, you can download the program to your robot, disconnect the robot from the computer and go run your program on the table in the middle of the classroom. 

## Discover

For each discovery, create a new project and call it ```discovery# - yourName```.

!!!note "DISCOVERY #1: ACCELERATE!"

	!!!danger "Difficulty: :fire:		Time: :material-clock-time-one-outline:" 

	Create a program that makes the robot move slowly at first and then accelerate. 

	1\. Place two set movement blocks and two Move Steering blocks on the canvas. Configure them as shown in the picture below.

	![discovery1](assets/discovery1.png){: .center style="width:350px"}

	2\. Place five more Move Steering blocks on the Programming Canvas. Configure them in the same way as the first two but set the motor’s Power setting to 30, incrementing by 10 with each block until you reach 70. 

	The blocks are now in On for Seconds mode. Once you’ve tested your program, change all 7 blocks to On for Rotations mode, with Rotations set to 1, and run the program again. 

	!!!question 
		Which program takes longer to run? Can you explain the different behavior?	

!!!note "DISCOVERY #2: PRECISE TURNS!"

	!!!danger "Difficulty: :fire:		Time: :material-clock-time-one-outline:"

	Can you get your robot to make a 90-degree turn in place?

	Create a new program with one Move Steering block configured in On for Degrees mode, as shown in the picture below. Make sure that the Steering slider is all the way to the right, as in the Move program. 

	How many degrees should the wheels turn for the robot to make an accurate 90-degree turn? Begin by setting Degrees to 275. If that’s not enough, try 280, 285, and so on, running the program each time to see whether the robot makes the required turn. 

	Once you find the right value to make a 90-degree turn, figure out which value you need to get the robot to make a 180-degree turn.

	![discovery2](assets/discovery2.png){: .center style="width:300px"}

 

!!!note "DISCOVERY #3: MOVE THAT BOT!"

	!!!danger "Difficulty: :fire:		Time: :material-clock-time-one-outline: :material-clock-time-one-outline:"

	Create a program that uses three Move Steering blocks to make the robot move forward for three seconds at 50 percent power, turn 180 degrees, and then return to its starting position. 

	When configuring the block that lets the robot turn around (the second block), use the Degrees value that you found in Discovery #2.


!!!note "DISCOVERY #4: ROBOSPELLER!"

	!!!danger "Difficulty: :fire: :fire: Time: :material-clock-time-one-outline: :material-clock-time-one-outline:"

	Use Move Steering blocks to make a program that enables the robot to drive as if it were drawing the first letter of your name. How many blocks do you need for that letter? 

	???info "Hint"
		For curved turns, use the Steering slider to adjust the tightness of turns.
