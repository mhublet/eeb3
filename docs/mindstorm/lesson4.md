# Lesson 3 - The Sound Block

## Learn

It’s fun to make programs that make the robot move, but things get even more fun when you program the EV3 to make sounds using the Sound block. Your robot can play two types of sounds: a simple tone, like a beep, or a prerecorded sound, such as applause or a spoken word like “hello.” When you use a Sound block in your programs, the robot will seem more interactive and lifelike because it can “talk.”

![sound block](assets/soundBlock.png){: style="width:300px"}

## Practice

Now let’s create a program called ```SoundCheck``` that makes the robot move and play sounds so you can see how the Sound block works. 

To begin, create the new program shown in the picture below.

![lesson4](assets/lesson4.png){: style="width:400px"}

Once you’ve finished creating your program, download it to your robot and run it.

## Discover

!!!note "DISCOVERY #5: WHICH DIRECTION DID YOU SAY?"

	!!!danger "Difficulty: :fire: :fire: Time: :material-clock-time-one-outline:" 

	Create a program like SoundCheck that has the robot announce its direction as it moves. While going forward, it should say “Forward,” and while going backward, it should say “Backward.” 

	How do you configure the Play Type settings in the Sound blocks? 



!!!note "DISCOVERY #6: BE THE DJ!"

	!!!danger "Difficulty: :fire: :fire: Time: :material-clock-time-one-outline: :material-clock-time-one-outline: :material-clock-time-one-outline:"

	By making a program with a series of Sound blocks configured to play notes, you can play your own musical compositions. 

	Can you play a well-known tune on the EV3 or create your own catchy original?
