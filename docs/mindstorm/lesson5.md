# Lesson 4 - The Display Block

## Learn

In addition to moving the robot around and playing sounds, an EV3 program can control the EV3’s display. You could create a program that makes the robot screen show text and graphics.  

The screen is 178 pixels wide and 128 pixels tall. A pixel is the basic logical unit in digital graphics. Pixels are combined to form a complete image, video, text, or any visible thing on a computer display. A pixel is also known as a picture element (pix = picture, el = element).

Once a Display block has put something on the EV3 screen, the program moves on to the next block—let’s say a Move block. The EV3 screen keeps showing the image until another Display block is used to display something else. So, in this example, the image would remain on the screen while the robot moves.

When a program ends, the EV3 returns to the menu immediately, which means that if the last block in your program is a Display block, you won’t have time to see what’s on the display because the program will have finished. To see what’s on the display, add a block—such as a Wait block—to keep the program from ending instantly.

![display example](assets/displayExample.png){: style="width:300px"}

Zoom on the ```write block```:

![display block](assets/displayBlock.png){: style="width:300px"}

## Practice
 
Let’s test the functionality of the Display block by creating a program that puts things on the EV3 screen while the robot moves. 

Create a program called ```DisplayTest```, and place three Display blocks and two Move Steering blocks on the Programming Canvas, as shown in the picture below. Then, configure each block as shown. 

Once you’ve configured all the blocks, transfer the program to your robot and run it.

![lesson 5](assets/lesson5.png){: style="width:300px"}

!!!question "Complete the gap-fill text and answer the questions."

	1. The first Display block sets the status light to orange pulse. The light remains orange until <input type="text" id="answer" name="answer" style="background-color:#ffffe6"/>.
	2. The second Display block shows the crazy eyes while <input type="text" id="answer" name="answer" style="background-color:#ffffe6"/>. 
	3. The third Display block shows snowflakes on the screen for <input type="text" id="answer" name="answer" style="background-color:#ffffe6"/> then the robot <input type="text" id="answer" name="answer" style="background-color:#ffffe6"/>. 
	4. The next display blocks write two lines of text for <input type="text" id="answer" name="answer" style="background-color:#ffffe6"/>. 
	5. The Clear Display block clears the screen. 
	6. The next Display block writes HEY! in the center of the screen for <input type="text" id="answer" name="answer" style="background-color:#ffffe6"/> while the status light is set to green.  
	7. What happens if you delete the first Wait block? And if you delete the second one? <input type="text" id="answer" name="answer" style="background-color:#ffffe6"/>
	8. What happens if you replace the third Display block (display snow for 2 seconds) with the one in the picture below? <br><input type="text" id="answer" name="answer" size="50" style="background-color:#ffffe6"/>
	
	![display question](assets/displayQuestion.png){: style="width:200px"}


## Discover

!!!note "DISCOVERY #7: SUBTITLES!"

	!!!danger "Difficulty: :fire: Time: :material-clock-time-one-outline:"

	Create a program that uses four Sound blocks to say, “Hello. Good morning. Goodbye!” Use Display blocks to display what the robot says as subtitles on the EV3 screen and to clear the screen each time the robot starts saying something new. 
	
	Do you place the Display blocks before or after the Sound blocks?


!!!note "DISCOVERY #8: W8 FOR THE EXPLOR3R!"

	!!!danger "Difficulty: :fire: :fire: Time: :material-clock-time-one-outline: :material-clock-time-one-outline:"

	Program the robot to drive in a figure eight, as shown in the picture below. The robot should show faces on the screen as it moves. Choose different images from the Eyes category.

	![discovery8](assets/discovery8.png){: .center style="width:300px"}
