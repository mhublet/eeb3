# Lesson 1 - First Program

* Connect the robot to the computer using the USB cable and make sure the EV3 brick is on.
* Launch the Mindstorms EV3 software (click on windows and type ```classroom``` in the search bar).

![lobby](assets/lobby.jpg){: style="width:600px"}

* Open a new programming project by clicking the + symbol.
* In the top left corner of the software, click on the three dots to rename your project and call it ```MyFirstProject```.

![rename](assets/rename.png){: style="width:300px"}

* Put the following blocks in your canvas:

![lesson1](assets/lesson1.png){: style="width:400px"}


* Click the Download button.

![run](assets/download.png){: style="width:200px"}

!!!warning

	You should never click on ```Download and Run```to test your code, you should always click on ```Download```.

* Disconnect your robot and place it on the table in the middle of the classroom.

* In the File Navigation tab, find your project, click on it, go down and click on the program.

![find program](assets/findProgram.png){: style="width:400px"}

Congratulations, you made your robot move!


!!!info

	The ```set movement``` blocks are needed to configure the robot before using movement blocks:

	* ```set movement motors to B and C ``` - determines which motors are connected to the left and right wheels.

	* ```set movement motors to hold position at stop``` - determines what the robot does at the end of a move block, it controls how the motors stop. ```Hold position``` makes the motors stop immediately, while ```float``` makes them stop gently.

	```stop and exit program``` is needed to let your robot know that the program is over.


