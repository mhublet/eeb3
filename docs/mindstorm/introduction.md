The goal of this LEGO Mindstorms curriculum is to teach core computer programming logic and reasoning skills using robots.

## Different Parts

The robot is composed of the following elements:

![parts](assets/parts.png){: style="width:600px"}

## Zoom on the Brick

You can see that we have: a screen, buttons and a status light around the buttons (green in this case).

![zoom](assets/front.png){: style="width:250px"} 

## Turn on

To turn the EV3 on, you just need to press the center button and it will open up a menu with four tabs.

![turn on](assets/buttons.png){: style="width:400px"}

## Turn off

To turn the EV3 off, press the back button. When you see the power off icon, either select the check mark to turn the EV3 off or select the ```X``` to cancel.

![turn off](assets/off.png){: style="width:400px"}

## Software

We will use the Lego Mindstorms Education EV3 Classroom software:

![part](assets/software.png){: style="width:600px"}


## Finding projects and programs on the EV3 brick

When you click ```Download``` or ```Download and Run```, your project is transferred to the EV3 brick.

![Download](assets/download1.png){: style="width:300px"}

In the File Navigation tab on the EV3 brick, you should find one folder for each project, containing its program and all the files used in the projects (such as images or sounds).

You start a program by selecting it and clicking the Center button.

![find program](assets/findProgram.png){: style="width:400px"}