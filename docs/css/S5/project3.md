# Paint Store

In this project, you will follow step-by-step instructions to improve a vibrant, color-rich web page for a home paint business. It displays information about using color in a home and color swatches with varying lightness, saturation, and hue.

The page is almost ready to be published. You’ll be making the following color-related changes:
* Exchange some named colors with hexadecimal color values.
* Add some semi-transparent overlays to the banner and footer using RGBA.
* Fill in the first color column of the swatch samples using HSL colors.

!!!danger "Important"

	Before you start:

	* Copy/paste the ```Project4``` folder from ClassesICT to your personal ```CSS``` folder.
	* Open Notepad++ and in Notepad++ open the file ```style.css``` from ```Project4```.
	* Run your code: open ```File Explorer```, go to your ```CSS``` folder and double click on your file ```index.html```.

1\. In style.css, find the three CSS rules that use the named color orange. This color is not exactly the right orange to match the paint store brand. 

Replace orange with the hex color #ff8000.

2\. Make the main title text of the h1 element semi-transparent (alpha value of 0.7) to match the spooky text theme. 

3\. Give the header’s background image a semi-transparent orange overlay to simulate a photographic filter.

In the #banner:before selector around line 95, add a background color with the value rgba(255, 128, 0, 0.75).

4\. Now do the same for the footer, adding a semi-transparent layer on top of the image to soften and darken it.

Find the selector footer:before and add a background color with same semi-transparent value that you used for the h1 element.

5\. Soften the subtitles which say “Saturation,” “Lightness,” and “Hue.” Currently, they are the default black.

Add a property to the .color .swatches h4 rule to change the text color to the light gray hex value #9b9b9b.

6\. In each section (red, green, and blue), there are 15 color cells. Each cell has its own rule specifying its color in style.css. Notice that at the beginning of each color swatch, the first cell is empty. Find the rule for each of the empty cells and fill in the hsl() value which completes the pattern.

For example, the first blank swatch is .reds .lightness .color-1. In each HSL value in this section, notice that the lightness value decreases by 15 percentage points. Following the pattern, fill in the value for the background-value for this cell, hsl(350, 100%, 80%).

Use the patterns for each of the other 8 rows and fill in the missing cell for each row.
In style.css, there is a comment above each empty cell’s rule that describes the pattern.


!!!info "You should get this as a final result"

	![Project3 - top](assets/project3a.png){: style="width:500px"}
	![Project3 - middle](assets/project3b.png){: style="width:500px"}
	![Project3 - middle](assets/project3c.png){: style="width:500px"}
	![Project3 - middle](assets/project3d.png){: style="width:500px"}
	![Project3 - bottom](assets/project3e.png){: style="width:500px"}

