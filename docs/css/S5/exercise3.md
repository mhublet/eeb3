# Exercises (3/3)

We are going to improve the CSS of a website.
I’ve put some elements in a file to get you started, but you’ll write the rest of the page on your own.

Before starting the exercises, you need to:

1. Copy/paste the ```Exercise3``` folder from ClassesICT to your personal ```CSS``` folder.
2. Open Notepad++ and in Notepad++ open the file ```style.css``` and ```index.html``` from ```Exercise3```.
3. Run your code: open ```File Explorer```, go to your ```CSS``` folder and double click on your file ```index.html```.

## Exercise 1

* Change the font family of the ```<h1>``` element to Georgia.
* Change the font family of the .editorial elements to Trebuchet MS.
* Use a font stack to give the .editorial elements fallback fonts of Times New Roman and serif.
* Change the font weight of elements with the .banner p selector to lighter.
* Change the font-weight of the .header class to 900.

## Exercise 2 

The web page features three sections, “Garamond”, “Helvetica”, and “Space Mono”. Each of these sections includes a line with the name of the font creator, such as “Claude Garamond”. Let’s italicize the creator’s name on each of these cards.

In the font card section, set the font style of .font-card .creator to italic.

## Exercise 3

* Transform the text in the main heading (h1) to appear uppercase.
* Set the letter spacing of the ```<h1>``` element to 0.3em.
* Set the word spacing in the .banner p ruleset to 0.25em.
* Set the line height in the .banner p ruleset to 1.4.
* Set the text alignment of the ```<p>``` elements to justify.

## Exercise 4

The font at the bottom of the page, under the Monospaced section, needs to be “Space Mono”. Let’s fix it by linking to the Space Mono Google Font.

* Navigate to Google Fonts and select the “Space Mono” font. 
* In the list of style variations, find “Regular 400” and click “+ Select this style”.
* Copy the provided ```<link>``` element, and paste it into the ```<head>``` element inside index.html.
* In style.css, inside the .space ruleset, create a declaration using the font-family property, with 'Space Mono', monospace; as the value.

## Exercise 5

Let’s change the font of the banner using local font files. If you open up the fonts directory, you’ll notice that we have added local font files Glegoo-Regular.woff2, Glegoo-Regular.woff, Glegoo-Regular.ttf.

* At the top of style.css, create a @font-face ruleset and give it the font-family property and 'GlegooBanner' as its value.
* Within the @font-face rule, add a src property with the following paths and formats as values, in the following order:

	* url('../fonts/Glegoo-Regular.woff') and a format of 'woff'
	* url('../fonts/Glegoo-Regular.ttf') and a format of 'truetype'

* Inside the .banner p ruleset, add a declaration that sets the font family to 'GlegooBanner', with a font size of 20px.

!!!info "You should get this as a final result"

	![ex3 - a](assets/ex3a.png){: style="width:500px"}
	<br>
	![ex3 - b](assets/ex3b.png){: style="width:500px"}
	<br>
	![ex3 - c](assets/ex3c.png){: style="width:500px"}
	<br>
	![ex3 - d](assets/ex3d.png){: style="width:500px"}
	<br>
	![ex3 - e](assets/ex3e.png){: style="width:500px"}
	<br>
	![ex3 - f](assets/ex3f.png){: style="width:500px"}


