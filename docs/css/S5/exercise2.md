# Exercises (2/3)

We are going to improve the CSS of a website.
I’ve put some elements in a file to get you started, but you’ll write the rest of the page on your own.

Before starting the exercises, you need to:

1. Copy/paste the ```Exercise2``` folder from ClassesICT to your personal ```CSS``` folder.
2. Open Notepad++ and in Notepad++ open the file ```style.css``` and ```index.html``` from ```Exercise2```.
3. Run your code: open ```File Explorer```, go to your ```CSS``` folder and double click on your file ```index.html```.

## Exercise 1

Let's change the position property of a few elements in ```style.css```:

* Inside the .question ruleset, change the position property to relative.
* Next, offset .question 40 pixels from the top.
* Set the position inside of the header ruleset to absolute.

!!!info

	Notice how it’s now removed from the normal flow of the document, and covering the welcome section.
	When you changed the position to absolute, you may have noticed that the header shrunk horizontally. We’ll learn why in a later exercise. 

* For now, set the width property of the header to 100%.
* Change the position property inside of the header rule to fixed. Scroll up and down the web page. What do you notice?

!!!info 

	Notice that part of the “Welcome” section is now covered up by the header. That’s because when we changed the position of the header to fixed, we removed it from the flow of the html document. Let’s fix that. 

* Set the position of the .welcome element to relative.
* Offset the “Welcome” section by 200 pixels from the top. Everything might not be displaying correctly just yet; we’ll fix it later.

## Exercise 2

Set the z-index of the header to 10. Notice how the header is no longer covered by other elements when you scroll!

## Exercise 3

* Add a ```<footer>``` element at the bottom of the document, right above the closing ```</body>``` tag.

Nothing changed! That’s because the ```<footer>``` element is empty. 

* Add an ```<h3>``` element inside of the footer that says “Thanks for taking our survey!”
* To improve the appearance of the web page, set the height of the footer to 100 pixels.

## Exercise 4

Let’s fix the display of the list elements in the menu at the top of the page.

* Set the display property of ```<li>``` elements to inline-block.
* Set the width of the li elements to 80 pixels.
* Now, we can reduce the top offset of the “Welcome” section. Set it to 50 pixels.
* Set the display property of .answer elements to inline-block.

## Exercise 5

* Add a declaration to the .answer ruleset that sets the float property to left.

!!!info

	Take a look at the .answer divs on the web page. They have been floated to the left, but the .question divs are touching the .answer divs on the right, let’s fix this.

* In the .question selector, set the clear property to left. Notice how the questions moved.
* On second thought, this layout is not looking so good. Remove the float property from .answer selector ruleset.

!!!info "You should get this as a final result"

	![Exercises 2/3](assets/ex2a.png){: style="width:500px"}