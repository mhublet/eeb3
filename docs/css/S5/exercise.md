# Exercises (1/3)

We are going to improve the CSS of a website.
I’ve put some elements in a file to get you started, but you’ll write the rest of the page on your own.

Before starting the exercises, you need to:

1. Create a ```CSS``` folder on your personal drive.
2. Copy/paste the ```Exercise1``` folder from ClassesICT to your personal ```CSS``` folder.
3. Open Notepad++ and in Notepad++ open the file ```style.css``` from ```Exercise1```.
4. Run your code: open ```File Explorer```, go to your ```CSS``` folder and double click on your file ```index.html```.


## Exercise 1

Let's change the height and width of some elements. In ```style.css```:

* Add a height of 700 pixels to ```#banner```.
* Set ```.pull-quote``` width to 350 pixels.
* Set the ```#banner .content h1``` width to 400 pixels.
* Set the minimum width of the ```p``` element to 200 pixels.
* Set the maximum width of the ```p``` element to 800 pixels.
* Set the minimum height of the ```p``` element to 200 pixels.
* Set the maximum height of the ```p``` element to 800 pixels.

## Exercise 2

Let's modify the borders:

* Add a dotted red border with 1-pixel thickness to all ```h2``` headings.
* Add a solid, white border, with a 3 pixel thickness, to the ```#banner .content h1``` rule.
* Set the border radius of ```#banner .content h1``` to 15 pixels.

## Exercise 3

Let's modify the padding:

* In a single line, set the ```.navigation li``` elements to have 20 pixels of padding.
* Look at the red boxes at the bottom of the web page. Set the ```.share a``` elements to have 14 pixels of padding.
* Set the top and bottom padding of ```h2``` elements to 20 pixels and set the left and right padding of ```h2``` elements to 30 pixels. Use padding shorthand.
* Using two values for the padding property, set the padding of the ```p``` element to 10 pixels on the top and bottom and 20 pixels on the left and right.

## Exercise 4

And now the margin:

* Set the top margin of ```p``` elements to 60 pixels.
* Look at the three red boxes at the bottom of the web page. These elements are anchor elements of class .share. Set these ```.share a``` elements to have a margin of 10 pixels.
* Using two values, set the ```h2``` top and bottom margins to 30 pixels and the left and right margins to 20 pixels.
* In one line, set the vertical (top and bottom) margins of the ```.pull-quote``` class to 0 and the horizontal (left and right) margins to auto.
* Set the vertical margins of the ```#main``` element to 0, and the horizontal margins to auto.

## Exercise 5

In order to see the impact of ```overflow: scroll```:

* Change the height of the ```#main``` element to 1000 pixels.
* Set the overflow of the ```#main``` element to scroll.

When you scroll down, a second scroll bar should appear over the paragraph section. You may have to expand the browser component in order to see this behavior clearly.

## Exercise 6

Reset the default margin and padding values for the body. What happens to the web page in the browser?


## Exercise 7

Take a look at the list items in ```index.html```. Notice that the list item Donate has a class of donate.

In ```style.css```:

* Add a class selector ruleset for donate.
* Set the visibility to hidden.


!!!info "You should get this as a final result"

	![ex1 - a](assets/ex1a.png){: style="width:500px"}
	<br>
	![ex1 - b](assets/ex1b.png){: style="width:500px"}
	<br>
	![ex1 - c](assets/ex1c.png){: style="width:500px"}
	<br>
	![ex1 - d](assets/ex1d.png){: style="width:500px"}
	<br>
	![ex1 - e](assets/ex1e.png){: style="width:500px"}

