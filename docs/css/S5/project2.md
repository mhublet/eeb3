# Broadway

In this project, you will use properties such as display and position to improve the layout of the landing page for a fictional design firm, Broadway Design.

The site has some style rules to begin with. You will improve the layout and positioning of the navigation menu at the top of the page and the three supporting sections (Design, Develop, Deploy) below the image.

!!!danger "Important"

	Before you start:

	* Copy/paste the ```Project2``` folder from ClassesICT to your personal ```CSS``` folder.
	* Open Notepad++ and in Notepad++ open the file ```style.css``` from ```Project2```.
	* Run your code: open ```File Explorer```, go to your ```CSS``` folder and double click on your file ```index.html```.

1\. Let's start with the Navigation Menu

* The ```<header>``` currently scrolls with the rest of the document.
Set its position property so that it stays stuck to the top of the window when the document is scrolled.

* The ```<header>``` has shrunk!
Change its width so that it stretches across its entire parent element.

* List items (```<li>```) inside of the navigation section (```<nav>```) are currently displayed as a list.
Set their display property so that they can appear next to each other horizontally (but so that you can still set their width).

* Set the width of the li elements to 80 pixels.

After changing the position of the ```<header>``` element to fixed, the contents of the entire site after it shifted upwards.

* Set the position of ```<main>``` so that we can position it relatively.

* The ```<header>``` has disappeared behind the ```<main>```.
Use z-index to make the ```<header>``` visible.

* Now the navigation bar looks good, but it is blocking the title at the top of the page.
Offset ```<main>``` by 80 pixels from the top.

2\. Now, let's fix up the supporting element style below the image.

* Add a declaration to the ```.supporting .col``` ruleset so that these elements can appear horizontally next to each other but have defined height and width. 
* Set the width and height of ```.supporting .col``` elements to 200 pixels.


!!!info "You should get this as a final result"

	![Project2 - top](assets/project2.png){: style="width:500px"}
	![Project2 - bottom](assets/project2b.png){: style="width:500px"}

