# Project 2 - Olivia Woodruff Portfolio

In this project, you’ll use your knowledge of CSS visual rules to create rule sets and improve the appearance of a photography portfolio site!

* Copy/Paste the folder ```Project2``` from ClassesICT to your CSS folder.
* Open the files ```index.html``` and ```style.css``` in ```Notepad++```.
* Link your html and your css.
* Take a look at the site’s structure in ```index.html```, add some CSS in ```style.css``` and recreate the below final result:

![Project2](assets/project2.png){: style="width:800px"}
