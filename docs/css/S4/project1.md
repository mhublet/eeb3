# Project 1 - Healthy Recipes

Using CSS selectors, you’ll give a recipe website some new style!

* Copy/Paste the folder ```Project1``` from ClassesICT to your CSS folder.
* Open the files ```index.html``` and ```style.css``` in ```Notepad++```.
* Link your html and your css.
* Take a look at the site’s structure in ```index.html```, add some CSS in ```style.css``` using different types of selectors and recreate the below final result:

![Project1](assets/project1.png){: style="width:800px"}
