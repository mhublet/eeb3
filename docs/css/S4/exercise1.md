# Exercises (1/4)

* Create a folder ```CSS``` in your student folder.
* Inside the CSS folder, copy/paste the ```Chapter1``` folder from ```ClassesICT```.
* Open Notepad++ and in Notepad++ open the files ```index.html``` and ```style.css``` from ```Chapter1```.

## Exercise 1

* In ```index.html```, use inline styling to set the color of the first paragraph (the first ```<p>``` element) to green.

???question "Help"

	```html
	<p style="color: green;">
	```


## Exercise 2

Let’s move the inline style that was added to the paragraph into an internal stylesheet.

* Start by adding an empty ```<style>``` element in the head of ```index.html```.
* Inside of the ```<style>``` element, add a CSS ruleset targeting the paragraph
(the ```<p>``` element) and setting the color to green.
* Finally, delete the inline style from the ```<p>``` element (the one from ex1).

???question "Help"

	```css
	p{
		color:green;
	}
	```

## Exercise 3

Let's go from internal stylesheet to external stylesheet. 

* From ```index.html```, cut everything between the opening and closing ```<style>``` tags and paste it into the file called ```style.css```.
* Delete the remaining ```<style>``` element (now empty) from ```index.html```, save your document and refresh the page. The color is gone.

Let’s link the stylesheet ```style.css``` to the HTML file ```index.html```. 

* First, add a ```<link>``` element within the ```<head>``` section of ```index.html```.
* Next, add the href attribute to the ```<link>``` element and set it equal to ```style.css```. Take a look at the web page in the browser. Do you notice any changes yet?
* Finally, add the rel attribute and set it to the value ```stylesheet```.

???question "Help"

	```html
	<link href="style.css" rel="stylesheet">
	```

!!!danger "Important"

	External stylesheet will be the method we will always use as of now.

