# Exercises (4/4)

* Inside your CSS folder, copy/paste the ```Chapter2``` folder from ClassesICT.
* Open Notepad++ and in Notepad++ open the files ```index.html``` and ```style.css``` from Chapter2.

## Exercise 7

* Inside ```style.css```, set the font family of the main heading (h1) and subheading (h2) to ```Georgia```.
* Next, add a style rule that sets the font family of the paragraph (p) to ```Helvetica```.
* Set the font-size of paragraph (p) elements to 18 pixels.
* Set the font weight of paragraph (p) elements to bold.
* Set the text-align property of the main heading (h1) so that it appears in the center.

## Exercise 8

Take a look at the caption on the image at the bottom of the page ```index.html```.

* In ```style.css```, set the background color in the ```.caption``` selector to white.
* Then, in the same ruleset, set the color of the text to black. That should make the text a bit easier to read!
* Let’s give the caption on the image some transparency! Set ```.caption``` to have 0.75 opacity.
* Change the background image of the element with the ```.image class```. The image is stored in the following URL: https://images.pexels.com/photos/114296/pexels-photo-114296.jpeg
* Imagine ```style-library.css``` is a stylesheet that is full of good styles! But, you don’t like how it is turning the color of ```<h1>``` yellow.
In ```style.css```, add an ```!important``` rule to the color style of inside the ```h1``` ruleset to change the color back to ```#FFF``` (white).