# Selectors

Declarations are a fundamental part of CSS because they apply a style to a selected element.
But how do you decide which elements will get the style? With a selector.

A **selector** is used to target the specific HTML element(s) to be styled by the declaration.

We will go through different kind of selectors:

* the type selector;
* the universal selector;
* the class selector;
* the id selector;
* the attribute selector;
* the pseudo-class selector.

## Type

One selector you may already be familiar with is the type selector. Just like its name suggests, the type selector matches the type of the element in the HTML document.

In the previous exercises, you changed the color of a paragraph element:

```css
p {
 color: green;
}
```
This is an instance of using the type selector! The element type is ```p```, which comes from the HTML ```<p>``` tag.

## Universal

The universal selector selects all elements of any type.
Targeting all of the elements on the page has a few specific use cases, such as resetting default browser styling, or selecting all children of a parent element.

The universal selector uses the ```*``` character in the same place where you specified the type selector in a ruleset, like so:

```css
* {
 font-family: Verdana;
}
```

In the code above, every text element on the page will have its font changed to Verdana.

## Class

CSS is not limited to selecting elements by their type. As you know, HTML elements can also have attributes. When working with HTML and CSS a class attribute is one of the most common ways to select an element.

For example, consider the following HTML:

```html
<p class='brand'>Sole Shoe Company</p>
```

The paragraph element in the example above has a class attribute within the opening tag of the ```<p>``` element. The class attribute is set to ```brand```. To select this element using CSS, we can create a ruleset with a class selector of ```.brand```.

```css
.brand {
	color: pink;
}
```

!!!danger "Important"

	To select an HTML element by its class using CSS, a dot ```.``` must be added before the class’s name. In the example above, the class is ```brand```, so the CSS selector for it is ```.brand```.

!!!info "Multiple Classes"

	It’s possible to add more than one class name to an HTML element’s class attribute.
	Perhaps there’s a heading element that needs to be green and bold. 

	You could write two CSS rulesets like so:

	```css
	.green {
 	color: green;
	}
	.bold {
 	font-weight: bold;
	}
	```

	Then, you could include both of these classes on one HTML element like this:

	```html
	<h1 class='green bold'> ... </h1>
	```

	We can add multiple classes to an HTML element’s class attribute by separating them with a space. This enables us to mix and match CSS classes to create many unique styles without writing a custom class for every style combination needed.

## ID

Oftentimes it’s important to select a single element with CSS to give it its own unique style. If an HTML element needs to be styled uniquely, we can give it an ID using the id attribute.

```html
<h1 id='large-title'> ... </h1>
```

In contrast to class which accepts multiple values, and can be used broadly throughout an HTML document, an element’s id can only have a single value, and only be used once per page.

To select an element’s ID with CSS, we add the hasthag sign ```#``` in front of the id name. For instance, if we wanted to select the HTML element in the example above, it would look like this:

```css
#large-title {
}
```

The id name is ```large-title```, therefore the CSS selector for it is ```#large-title```.

While classes are meant to be used many times, an ID is meant to style only one element. IDs override the styles of types and classes. Since IDs override these styles, they should be used sparingly and only on elements that need to always appear the same.

## Attribute

Some HTML elements use attributes to add extra detail or functionality to the element. Some familiar attributes may be ```href``` and ```src```, but there are many more — including ```class``` and ```id```!

The attribute selector can be used to target HTML elements that already contain attributes. Elements of the same type can be targeted differently by their attribute or attribute value. This alleviates the need to add new code, like the class or id attributes.

Attributes can be selected similarly to types, classes, and IDs.

```css
[href]{
 color: magenta;
}
```

The most basic syntax is an attribute surrounded by square brackets. In the above
example: ```[href]``` would target all elements with an href attribute and set the color to magenta.

And it can get more granular from there by adding type and/or attribute values. One way is by using type[attribute*=value]. In short, this code selects an element where the attribute contains any instance of the specified value. 

Let’s take a look at an example.

```html
<img src='/images/seasons/cold/winter.jpg'>
<img src='/images/seasons/warm/summer.jpg'>
```

The HTML code above renders two ```<img>``` elements, each containing a ```src``` attribute with a value equaling a link to an image file.

```css
img[src*='winter'] {
 height: 50px;
}
img[src*='summer'] {
 height: 100px;
}
```

The attribute selector is used to target each image individually.

* The first ruleset looks for an ```img``` element with an attribute of ```src``` that contains the string ```winter```, and sets the height to ```50px```.
* The second ruleset looks for an ```img``` element with an attribute of ```src``` that contains the string ```summer```, and sets the height to ```100px```.

Notice how no new HTML markup (like a class or id) needed to be added, and we were still able to modify the styles of each image independently. This is one advantage to using the attribute selector!

## Pseudo-class

You may have observed how the appearance of certain elements can change, or be in a different state, after certain user interactions. 

For instance:

* When you click on an ```<input>``` element, and a blue border is added showing that it is in focus.
* When you click on a blue ```<a>``` link to visit to another page, but when you return the link’s text is purple.
* When you’re filling out a form and the submit button is grayed out and disabled. But when all of the fields have been filled out, the button has color showing that it’s active.

These are all examples of pseudo-class selectors in action! In fact, ```:focus```, ```:visited```, ```:disabled```, and ```:active``` are all pseudo-classes. 

Factors such as user interaction, site navigation, and position in the document tree can all give elements a different state with pseudo-class.

A pseudo-class can be attached to any selector. It is always written as a colon ```:``` followed by a name. For example ```p:hover```.

```css
p:hover {
 background-color: lime;
}
```

In the above code, whenever the mouse hovers over a paragraph element, that paragraph will have a lime-colored background.