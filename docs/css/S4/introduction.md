# Introduction

While HTML is the fundamental structure of every web page, it can be visually unappealing on its own. Cascading Style Sheets or CSS is a language web developers use to style the HTML content on a web page. 

If you’re interested in modifying colors, font types, font sizes, images, element positioning, and more, CSS is the tool for the job!

![CSS Anatomy](assets/anatomy.png){: style="width:800px"}

The diagram above shows two different methods, or syntaxes, for writing CSS code. 
The first syntax shows CSS applies as a ruleset, while the second shows it written as an inline style.