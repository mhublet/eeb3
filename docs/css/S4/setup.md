# Set up and Syntax

You have three different methods to add some CSS to your HTML:

* Inline Styles;
* Internal Stylesheet;
* External Stylesheet.

Our preferred method will be the last one.

## Inline Styles

Although CSS is a different language than HTML, it’s possible to write CSS code directly within HTML code using inline styles.
To style an HTML element, you can add the style attribute directly to the opening tag. After you add the attribute, you can set it equal to the CSS style(s) you’d like applied to that element.

```html
<p style='color: red;'>I'm learning to code!</p>
```
The code in the example above demonstrates how to use inline styling. The paragraph element has a style attribute within its opening tag. Next, the style attribute is set equal to color: red;, which will set the color of the paragraph text to red within the browser.

If you’d like to add more than one style with inline styles, simply keep adding to
the style attribute. Make sure to end the styles with a semicolon (;).

```html
<p style='color: red; font-size: 20px;'>I'm learning to code!</p>
```

!!!note "Info"

	It’s important to know that inline styles are a quick way of directly styling an HTML element, but are rarely used when creating websites. But you may encounter circumstances where inline styling is necessary, so understanding how it works, and recognizing it in HTML code is good knowledge to have.

## Internal Stylesheet

As previously stated, inline styles are not the best way to style HTML elements. If you wanted to style, for example, multiple ```<h1>``` elements, you would have to add inline styling to each element manually. In addition, you would also have to maintain the HTML code when additional ```<h1>``` elements are added.

Fortunately, HTML allows you to write CSS code in its own dedicated section with a
```<style>``` element nested inside of the ```<head>``` element. The CSS code inside the ```<style>``` element is often referred to as an internal stylesheet.

An internal stylesheet has certain benefits and use cases over inlines styles, but once again, it’s not best practice (we’ll get there). Understanding how to use internal stylesheets is nonetheless helpful knowledge to have.

To create an internal stylesheet, a ```<style>``` element must be placed inside of
the ```<head>``` element.

```html
<head>
 <style>
 </style>
</head>
```

After adding opening and closing ```<style>``` tags in the head section, you can begin writing CSS code:

```html
<head>
 <style>
 p {
 color: red;
 font-size: 20px;
 }
 </style>
</head>
```

The CSS code in the example above changes the color of all paragraph text to red and also changes the size of the text to 20 pixels. Note how the syntax of the CSS code matches (for the most part) the syntax you used for inline styling. The main difference is that you can specify which elements to apply the styling.

## External Stylesheet

Developers avoid mixing code by storing HTML and CSS code in separate files (HTML files contain only HTML code, and CSS files contain only CSS code).

You can create an external stylesheet by using the ```.css``` file name extension, like so: ```style.css```.

With an external stylesheet, you can write all the CSS code needed to style a page without sacrificing the readability and maintainability of your HTML file.

### Linking the CSS File

When HTML and CSS codes are in separate files, the files must be linked. Otherwise, the HTML file won’t be able to locate the CSS code, and the styling will not be applied. You can use the ```<link>``` element to link HTML and CSS files together. The ```<link>``` element must be placed within the head of the HTML file. It is a self-closing tag and requires the following attributes:

1. ```href``` — like the anchor element, the value of this attribute must be the address, or path, to the CSS file.
2. ```rel``` — this attribute describes the relationship between the HTML file and the CSS file. Because you are linking to a stylesheet, the value should be set to stylesheet.

When linking an HTML file and a CSS file together, the ```<link>``` element will look like the following if the CSS file is stored in the same directory as your HTML file:

```html
<link href='style.css' rel='stylesheet'>
```
