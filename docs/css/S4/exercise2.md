# Exercises (2/4)

!!!info

	The difficulty of these exercises will be for you to get the right selector. I will give you the declarations each time.

## Exercise 4

* In ```style.css```, use a type selector to change the color of all ```<h1>``` to maroon.

```css
color: maroon;
```

* To see how the universal selector targets all elements on a page, copy the rule below and paste it on the first line of ```style.css```.

```css
* {
 border: 1px solid red;
}
```

* In ```index.html``` there is an ```<h1>``` element with the class ```title```. Create a ruleset that selects that HTML element using  its class and add the declaration:

```css
color: teal;
```

???question "Help"

	To select an element using its class, you have to add a dot ```.``` in front of the name of the class.
	For example, if the class of a paragraph is ```intro```, the selector would be:
	```css
	.intro {

	}
	```


* In ```index.html```, in the ```<h1>``` element with a class of ```title```, add a second class named ```uppercase``` to this ```<h1>```. Next, in ```style.css```, and add a ruleset using the ```.uppercase``` class selector. Then, add the declaration below inside of the curly braces.

```css
text-transform: uppercase;
```

???question "Help"

	To add a second class in the opening tag of an element, you just need to seperate them with a space. 
	For example: 
	```html
	<h1 class = "hello">
	```
	is an h1 element with one class ```hello```

	```html
	<h1 class= "hello world">
	```
	is an h1 element with two classes ```hello``` and ```world```.


* In ```index.html```, inside the same h1 opening tag and after the class attribute, add an id with the value ```article-title```. Next, in ```style.css``` add a ruleset using the ID selector to target the ```article-title ID```. 

Inside of its curly braces, write the declaration:
```css
font-family: cursive;
```

???question "Help"

	To select an element using its id, you have to add a hashtag ```#``` in front of the name of the id.
	For example, if the id of a paragraph is ```intro```, the selector would be:
	```css
	#intro {

	}
	```

* Use the attribute selector to select the ```<a>``` element with an ```href``` attribute value containing ```florence```.

Inside of its curly braces, write the declaration:
```css
color: lightgreen;
```

???question "Help"

	Example: to select the ```img``` element with a ```src``` attribute value containing ```winter```, you have to write: ```img[src*='winter']```.


* Add a ruleset at the end of ```style.css``` that selects the ```<a>``` elements on the page. Add a ```:hover``` pseudo-class to the ```a``` selector you just created. Then, set the text color to dark orange by adding the following CSS declaration inside the declaration block:

```css
color: darkorange;
```

???question "Help"

	```css
	a:hover{
		color: darkorange;
	}
	```

!!!info

	Now when you hover the mouse over the “Learn More” text, the font color changes to dark orange!

## Exercise 5

* To demonstrate using classes on multiple elements, let’s give a few elements the same style. In index.html, there are four ```<h2>``` elements.
Give each of them a class called ```heading-background```.

* Now, let’s give a unique style to a single element using ID. 
In ```index.html```, there’s an ```<h6>``` element that displays the time the article on the page was published.
Add an id attribute to the ```<h6>```, and give it a value of ```publish-time```.

* Then, in ```style.css```, create a ruleset targeting the new ```heading-background``` class, and give it a declaration of:

```css
background-color: aqua;
```

After you refresh your page, notice how all of the ```<h2>``` elements now have the same style.

* Finally, in ```style.css```, create another ruleset using the ```publish-time``` ID selector. Add the declaration:

```css
color: gray;
```

Since ID’s are single-use, this element now has a unique ID that can’t be used again in this document!