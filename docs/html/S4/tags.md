# Common Tags (1/2)

Let's start with a bit of theory to understand how HTML works. Each time there is an example, I want you to try it and make sure you understand what it does. 

In order to be able to test the examples, you should:

1. Create a new file in Notepad++.
2. Add the basic structure for an HTML document:
```html
<!DOCTYPE html>
<html>
  <head>
  </head>
  <body>
  </body>
</html>
```
3. Save this document in your personal HTML folder and name it ```theory.html```.
4. Each time there is an example, copy/paste it between the opening and closing body tags.
5. Save your file: ```CTRL + S```.
6. Run your code: open ```File Explorer```, go to your ```HTML``` folder and double click on your file ```theory.html```.


!!!danger "Important"
    
    You don't have to delete the previous example when you want to test a new one, you can just add the new example below it.


## Headings

Headings is another word for titles. Headings in HTML are similar to headings in other types of media. For example, in newspapers, large headings are typically used to capture a reader’s attention. Other times, headings are used to describe content, like the title of a movie or an educational article.

In HTML, there are six different headings, or heading elements, they are ordered from largest to smallest in size:

1.	```<h1>``` — used for main headings. All other smaller headings are used for subheadings.
2.	```<h2>```
3.	```<h3>```
4.	```<h4>```
5.	```<h5>```
6.	```<h6>```


The following example code uses a headline intended to capture a reader’s attention. It uses the largest heading available, the main heading element:

```html
<h1>BREAKING NEWS</h1>
```

## Divs

One of the most popular element in HTML is the ```<div>``` element. ```<div>``` is short for ```division``` or a container that divides the page into sections. These sections are very useful for grouping elements in your HTML together. 

```<div>```s can contain any text or other HTML elements, such as links, images, or videos.

```html
<div>
  <h1>Why use divs?</h1>
    <p>Great for grouping elements!</p>
</div>
```

!!!danger "Important"

    ```<div>```s don’t inherently have a visual representation, but they are very useful when we want to apply custom styles to our HTML elements. 


## Attributes

If we want to expand an element’s tag, we can do so using an attribute. Attributes are content added to the **opening** tag of an element and can be used in several different ways, from providing information to changing styling.

Attributes are made up of the following two parts:

* the name of the attribute;
* the value of the attribute.

One commonly used attribute is the id. We can use the id attribute to specify different content (such as ```<div>```s) and is really helpful when you use an element more than once. ids have several different purposes in HTML, but for now, we’ll focus on how they can help us identify content on our page.

When we add an id to a ```<div>```, we place it in the opening tag:

```html  
<div id="intro">
  <h1>Introduction</h1>
</div>
```

!!!danger "Important"
    
    You will not immediately see the point of the id because it will have no visual representation. But it will be important in the future to uniquely identify an element.

## Displaying Text

If you want to display text in HTML, you can use a paragraph or span:

* paragraphs ```<p>``` contain a block of plain text;
*	```<span>``` contains short pieces of text or other HTML. They are used to separate small pieces of content that are on the same line as other content.

```html
<div>
  <h1>Technology</h1>
</div>
<div>
  <p><span>Self-driving cars</span> are anticipated to replace up to 2 million jobs over the next two decades.</p>
</div>
```

In the example above, there are two different ```<div>```. The second ```<div>``` contains a ```<p>``` with ```<span>Self-driving cars</span>```. This ```<span>``` element separates ```Self-driving cars``` from the rest of the text in the paragraph.

It’s best to use a ```<span>``` element when you want to target a specific piece of content that is inline, or on the same line as other text. If you want to divide your content into blocks, it’s better to use a ```<div>```.

## Styling Text

You can also style text using HTML tags.

* The ```<em>``` tag will generally render as <em>italic</em> emphasis.
* The ```<strong>``` will generally render as **bold** emphasis.


```html
<p><strong>The Nile River</strong> is the <em>longest</em> river in the world, measuring over 6,850 kilometers long (approximately 4,260 miles).</p>
```

## Line Breaks

The spacing between code in an HTML file doesn’t affect the positioning of elements in the browser. If you are interested in modifying the spacing in the browser, you can use HTML’s line break element: ```<br>```.

The line break element is unique because it is only composed of a starting tag. You can use it anywhere within your HTML code and a line break will be shown in the browser.


```html
<p>The Nile River is the longest river <br> in the world, measuring over 6,850 <br> kilometers long (approximately 4,260 <br> miles).</p>
```