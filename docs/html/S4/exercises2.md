# Exercises (2/3)

Let's continue our bear website.

## Exercise 5

We are now going to add two lists.

1. Under the heading that says ```Species```, create an unordered list with the following items:

	* Arctos
	* Collarus
	* Horribilis
	* Nelsoni (extinct)

2. Under the heading that says ```Countries with Large Brown Bear Populations```, add an ordered list with the following items:

	* Russia
	* United States
	* Canada


!!!info "Result"

	![ex5](assets/ex5.png){: style="width:650px"}


## Exercise 6

Under the Media ```<h2>``` heading, add an image. You can use the image ```brownBear.jpg``` saved in your ```Exercise1``` folder. Don't forget to add the ```alt``` attribute and include a description.

???question "Help"

	src = "brownBear.jpg"
	alt = "A bear rolling in the grass."
	width = "320"


## Exercise 7

Under the image, create a ```<video>``` tag and add the video ```bear.mp4``` from the folder ```Exercise1```.

???question "Help"

	src = "bear.mp4"
	width = "320"
	height = "240"
	controls

!!!info "Result"

	![ex7](assets/ex7.png){: style="width:750px"}


