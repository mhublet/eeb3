# Exercises (1/3)

We are going to build an informational website about bears using some of the most common HTML elements. 

I’ve put some elements in a file to get you started, but you’ll write the rest of the page on your own.

Before starting the exercises, you need to:

1. Copy/paste the ```Exercise1``` folder from ClassesICT to your personal HTML folder.
2. Open Notepad++ and in Notepad++ open the file index.html from ```Exercise1```.
3. Run your code: open ```File Explorer```, go to your ```HTML``` folder and double click on your file ```index.html```.

!!!info "You should get the result below"

	![ex0](assets/exercise0.png){: style="width:300px"}



## Exercise 1

We want to add some titles (headings) to our website.

* Add an ```<h2>``` heading that says Habitat.
* Below the Habitat heading, add an ```<h3>``` heading that says ```Countries with Large Brown Bear Populations```.
* Add one more ```<h3>``` heading that says ```Countries with Small Brown Bear Populations```.
* Finally, add an ```<h2>``` heading that says ```Media```.

Save your file, run your code and check that you get the below result.

!!!info "Result"

	![ex1](assets/ex1.png){: style="width:400px"}


## Exercise 2

We want to divide our website in three sections using the ```<div>``` tag and we want to give an ```id``` to each of the three sections.

![div](assets/div.png){: style="width:400px"}

To do so, you need to put an opening ```div``` tag at the beginning of the section, specify the ```id``` in the opening tag and close the ```div``` tag at the end of the section.

Example for the first section:

```html
<div id = "introduction">
	<h2>About Brown Bears</h2>
		<h3>Species</h3> 
		<h3>Features</h3>
</div>
```


!!!danger "Important"

	Adding ```<div>``` and ```<id>``` tags will not change your page when you run it in the browser. However it will structure it and later on, we will be able to create a menu that we will link to the individual sections.


## Exercise 3

Now we are going to add some paragraphs:

* Below the ```<h2>``` element that says ```About Brown Bears```, add ```<p>``` opening and closing tags, and inside of the tags put the following text:

```The brown bear (Ursus arctos) is native to parts of northern Eurasia and North America. Its conservation status is currently Least Concern. There are many subspecies within the brown bear species, including the Atlas bear and the Himalayan brown bear.```

* Below the ```<h3>``` element that says ```Features```, add a paragraph with the following text:

```Brown bears are not always completely brown. Some can be reddish or yellowish. They have very large, curved claws and huge paws. Male brown bears are often 30% larger than female brown bears. They can range from 5 feet to 9 feet from head to toe.```

* Under the ```<h3>``` element that says:
```Countries with Small Brown Bear Populations```, add a paragraph with the following text:

```Some countries with smaller brown bear populations include Armenia, Belarus, Bulgaria, China, Finland, France, Greece, India, Japan, Nepal, Poland, Romania, Slovenia, Turkmenistan, and Uzbekistan.```

!!!info "Result"

	![ex3](assets/ex3.png){: style="width:650px"}

## Exercise 4

Let's do some changes in the first paragraph that starts with “The brown bear…”:

* put ```Ursus arctos``` in italic;
* make the words ```Least Concern``` bold;
* add two line breaks ```<br>``` after ```Least Concern```.

!!!info "Result"

	![ex4](assets/ex4.png){: style="width:650px"}

