# Introduction to HTML

HTML provides structure to the content appearing on a website, such as images, text, or videos. Right-click on any page on the internet, choose “Inspect,” and you’ll see the HTML code appearing.

HTML stands for HyperText Markup Language:

* HyperText is text displayed on a computer or device that provides access to other text through links, also known as hyperlinks.
* A markup language is a computer language that defines the structure and presentation of raw text.
* In HTML, the computer can interpret raw text that is wrapped in HTML elements.
 
Learning HTML is the first step in creating websites.

## HTML Versions

Since the early days of the web, there have been many versions of HTML:

| Version     | Year                          |
| ----------- | ------------------------------------ |
| HTML      | 1991  |
| HTML+      | 1993  |
| HTML 2.0     | 1995  |
| HTML 3.2     | 1997  |
| HTML 4.01     | 1999  |
| XHTML 1.0      | 2000  |
| HTML5      | 2012  |

## HTML Anatomy

HTML is composed of elements. These elements structure the webpage and define its content.

![anatomy](assets/anatomy.png){: style="width:200px"}
 
The diagram above displays an HTML paragraph element. As we can see, the paragraph element is made up of:

* an opening tag ```<p>```;
* the content ```Hello World!```;
* a closing tag ```</p>```.

There are many tags that we can use to organize and display text and other types of content, like images. 

!!!danger "Important"

	* Tags and the content between them is called an HTML element.
	* Tags are always written in lowercase and 95% of them are defined in pairs.
	* Comments are written in HTML using the following syntax: ```<!-- comment -->```.

## Body

One of the key HTML elements we use to build a webpage is the body element. Only content inside the opening and closing body tags can be displayed to the screen. 
Here’s what opening and closing body tags look like:
```html
<body>
 
</body>
```
Once the file has a body, many different types of content – including text, images, and buttons – can be added to the body.

!!!note

	While some browsers may show some content outside body tags as well to accommodate the occasional mistake in your HTML, not all browsers do this! The best way to ensure that all your HTML renders the same way in all browsers is to ensure that your elements remain within the opening and closing body tags.

## HTML Page Structure

HTML is organized as a collection of family tree relationships. When an element is contained inside another element, it is considered the child of that element. The child element is said to be nested inside of the parent element.

![hierarchy](assets/hierarchy.png){: style="width:500px"}


Understanding HTML hierarchy is important because child elements can inherit behavior and styling from their parent element. You’ll learn more about webpage hierarchy when you start digging into CSS.

## Example

Let’s create our first HTML document:

1. Create a new folder called ```HTML``` in your H drive.
2. Open the ```Notepad++``` editor.
3. Copy/Paste the following HTML code:
```html
<!DOCTYPE html>
<html>
	<head>
		<title>
			Where is the title?
		</title>
	</head>

	<body>
		<h1>
			Title of the page
		</h1>
		<p>
			The title between the h1 opening and closing tags is visible on the page because it is inside the body.
		</p>
	</body>
</html>
```
4. Save the document in your HTML folder and give it the name ```example.html```.
 
5. Open ```File Explorer```, go to your ```HTML``` folder and double click on your file ```example.html```.


!!!danger "Important"

	Make sure you saved it as type HTML, otherwise it will be saved as a .txt. If you saved it properly, color should appear in ```Notepad++```.


Let’s check the example in details:

* The ```<!DOCTYPE html>``` declaration is an instruction, and it must be the first line of code in your HTML document. It tells the browser what type of document to expect, along with what version of HTML is being used in the document. For now, the browser will correctly assume that the html in ```<!DOCTYPE html>``` is referring to HTML5, as it is the current standard.
* To create HTML structure and content, we must add an opening and closing ```<html>``` tag after declaring ```<!DOCTYPE html>```. Anything between the opening ```<html>``` and closing ```</html>``` tags will be interpreted as HTML code. Without these tags, it’s possible that browsers could incorrectly interpret your HTML code.
* The ```<head>``` element is part of this HTML metaphor. It goes above our ```<body>``` element. The ```<head>``` element contains the metadata for a web page. Metadata is information about the page that isn’t displayed directly on the web page. Unlike the information inside of the ```<body>``` tag, the metadata in the head is information about the page itself.
* The ```<title>``` tag is always inside the head and specifies a title for the document. Here the browser displays the words Page Title in the title bar (or in the tab’s title).
* The ```<body>``` element contains the visible page content.
* The ```<h1>``` element defines a large heading.
* The ```<p>``` element defines a paragraph.

## Web Browsers

The purpose of a web browser (Chrome, Edge, Firefox, Safari) is to read HTML documents and display them.

The browser does not display the HTML tags, but uses them to determine how to display the document:

![example](assets/example.png){: style="width:500px"}

 

