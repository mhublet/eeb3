# Common Tags (2/2)

In order to be able to test the examples, reopen your file ```theory.html``` in ```Notepad++```.

## Unordered List

In addition to organizing text in paragraph form, you can also display content in an easy-to-read list.

In HTML, you can use an unordered list tag ```<ul>``` to create a list of items in no particular order. An unordered list outlines individual list items with a bullet point.

Individual list items must be added to the unordered list using the ```<li>``` tag. The ```<li>``` or list item tag is used to describe an item in a list.

```html
<ul>
  <li>Limes</li>
  <li>Tortillas</li>
  <li>Chicken</li>
</ul>
```

In the example above, the list was created using the ```<ul>``` tag and all individual list items were added using ```<li>``` tags.


## Ordered List

Ordered lists ```<ol>``` are like unordered lists, except that each list item is numbered. They are useful when you need to list different steps in a process or rank items from first to last.

You can create the ordered list with the ```<ol>``` tag and then add individual list items to the list using ```<li>``` tags.

```html
<ol>
  <li>Preheat the oven to 350 degrees.</li>
  <li>Mix whole wheat flour, baking soda, and salt.</li>
  <li>Cream the butter, sugar in separate bowl.</li>
  <li>Add eggs and vanilla extract to bowl.</li>
</ol>
```

## Images

!!!danger "Important"

    To make sure the example will work for you, you need to copy/paste the file ```imageExample.jpg``` from ClassesICT to your HTML folder. 

    It is very important that your html file and the image are saved in the exact same folder.

All of the elements you’ve learned about so far (headings, paragraphs, lists, and spans) share one thing in common: they’re composed entirely of text! What if you want to add content to your web page that isn’t composed of text, like images?

The ```<img>``` tag allows you to add an image to a web page:

```html
<img src="imageExample.jpg" />
```

The ```<img>``` tag has a required attribute called src. 

The src attribute must be set to the image’s source, or the location of the image. 

If you want to change the size of the image, you can also add an optional attribute width:

```html
<img src="imageExample.jpg" width=500 />
```

!!!danger "Important"

    Most elements require both opening and closing tags, but the ```<img>``` tag is a self-closing tag.  
    Note that the end of the ```<img>``` tag has a forward slash /. Self-closing tags may include or omit the final slash — both will render properly.


## Image Alts

Part of being an exceptional web developer is making your site accessible to users of all backgrounds. In order to make the Web more inclusive, we need to consider what happens when assistive technologies such as screen readers come across image tags.

The alt attribute, which means alternative text, brings meaning to the images on our sites. The alt attribute can be added to the image tag just like the src attribute. The value of alt should be a description of the image.

```html
<img src="imageExample.jpg " alt="A lion chilling at the beach." />
```

The alt attribute also serves the following purposes:

* If an image fails to load on a web page, a user can mouse over the area originally intended for the image and read a brief description of the image. This is made possible by the description you provide in the alt attribute.
* Visually impaired users often browse the web with the aid of screen reading software. When you include the alt attribute, the screen reading software can read the image’s description out loud to the visually impaired user.
* The alt attribute also plays a role in Search Engine Optimization (SEO), because search engines cannot “see” the images on websites as they crawl the internet. Having descriptive alt attributes can improve the ranking of your site.

If the image on the web page is not one that conveys any meaningful information to a user (visually impaired or otherwise), the alt attribute should be left empty.

## Videos

!!!danger "Important"

    To make sure the example will work for you, you need to copy/paste the file ```myExample.mp4``` from ClassesICT to your HTML folder. 

    It is very important that your html file and the video are saved in the exact same folder.

In addition to images, HTML also supports displaying videos. Like the ```<img>``` tag, the ```<video>``` tag requires a src attribute with a link to the video source.

Unlike the ```<img>``` tag however, the ```<video>``` element requires an opening and a closing tag.

```html
<video src="myExample.mp4" width="320" height="240" controls>
  Video not supported
</video>
```

In this example:

* The video source (src) is myExample.mp4 The source can be a video file that is hosted alongside your webpage, or a URL that points to a video file hosted on another webpage.
* After the src attribute, the width and height attributes are used to set the size of the video displayed in the browser. 
* The controls attribute instructs the browser to include basic video controls: pause, play and skip.
* The text, “Video not supported”, between the opening and closing video tags will only be displayed if the browser is unable to load the video.