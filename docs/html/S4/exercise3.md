# Exercises (3/3)

Let's finish our bear website.

## Exercise 8

Let's add an external link.

* After the first paragraph, right above the heading that says ```Species```, add a link that says: ```Learn More.``` and that references to the following URL:

```https://en.wikipedia.org/wiki/Brown_bear```

* Add the following attribute in the link and check what it does:

```target="_blank"```


## Exercise 9

Let's create a menu adding some internal links.

* Directly below the opening ```<body>``` tag within ```index.html```, add an anchor tag that links to ```index.html```. The link should say ```Brown Bear```.

* Under the link to ```index.html```, add an anchor tag that links to ```aboutme.html```. The link should say ```About Me```.

## Exercise 10

Let's link our page to different divisions within the same page.

* Under the ```<h1>``` element that says ```The Brown Bear```, create an unordered list with the following three list items:

	* Introduction
	* Habitat
	* Media

* Wrap the text of each list item in the unordered list in an anchor element. Each anchor tag should link to the corresponding ```<div>``` on the page (The ```<a>``` element that contains the text ```Introduction``` links to ```#introduction```).


## Exercise 11

In ```index.html```, transform the image on your page into a link by wrapping the image element with an anchor element. Set the target attribute so that the link opens in a new window.
Your image element should be a nested tag inside your anchor element.

Use the same URL as before:

```https://en.wikipedia.org/wiki/Brown_bear```



!!!info "Final result"

	![ex11a](assets/ex11a.png){: style="width:750px"}
	![ex11b](assets/ex11b.png){: style="width:750px"}

