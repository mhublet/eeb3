# Links

In order to be able to test the examples, reopen your file ```theory.html``` in ```Notepad++```.

## Linking to External Pages

One of the powerful aspects of HTML (and the Internet), is the ability to link to other web pages.

You can add links to a web page by adding an anchor element ```<a>``` and including the hyperlink reference ```href``` and the text of the link in between the opening and closing tags.

```html
<a href="https://www.wikipedia.org/">This Is A Link To Wikipedia</a>
```

## Linking to Internal pages

!!!danger "Important"

    To make sure the example will work for you, you need to copy/paste the file ```aboutme.html``` from ClassesICT to your HTML folder. 

    It is very important that the two html files are saved in the same folder.

Many sites also link to internal web pages like Home, About, and Contact.

If you have another html page saved in the same folder, you can link them together by adding the following code:

```html
<a href="./aboutme.html">About me</a>
```

## Linking to Same Page

Sometimes we want users to be able to click a link and have the page automatically scroll to a specific section.

In order to link to a target on the same page, we must give the target an id, like this:

```html
<p id="top">This is the top of the page!</p>
<h1 id="bottom">This is the bottom! </h1>
```

In this example, the ```<p>``` element is assigned an ```id``` of ```top``` and the ```<h1>``` element is assigned ```bottom```. An ```id``` can be added to most elements on a webpage.

An ```id``` should be descriptive to make it easier to remember the purpose of a link. The target link is a string containing the # character and the target element’s id.

```html
<ol>
  <li><a href="#top">Top</a></li>
  <li><a href="#bottom">Bottom</a></li>
</ol>
```

In the example above, the links to ```<p id="top">``` and ```<h1 id="bottom">``` are embedded in an ordered list. These links appear in the browser as a numbered list of links. An ```id``` is especially helpful for organizing content belonging to a div.

