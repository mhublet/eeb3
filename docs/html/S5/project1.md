Let's use a form to create a story:

![projet](assets/projet.png){: style="width:800px"}

Read the below guidelines and try to reproduce what you see in the above picture.

## How to start

Before starting the project, you need to:

1. Copy/paste the ```Project``` folder from ClassesICT to your personal HTML folder.
2. Open Notepad++ and in Notepad++ open the file ```index.html``` from ```Project```.
3. Run your code: open ```File Explorer```, go to your ```HTML``` folder and double click on your file ```index.html```.

!!!info "You should get the result below"

	![project0](assets/project0.png){: style="width:300px"}


## General Information

* The action for the form is ```story.html``` and the method is ```GET```.
* It is very important to give the correct name to each field (see list below).
* See below for the different types of the input fields.
* All the fields are required.
* Inside the ```<select>``` add a few ```<option>s``` for users to choose from, for example slower, faster, etc,...
* Let’s add a few ```<option>s``` with values within the ```<datalist>``` element. For example: ```If it gets dark, look at the light.```.
* textarea should have 8 rows and 40 columns.

## type

1. text
2. text
3. text
4. text
5. text
6. number
7. radio
8. select
9. datalist
10. textarea
11. submit

## name

1. animal-1
2. animal-2
3. animal-3
4. adj-1
5. verb-1
6. num-1
7. answer
8. speed
9. quote
10. message