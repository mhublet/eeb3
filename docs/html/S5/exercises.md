# Guided Exercises

We are going to build a form on a burger site using the most common form tags.

I’ve put some elements in a file to get you started, but you’ll write the rest of the page on your own.

Before starting the exercises, you need to:

1. Copy/paste the ```Exercise1``` folder from ClassesICT to your personal HTML folder.
2. Open Notepad++ and in Notepad++ open the file index.html from ```Exercise1```.
3. Run your code: open ```File Explorer```, go to your ```HTML``` folder and double click on your file ```index.html```.

!!!info "You should get the result below"

	![ex0](assets/ex0.png){: style="width:300px"}


## Exercise 1

In the ```<section>``` element, add a ```<form>``` element under the comment ```Add your code below```. 

1\. Assign the ```<form>``` with:

* an action attribute with a value of ```/practice.html```
* a method attribute with a value of ```POST```

Right now we have a blank ```<form>``` on a burger site, let’s add some context.

2\. Add an ```<h1>``` inside the ```<form>``` element with text related to the site between the opening and closing ```<h1>``` tags (for example: ```Create a Burger!```).

3\. Add some details to the form by inserting a ```<p>``` element below the ```<h1>``` element. Write a relevant description within the ```<p>``` element.

!!!info "You should get the result below"

	![ex1](assets/ex1.png){: style="width:300px"}

## Exercise 2

Let's start by creating a login section with a username and a password above your title. Inside your ```<form>```:

* add a label ```username```;
* add an ```<input>``` element with a type attribute of ```text```;
* add a label ```password```;
* add an ```<input>``` element with a type attribute ```password```.

!!!danger "Important"

	Let’s develop some good habits by giving a meaningful name and an id to each ```<input>``` and by linking the labels with the correct ```<input>```.

???help "What happens if we add a value attribute with a value of ```Davie``` in the username ```<input>```?"

	The username input will be pre-filled with the value ```Davie```.

!!!info "You should get the result below"

	![ex2](assets/ex2.png){: style="width:300px"}

## Exercise 3

Let's add a label asking ```How many patties would you like?``` and an ```<input>``` element with a ```type``` attribute of ```number``` and a ```step```of ```1```.

Don't forget to give a name and an id to your input and to link it with the label.

!!!info "You should get the result below"

	![ex3](assets/ex3.png){: style="width:300px"}

???help

	How to display a horizontal line? Use ```<hr>``` - an element that is used to a break between paragraph-level elements.

## Exercise 4

Let’s give our users an option for how they want to cook their patties. We can do this by adding a slider to the existing ```<form>```.

Add a label asking ```How do you want your patty cooked``` and an ```<input>``` element with a ```type``` attribute of ```range```.

For the ```<input>```, set the:

* ```min``` attribute to ```0```;
* ```max``` attribute to ```5```;
* ```step``` attribute to ```0.5```.

Don't forget to give a name and an id to your input and to link it with the label.

!!!info "You should get the result below"

	![ex4](assets/ex4.png){: style="width:300px"}


## Exercise 5

Time to add some toppings! 
You need to add:

* a question: ```What toppings would you like?```;
* three ```<input>``` with a ```type``` attribute ```checkbox```;
* three labels (one for each input) to explain to the user what each checkbox is for.

Don't forget to give a name, an id and a value to your input and to link each input with its label.

!!!info "You should get the result below"

	![ex5](assets/ex5.png){: style="width:300px"}


## Exercise 6

We can give our users the option to make the burger into a cheeseburger. Let’s use radio buttons for that.

You need to add:

* a question: ```Would you like to add cheese?```;
* two ```<input>``` with a ```type``` attribute ```radio```;
* two labels (one for each input) to explain to the user what each radio button is for.

Don't forget to give a name, an id and a value to your input and to link each input with its label.

!!!info "You should get the result below"

	![ex6](assets/ex6.png){: style="width:300px"}

## Exercise 7

Let’s now give our users a choice of buns using a dropdown list.

Add a label asking ```What type of bun would you like?``` and an ```<select>``` element with 3 ```<option>'s```:

* The first ```<option>``` should have a value of ```sesame``` and display the text ```Sesame``` on the webpage.
* The second ```<option>``` should have a value of ```potato``` and display the text ```Potato``` on the webpage.
* The third ```<option>``` should have a value of ```pretzel``` and display the text ```Pretzel``` on the webpage.

Don't forget to give a name and an id to the select and to link it with its label.

!!!info "You should get the result below"

	![ex7](assets/ex7.png){: style="width:300px"}


## Exercise 8

Time to add some sauce! Users might get creative with what sauce they choose to put, so let’s use the ```<datalist>``` element.

Add a label asking ```What type of sauce would you like?``` and a ```<datalist>``` element with 3 ```<option>'s```:

* The first ```<option>``` should have a value of ```ketchup```.
* The second ```<option>``` should have a value of ```mayo```.
* The third ```<option>``` should have a value of ```mustard```.

Don't forget to give a name and an id to the datalist and to link it with its label.

!!!info "You should get the result below"

	![ex8](assets/ex8.png){: style="width:300px"}

## Exercise 9

We covered a lot of options but users might still have other ideas. Let’s make use of a ```<textarea>``` element to give users more freedom.

Add a label asking ```Anything else you want to say?``` and a ```<textarea>``` element with 3 rows and 40 columns.

Don't forget to give a name and an id to the text area and to link it with its label.

!!!info "You should get the result below"

	![ex9](assets/ex9.png){: style="width:300px"}


## Exercise 10

At the bottom of the ```<form>``` add a submit button using the ```<input>``` element.
The text inside the submit button should read: Submit.

!!!info "You should get the result below"

	![ex10](assets/ex10.png){: style="width:300px"}