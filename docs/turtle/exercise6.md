# Exercise 6 - Create Line Drawings

You know how to move a robotic Turtle on the screen. Use this skill to create line drawings.

![Line Drawing](assets/ex6.png){: style="width:300px"}

* Open Thonny and Create a New File. Save the file as ```line```.
* Look at the line from the picture above. Complete the program below to draw it.

```python
from turtle import * 
speed("slowest") 
shape("turtle") 
forward(100)
```

What happens next? 

