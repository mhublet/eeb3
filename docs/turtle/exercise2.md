# Exercise 2 - Edit a Python Program

You are smarter than the computer. You can think for yourself and do what you want. A computer must be told what to do by a person. Follow the instructions to edit a Python program to tell the computer what you want to draw.

## 1. Open a Python Program in Thonny and Rename the File

* Open Thonny if not already open.
* From the File menu, select Open, go to the Turtle folder in your personal folder.
* Select the ```edit``` file. Click Open.
* Look at the first line of code. It imports the Turtle library of commands:

```python
from turtle import *
```

## 2. Edit the Title of the Window

* Find the code that defines the title:

```python
title("Edit a Python Script")
```

* Click on the green play button to run your code.
* Look at the top of the canvas. It has the title:

![title](assets/title.png){: style="width:600px"}

* In the code, add your name to the title
* From the File menu, select ```Save``` or press ```CTRL + S```.
* Click on the green play button to run your code.
* Look at the top of the canvas. It has the edited title.

## 3. Set the Screen Size and Background Color

* The screen size sets the width and height of the canvas. Change the canvas size:

```python
screensize(1000, 1000)
```

* Run your code.
* Drag the scroll bars to see more of the window.
* You can set the background color using a color name. Pick a color from the list below:

![colors](assets/colors.png){: style="width:300px"}

For example, if we choose honeydew:

```python
bgcolor(“honeydew”)
```

* Check the changes.

## 4. Change the Speed, Pen Size, and Shape of the Turtle

* The speed that the Turtle moves can be changed. Try it!

![speed](assets/speed.png){: style="width:300px"}

* The pen size can be set. A low value makes a thin line. A higher value makes a thick line. Change the pen size to a number between 1-10.
* The Turtle can be many shapes. Pick one you like:

![shape](assets/shape.png){: style="width:300px"}

* Check the changes.

## 5. Move the Turtle to Draw a Line

The black line on the canvas is made using this code:

```python
#move turtle
pencolor("black")
forward(100)
right(30)
backward(130)
left(90)
forward(20)
```

* Edit the amount the Turtle moves forward and backward to change the line.
* Edit the angle the Turtle moves right and left. Pick a number between 0-360:

## 6. Edit the Location and Size of a Circle

The blue circle on the canvas is made using this code:

```python
#draw a circle
penup()
goto(100, 100)
fillcolor("blue")
begin_fill()
circle(25)
end_fill()
```

* You can tell the Turtle to go to a specific point on the canvas. Tell it to go to the top right of the canvas (400, 400)
* You can set the width of a circle. Change the size.

## 7. Customize a Loop to Draw a New Shape

A loop is a block of instructions that are done more than once. A square has four lines. A loop is used to draw each line:

```python
#draw a rectangle using a loop
penup()
goto(-100, 50)
setheading(90)
pencolor ("green")
pendown()
# beginning of the loop
for line in range(4):
    forward(30)
    right(90)
# end of the loop
```

* Edit the number of times the code loops and change the angle to create a new shape.

## 8. Write a Message

Text is added to the canvas using write:

```python
#write a message
penup()
goto(-100, -100)
write("Programming is fun!", font=("Arial", 12))
```

* Edit the message and the font size.

## 9. Hide the Turtle to See When a Command is Run"

Python code is linear. Each line runs one instruction at a time from top to bottom.

* Add ```hideturtle()``` after a comment line.
* Run the program to see when the Turtle is hidden from view. If things happen too fast, slow it down: ```speed("slowest")```
* Move ```hideturtle()```. Put it after a different comment line. Run the program again.