# Exercise 5 - Ready, Set, GO

* Open Thonny.
* From the File menu, select New File.
* From the File menu, select Save.
* Type move as the file name. Click Save.
* Import the Turtle Library

You must import the Turtle library first. This helps the computer understand the meaning of the commands you will be using.

???Help

	```python
	from turtle import *
	```

* Move the Turtle forward 100, backward 400 and forward 200.

???Help

	```python
	forward(100)
	backward(400)
	forward(200)
	```

* Set the Speed and Shape of the Turtle

???Help

	```python
	speed("slowest")
	shape("turtle")
	```

* Turn left 90°, move forward 100, turn right 90° and move forward 200.

???Help

	```python
	left(90)
	forward(100)
	right(90)
	forward(200)
	```

The amount a Turtle turns is set by the angle. It can turn 360°, which is a full circle.

* Your turn, Apply your skills to move the Turtle around the screen.