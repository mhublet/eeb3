# Exercise 9 - Stamp around

The canvas is a grid. It is made up of squares. These are called pixels. When the Turtle moves forward a step, it is moving one pixel. Since the pixels are very tiny the Turtle must move many steps to see it on the canvas.

Each pixel has two numbers that let the computer know where it is on the canvas – the x and y values. These can be used to tell the Turtle to go to a specific spot. The code looks like this:

![canvas](assets/ex9.png){: style="width:400px"} 
 
Apply what you know about the canvas to add stamps, make a cool design!

![stamp](assets/ex9b.png){: style="width:200px"} 

Here are the commands you will use:

![commands](assets/ex9c.png){: style="width:400px"} 


* Open Thonny, create a new file and save it as stamp.
* There must be a stamp in each quadrant of the canvas, pick 4 coordinates (x and y) where you want to add a stamp.
* Import the Turtle library, set the screen size and the speed to slow. Make the symbol look like a Turtle.
* Add the places you want the Turtle to go, using the goto function.
* Add a stamp at each spot using stamp ( )
* Colors have names. You can make each stamp a different color. Try it!

Some color names:

![color](assets/ex9d.png){: style="width:500px"} 

Find more color names <a href="https://www.w3schools.com/colors/colors_names.asp">here</a>.

* Control when a line is made. Pick the pen up to stop drawing a line. Put the pen down to start drawing a line.
* Be creative!