# Introduction to Turtle

## What Is a Programming Language?

A programming language is a set of words used to talk to computers. It is a code that machines understand. Just like the language you speak and write; each word has meaning.

When you code, you become a programmer. A programmer is a person that understands a programming language and uses it to make programs.

A program is a set of instructions that tells a device what to do. You use programs every day! They are used to select a show or movie to watch, upload a picture using a mobile phone, play an online game, or stream a video from the Internet.

## What Is Python?

Python is a type of programming language. It was created by Guido van Rossum about thirty years ago. His goal was to invent code that was easy to read, write, and understand. He wanted it to be for everybody! Today, Python is used by programmers to develop programs that:

* analyze large amounts of data
* build models to test ideas
* find information on a website
* host websites
* design and launch mobile apps
* support machine learning, which is when a device can improve how it works by itself

## What Is a Python Library?

The Python programming language uses special words to tell the computer what to do. A
function is a word that does a specific task. Many Python functions are stored into libraries.

You will be using Python libraries to build programs. The one you will use the most is the Turtle library. You will also use the Random library and Time library to help you write your programs.

|Icon | Library Name     | Description                          |
| ----------- | ----------- | ------------------------------------ |
| ![turtleLib](assets/turtle.png) | Turtle Library | A set of commands that control a robotic Turtle making it move, draw, and write. |
| ![randomLib](assets/random.png) | Random Library | A set of commands that picks an unknown number or choice from a list. |
| ![timeLib](assets/time.png) | Time Library | A set of commands that show the current time or direct the timing of events. |


## Let’s visit the Turtle Library.

The name of a command or function often gives a clue about what it does. Look at the list of functions in the Turtle library. You will be learning many of them during this project.

1.	Visit the <a href='https://docs.python.org/3.7/library/turtle.html'>Turtle Graphics Standard library</a>. 
2.	Scroll down to see a list of commands. What do you think they do?
	
	* forward()	
	* stamp()	
	* pendown()		
	* hideturtle()