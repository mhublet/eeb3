# Exercise 3 - Zap the Bugs

Many of the skills you use to write a story are the same ones you need to program. A story needs proper spelling, punctuation, and grammar. If it does not, the reader may not understand the text.
Finding and fixing the mistakes in your writing is called proofreading.

Building a program is a lot like story writing. When programming, each word must be spelled correctly. Quotes, brackets, colons, and commas must all be in the right place. Plus, some lines need to be indented to group the steps. If something is wrong, the machine will not understand the code and the program will not run. Finding and fixing the mistakes is called debugging.

You are going to add bugs or mistakes into a program. You will then read the error messages and fix the code. This will help you to learn how to debug Python.

## 1. Open Bug Zapper in Thonny

* Open Thonny if not already open.
* From the File menu, select Open and go to the Turtle folder in your personal folder.
* Select the file ```bug zapper```. Click Open.

## 2. Do Not Import the Turtle Library and Then Read the Error Message"

You must import a library to be able to use the commands or functions. If you do not, the computer will not understand your program because the words are not defined.

* Delete the first line of code. It imports the Turtle library of commands:

```python
from turtle import *
```

* Run the program. An error message will show in the Python Shell:

![bz1](assets/bz1.png){: style="width:300px"}

* From the Edit menu, select Undo to fix the code.

## 3. Remove the Quotes and Then View the Line of Code That Has the Error

The value in a bracket gives information to the function. Text inside a bracket must have quotes. If you forget them, you will get an error.

* Delete the quotes around the string ```black``` from the code ```pencolor("black")```.
* Run the program. An error message will show in the Python Shell:

![bz2](assets/bz2.png){: style="width:300px"}

The Python Shell will tell you the line number with the error.

* Add quotes around the string in line 4.

## 4. Forget # On a Comment

A comment is a note to the programmer. They are not instructions for the computer. A comment starts with a #. Comments are grey. If you forget the # you will get an error.

* Delete the # from the line ```#set drawing tools```.
* Run the program. A syntax error message appears:

![bz3](assets/bz3.png){: style="width:300px"}	

* Add # to fix the code.

## 5. Leave Out the End Bracket and Then Search for the Error

An argument goes inside brackets. If you forget one bracket or have too many, you will get a syntax error. The highlighted word will give you a clue about where the problem is in the code.

* Delete the end bracket ```)``` from the line ```pensize(5)```.
* Run the program.
* A syntax error message appears:

![bz4](assets/bz4.png){: style="width:300px"}	

* Add the bracket to fix the code.

## 6. Indent a Block of Text Incorrectly

Some code must be stacked together, such as the instructions that are part of a loop. This is done by indenting each line the same number of spaces. This creates a block of instructions that the computer will run as a group. If you do not indent a line correctly, you will get an error.

* Change the indent of the first instruction in the loop ```for line in range(4)```:

```python
for line in range(4):
	forward(50)
	right(90)
```

* Run the program.
* A syntax error message appears. It states that Python expected an indented block.

![bz5](assets/bz5.png){: style="width:300px"}	

* Add four spaces (press ```TAB```) to reindent the line correctly.
