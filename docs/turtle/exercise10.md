# Exercise 10 - Draw a Robot

You are going to build a program that draws a robot. 

![robot](assets/robot2.png){: style="width:200px"} 

Here are the drawing commands you will use:

| Method      | Description                          |
| ----------- | ------------------------------------ |
| goto(x, y) | Go to a specific spot on the canvas.|
| pensize(thickness) | Set the size of the pen outline.|
| pencolor("color") | Set the color of the pen.|
| penup() | Pick up the pen to stop drawing a line.|
| pendown() | Put down the pen to start drawing a line.|
|fillcolor ("color") | Set the fill color of a stamp.|
|begin_fill() | Let the computer know you are making a shape to fill.|
|end_fill() | Fill the completed shape with a fill color.|
|setheading(direction) | Set the direction the Turtle faces. 0 is right, 180 is left, 90 is up, 270 is down.|
|circle(size) | Draw a circle that is a specific size.|
|dot() | Add a filled circle to the canvas.|
|stamp () | Put a mark of the Turtle symbol on the canvas.|

Study the plan of what you will be making:

![robot](assets/robot.png){: style="width:500px"} 
 
Here are some steps to help you:

* Open Thonny, create a new file and save it as robot.
* Import the Turtle library. Set the screen size and speed.
* Draw a line to make an antenna.
* Pick the pen up and put it down to draw a rectangle for the head.
* Create a triangle body and fill it with color.
	
	* Pick a fill color for the body, for example ```fillcolor(“steelblue”)```.
	* You must tell the computer what you want to fill. ```begin_fill()``` goes before the start of the shape and ```end_fill()``` goes after the last shape instruction.

* Control the direction to make a circle using ```setheading()```
* Add a dot to the top of the antenna.
* Draw a face using stamps.
 
