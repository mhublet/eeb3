# Exercise 1 - Run a Python Program

Python programs can be viewed and run using ```Thonny```.

![thonny](assets/thonny.png){: style="width:300px"}

```Thonny``` comes with Python 3.7 built in, so just one simple installer is needed and you're ready to learn programming.

## 1. Open a Python Program in Thonny

* Go to ClassesICT and copy the folder Turtle.
* Paste it in your personal folder.
* Open Thonny (Python).
* From the File menu, select Open.
* Go to the Turtle folder in your personal folder.
* Select the file called ```edit```. Click Open.


![edit](assets/edit.png){: style="width:300px"}


## 2. Read the Code to Guess What the Program Will Make

Thonny color codes the text. This makes it easier to read and find mistakes.

Read the comments. They are in grey. Guess two things that the program will make.

Answer 1: <input type="text" id="answer" name="answer"/>
<br>
Answer 2: <input type="text" id="answer" name="answer"/>

## 3. Run the Program to View the Python Shell and Canvas

* In the top ribbon, click on the green play button.
* The Python Shell reads the code and then shows the output. If there is an error, it will show in red.
* A window opens to show the Turtle canvas.
* Close the canvas.

![canvas](assets/canvas.png){: style="width:300px"}


## 4. Study the Python Code and Run the Module to Answer the Questions

* Many commands end with a bracket. Some brackets have numbers inside.
What do you think ```forward(100)``` does?
* Some commands have text called a string inside the bracket.
What do you think ```fillcolor("blue")``` does?

Answer 1: <input type="text" id="answer" name="answer"/>
<br>
Answer 2: <input type="text" id="answer" name="answer"/>