# Exercise 7 - Move through the Maze

Your goal is to move the Turtle through the maze. You must write instructions to go from the start to the end point. The Turtle must stay inside the path!
Help the Turtle swim home.

![Maze](assets/ex7.png){: style="width:500px"} 

## 1. Copy the Maze Folder

The maze is a picture. You will add it to the background using the following python code

```bgpic("picture.gif")```

To have it display, the picture MUST BE in the same place as the maze Python file.

* Open the Turtle folder in your personal folder.
* Copy the maze folder and paste it where you save your work.

## 2. Save a Python File into the Maze Folder

* Open Thonny.
* From the File menu, select New File.
* From the File menu, select Save.
* Go the maze folder you copied in Step 1.
* Type swim as the file name. Click Save.

## 3. Start the Program

* Import the Turtle library. Set the speed to slow. Make the symbol look like a Turtle.
 
## 4. Apply a Background Picture to the Canvas

* Add the maze image to the background using the command ```bgpic("maze.gif")```:
The Turtle will be at the start of the maze.

## 5. Program the Turtle to Move to the End of the Maze

* Move the Turtle to the end of the maze. (The maze is 600x600)
