# Exercise 4 - Stamp it

Put your debugging skills to the test!

* In Thonny, open the file ```stamp it``` from the Turtle folder in your personal folder.
* There are four errors in the stamp it program. Can you find and fix the bugs?
