# Exercise 8 - Connect the dots

![Maze](assets/ex8.png){: style="width:100px"} 


## 1. Copy the Dot folder

* Open the Turtle folder located in your personal drive. Copy the dot folder and paste it where you save your work.
* Open Thonny.
* From the File menu, select New File.
* From the File menu, select Save.
* Go the dot folder you copied in Step 1. Type dot as the file name. Click Save.

## 2. Connect the Dots

* Add the kite image to the background:

```python
from turtle import * 
speed("slowest") 
shape("turtle") 
pensize(4) 
bgpic("kite.gif")
```

* Use the following commands to connect the dots:

![Maze](assets/ex8B.png){: style="width:300px"} 